* LinkAhead Server 0.12.0
* Make 4.2.0

# Java Script Libraries (included in this repository)
* bootstrap-5.0.1
* bootstrap-autocomplete-2.3.5
* bootstrap-icons-1.4.1
* bootstrap-select-1.14.0-beta3
* dropzone-5.5.0
* javascript-state-machine-master
* jquery-3.6.0.min.js
* loglevel-1.6.4
* qrcode-1.4.4
* showdown-1.8.6
* plotly.js-1.52.2
* UTIF-8205c1f

## For the map

* leaflet-1.5.1
* Leaflet.Coordinates-0.1.5
* leaflet.latlng-graticule-20191007
* L.Graticule.js https://github.com/turban/Leaflet.Graticule/blob/e9146fbea59ce1b0ada4ea2a012087f9a1a12473/L.Graticule.js
* proj4js-2.5.0
* Proj4Leaflet-1.0.1

## For CKEditor (WYSIWYG editor in edit mode)

* We're using a custom-built ckeditor 32.0.0 from [IndiScale's fork of CKEditor
  5](https://gitlab.indiscale.com/caosdb/src/ckeditor5) with a customized set of
  editor plugins. Please refer to the `package.json` within
  `libs/ckeditor...zip` for a full list of said plugins.

## For unit testing
* qunit-2.9.2

### Debian 12 (also on WSL)
* unzip
* gettext
* firefox-esr
* xvfb
* libpci-dev
* libegl1
* imagemagick (to use `convert` on `screenshot.xwd`)

Install test dependencies on Debian
```bash
sudo apt install unzip gettext firefox-esr xvfb x11-apps libpci-dev libegl1 imagemagick
```

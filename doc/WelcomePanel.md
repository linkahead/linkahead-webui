<!--
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2019 IndiScale GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
-->

# Welcome Panel

The Welcome Panel shows a welcome message to the authenticated or anonymous user. It is shown in the webinterface when nothing elso would be there.

# Implementation and Customization

The `src/core/xsl/main.xsl` calls the named XSL template "welcome" if the `/Response` node of the server's response has less than 2 child nodes and none of them are `Info`, `Warning` or `Error` nodes. This rule is that complicated because most of the time there is a `/Response/UserInfo` node which does not generate any visual content and will therefore be ignored.

The "welcome" template is defined in `src/core/xsl/welcome.xsl` and shows a html `div` per default. It should be overridden by a `src/ext/xsl/welcome.xsl` to show some useful welcome message. Just copy the core file to the ext directory and change it. The welcome message's `div` should have the class `caosdb-f-welcome-panel` just as in the default definition.



#!/usr/bin/env python3
# encoding: utf-8
#
# ** header v3.0
# This file is a part of the LinkAhead Project.
#
# Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
# Copyright (C) 2020 Henrik tom Wörden <h.tomwoerden@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#

import os
import unittest

import linkahead as db
from linkahead.common.models import _parse_single_xml_element
from lxml import etree
from pandas_table_preview import (MAXIMUMFILESIZE, create_table_preview,
                                  ending_is_valid, read_file, size_is_ok)


class PreviewTest(unittest.TestCase):
    def test_file_ending(self):
        self.assertFalse(ending_is_valid("/this/is/no/xls.lol"))
        self.assertFalse(ending_is_valid("xls.lol"))
        self.assertFalse(ending_is_valid("ag.xls.lol"))
        assert ending_is_valid("/this/is/a/lol.xls")
        assert ending_is_valid("/this/is/a/lol.csv")
        assert ending_is_valid("/this/is/a/lol.cSv")
        assert ending_is_valid("/this/is/a/lol.CSV")
        assert ending_is_valid("lol.CSV")

    def test_file_size(self):
        entity_xml = ('<File id="1234" name="SomeFile" '
                      'path="/this/path.tsv" size="{size}"></File>')
        small = _parse_single_xml_element(
            etree.fromstring(entity_xml.format(size="20000")))

        assert size_is_ok(small)
        large = _parse_single_xml_element(
            etree.fromstring(entity_xml.format(
                size=str(int(MAXIMUMFILESIZE+1)))))
        assert not size_is_ok(large)

    def test_output(self):
        files = [os.path.join(os.path.dirname(__file__), "data", f)
                 for f in ["test.csv", "test.tsv", "test.xls", "test.xlsx"]]

        for fi in files:
            table = read_file(fi, ftype="."+fi.split(".")[-1])
            searchkey = fi.split(".")[-1]+"file"
            print(table)
            assert (table == searchkey).any(axis=None)

        badfiles = [os.path.join(os.path.dirname(__file__), "data", f)
                    for f in ["bad.xls", "bad.xlsx"]]

        for bfi in badfiles:
            print("bfi: ", bfi)
            self.assertRaises(ValueError, read_file,
                              bfi, "."+bfi.split(".")[-1])

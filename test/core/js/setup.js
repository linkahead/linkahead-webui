/*
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2020 Timm Fitschen (t.fitschen@indiscale.com)
 * Copyright (C) 2020 IndiScale GmbH (info@indiscale.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
"use strict";

function _reset_env() {
  if(connection && typeof connection._init === "function") {
    connection._init()
  }
}

QUnit.config.testTimeout = 10000;

QUnit.moduleStart(function(module) {
    localStorage.clear();
    sessionStorage.clear();
});

QUnit.moduleDone(function(module) {
    _reset_env();
});

QUnit.done(function( details ) {
    var report = (details.failed === 0 ? "SUCCESS\n" : "FAILURE\n") + JSON.stringify(details, null, 2);
    $.post("/done", report);
});

const sleep = function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

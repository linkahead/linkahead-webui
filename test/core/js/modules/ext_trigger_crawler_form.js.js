/*
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

'use strict';

QUnit.module("ext_trigger_crawler_form", {
    before: () => {
        $(document.body).append('<div id="top-navbar"><ul class="caosdb-navbar"/></div>');
    },
    beforeEach: () => {
        $(".caosdb-f-navbar-toolbox").remove();
    },
    after: () => {
        $("#top-navbar").remove();
    },
});

QUnit.test("test build variables and availability", function(assert) {
    assert.ok(ext_trigger_crawler_form, "availble");
    assert.equal("${BUILD_MODULE_EXT_TRIGGER_CRAWLER_FORM}", "DISABLED", "BUILD_MODULE_EXT_TRIGGER_CRAWLER_FORM disabled by default.");
    assert.equal("${BUILD_MODULE_EXT_TRIGGER_CRAWLER_FORM_TOOLBOX}", "Tools", "BUILD_MODULE_EXT_TRIGGER_CRAWLER_FORM_TOOLBOX defaults to Tools");
});

QUnit.test("test init", function(assert) {
    var tools = navbar.get_toolbox("Tools");

    assert.equal($(tools).length, 1);
    assert.equal($(tools).find("button").length, 0);
    ext_trigger_crawler_form.init()
    tools = navbar.get_toolbox("Tools");
    assert.equal($(tools).find("button").length, 1);

});



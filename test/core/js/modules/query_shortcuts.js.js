/*
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019 Timm Fitschen (t.fitschen@indiscale.com), Göttingen
 * Copyright (C) 2019 IndiScale GmbH, Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

/* SETUP query_shortcuts test module */
QUnit.module("query_shortcuts.js", {
    before: function(assert) {
        query_shortcuts._shortcuts_property_description_name = "templateDescription";
        query_shortcuts._shortcuts_property_query_name = "Query";
        query_shortcuts._shortcuts_record_type_name = "UserTemplate";

        query_shortcuts._shortcuts_property_description_id = "1234";
        query_shortcuts._shortcuts_property_query_id = "2345";
        query_shortcuts._shortcuts_record_type_id = "3456";

        markdown.init();
        connection._init();
    },
    after: function(assert) {
        connection._init();
    }
});

QUnit.test("available", function(assert) {
    assert.ok(query_shortcuts);
});

QUnit.test("init", function(assert) {
    assert.ok(query_shortcuts.init);
});

QUnit.test("generate_shortcut_form", function(assert) {
    assert.ok(query_shortcuts.generate_shortcut_form);
});

QUnit.test("find_placeholders", function(assert) {
    assert.propEqual(query_shortcuts.find_placeholders("{test1}"), ["test1"]);
    assert.propEqual(query_shortcuts.find_placeholders("asdf {test1}"), ["test1"]);
    assert.propEqual(query_shortcuts.find_placeholders("asdf {test1} sdfg"), ["test1"]);
    assert.propEqual(query_shortcuts.find_placeholders("asdf {test1} sdfg {test2}"), ["test1", "test2"]);
    assert.propEqual(query_shortcuts.find_placeholders("asdf {test1} sdfg {test2} wert"), ["test1", "test2"]);
});


QUnit.test("replace_placeholders_with_values", function(assert) {
    assert.equal(query_shortcuts.replace_placeholders_with_values("FIND RECORD WITH date IN {bla}", {"bla":"2018"}), "FIND RECORD WITH date IN 2018");
    assert.equal(query_shortcuts.replace_placeholders_with_values("FIND RECORD WITH date IN {bla} and {bla}", {"bla":"2018"}), "FIND RECORD WITH date IN 2018 and 2018");
});

QUnit.test("extract_placeholder_values", function(assert) {
    var form = $('<div class="shortcut-form"/>');

    form.append('<input name="test1" value="val1"/>');
    assert.propEqual(query_shortcuts.extract_placeholder_values(form[0]), {"test1": "val1"});

    form.append('<input name="test2" value="val2"/>');
    assert.propEqual(query_shortcuts.extract_placeholder_values(form[0]), {"test1": "val1", "test2": "val2"});

});

QUnit.test("replace_placeholders_with_inputs", function(assert) {
    assert.equal(query_shortcuts.replace_placeholders_with_inputs("{test1}"), " <input type=\"text\" name=\"test1\" title=\"test1\"/> ");
    assert.equal(query_shortcuts.replace_placeholders_with_inputs("asdf {test1}"), "asdf  <input type=\"text\" name=\"test1\" title=\"test1\"/> ");
    assert.equal(query_shortcuts.replace_placeholders_with_inputs("asdf {test1} sdfg"), "asdf  <input type=\"text\" name=\"test1\" title=\"test1\"/>  sdfg");
    assert.equal(query_shortcuts.replace_placeholders_with_inputs("asdf {test1} sdfg {test2}"), "asdf  <input type=\"text\" name=\"test1\" title=\"test1\"/>  sdfg  <input type=\"text\" name=\"test2\" title=\"test2\"/> ");
    assert.equal(query_shortcuts.replace_placeholders_with_inputs("asdf {test1} sdfg {test2} wert"), "asdf  <input type=\"text\" name=\"test1\" title=\"test1\"/>  sdfg  <input type=\"text\" name=\"test2\" title=\"test2\"/>  wert");
});

QUnit.test("retrieve_global_shortcuts", async function(assert) {
    assert.ok(query_shortcuts.retrieve_global_shortcuts);
    var done = assert.async();


    // mock simple shortcut
    query_shortcuts._query_global_shortcuts = async function() {
        done();
        return [{description: "help-text-bla", query: "FIND blabla"}];
    }

    var arr = await query_shortcuts.retrieve_global_shortcuts();
    assert.equal(arr.length, 1, "array with one element");
    assert.equal($(arr[0]).find("span.caosdb-f-query-shortcut-form").text(), "help-text-bla", "desc text inserted");
});

QUnit.test("retrieve_user_shortcuts", async function(assert) {
    assert.ok(query_shortcuts.retrieve_user_shortcuts);
    var done = assert.async();


    // TODO return something meaningful
    query_shortcuts._query_user_shortcuts = async function() {
        done();
        return [];
    }

    var userShortcuts = await query_shortcuts.retrieve_user_shortcuts();
    assert.ok($.isArray(userShortcuts), "returned array");
    assert.equal(userShortcuts.length, 0, "array is empty");
});

QUnit.test("init_delete_shortcut_form", function(assert) {
    assert.equal(typeof query_shortcuts.init_delete_shortcut_form, "function", "function available");

    // test panel
    var panel = $('<div/>');
    var header = $('<span class="h3">Shortcuts</span>');
    panel.append(header);
    var orig_children = panel.children();
    $('body').append(panel);


    // before
    assert.equal(orig_children.is(":visible"), true, "panel content visible before");
    assert.equal(panel.find("form").length, 0, "no form before");

    query_shortcuts.init_delete_shortcut_form(panel[0]);

    // after
    assert.equal(orig_children.is(":visible"), false, "original panel content not visible after");
    assert.equal(panel.find(".caosdb-f-form-wrapper").length, 1, "panel has form after");

    // test cancel button
    var done = assert.async();
    panel[0].addEventListener("caosdb.form.cancel", async function(e) {
        await sleep(200);
        assert.equal(panel.find("form").length, 0, "form is gone");
        done();
    }, true);

    panel.find("button.caosdb-f-form-elements-cancel-button").click();



    // clean up
    panel.remove();
});

QUnit.test("make_delete_form", function(assert) {
    assert.equal(typeof query_shortcuts.make_delete_form, "function", "function available");
    var done = assert.async();

    // test panel
    var panel = $('<div/>');
    var header = $('<span class="h3">Shortcuts</span>');
    var userTemplate1 = query_shortcuts.generate_user_shortcut("the_description", "FIND nothing", "id28");
    var userTemplate2 = query_shortcuts.generate_user_shortcut("the_description", "FIND nothing", "id29");
    var userTemplate3 = query_shortcuts.generate_user_shortcut("the_description", "FIND nothing", "id39");
    var globalTemplate = query_shortcuts.generate_shortcut_form("the_other_description", "FIND anything");

    panel.append(header);
    panel.append(userTemplate1);
    panel.append(userTemplate2);
    panel.append(userTemplate3);
    panel.append(globalTemplate);

    query_shortcuts._deleteEntities = function(dels) {
        assert.propEqual(dels, ["id28","id29"], "deleteEntities called");
        done();
        return str2xml("<root/>");
    };

    var delete_callback = async function(delform) {
        assert.equal(delform, $(form).find("form")[0], "form calls delete_callback");
        var dels = query_shortcuts.get_checked_ids(delform);
        assert.propEqual(dels, ["id28", "id29"], "get_checked_ids(form) = [id28 id29]");
        var ret = await query_shortcuts.delete_callback(delform);
        return ret;
    }

    var form = query_shortcuts.make_delete_form(panel[0], delete_callback);
    $('body').append(form);

    assert.equal($(form).hasClass("caosdb-f-form-wrapper"), true, "is form");
    assert.equal($(form).find("input[type='checkbox']").length, 3, "three checkboxes (for three user-defined shortcuts)");

    // check two
    $(form).find(":checkbox[name='id28']").click();
    $(form).find(":checkbox[name='id29']").click();
    $(form).find("[type='submit']").click();

    $(form).find("button.caosdb-f-form-elements-cancel-button").click();

});

QUnit.test("transform_entities", async function(assert) {
    var result = str2xml(`<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="https://localhost:44301/webinterface/webcaosdb.xsl" ?>
<Response username="tf" realm="PAM" srid="e32b9552475ef9ca12a2b5f49aa58c1c" timestamp="1571305648225" baseuri="https://localhost:44301" count="1">
  <Record id="2735" name="Test Template (User 1)">
    <Info code="10" description="This entity has been deleted successfully." />
    <Parent id="2733" name="UserTemplate" />
    <Property id="2731" name="Query" datatype="TEXT" importance="FIX">
      FIND Thing1
    </Property>
    <Property id="2732" name="templateDescription" datatype="TEXT" importance="FIX">
      Test Template (User 1)
    </Property>
  </Record>
</Response>`);
   var transformed = await query_shortcuts.transform_entities(result);
    assert.ok(Array.isArray(transformed), "transformed is array");
    assert.ok(transformed[0] instanceof HTMLElement, "transformed[0] is html element");
});

QUnit.test("make_create_form", function(assert) {
    var panel = $('<div/>');
    var form = query_shortcuts.make_create_form(panel[0], () => {});
    assert.ok($(form).hasClass("caosdb-f-form-wrapper"), "form created");

    $('body').append(form);

    $(form).find(":input[name='templateDescription']").val("NEW DESC");
    $(form).find(":input[name='Query']").val("NEW QUERY");

    var entity = getEntityXML(form);
    assert.equal(xml2str(entity), "<Record><Parent name=\"UserTemplate\"/><Property name=\"templateDescription\">NEW DESC</Property><Property name=\"Query\">NEW QUERY</Property></Record>", "entity extracted");

    form_elements.dismiss_form(form);


});

QUnit.test("make_update_form", function(assert) {
    var panel = $('<div/>');
    var header = $('<span class="h3">Shortcuts</span>');
    var userTemplate1 = query_shortcuts.generate_user_shortcut("the_description", "FIND nothing", "id28");
    var userTemplate2 = query_shortcuts.generate_user_shortcut("the_description", "FIND nothing", "id29");
    var userTemplate3 = query_shortcuts.generate_user_shortcut("the_description", "FIND nothing", "id39");
    var globalTemplate = query_shortcuts.generate_shortcut_form("the_other_description", "FIND anything");

    panel.append(header);
    panel.append(userTemplate1);
    panel.append(userTemplate2);
    panel.append(userTemplate3);
    panel.append(globalTemplate);

    var form = query_shortcuts.make_update_form(panel[0],"id28", () => {}, ()=>{}, ()=>{});
    assert.ok($(form).hasClass("caosdb-f-form-wrapper"), "form created");

    $('body').append(form);

    $(form).find(":input[name='templateDescription']").val("NEW DESC");
    $(form).find(":input[name='Query']").val("NEW QUERY");

    var entity = getEntityXML(form);
    assert.equal(xml2str(entity), "<Record id=\"id28\"><Parent name=\"UserTemplate\"/><Property name=\"templateDescription\">NEW DESC</Property><Property name=\"Query\">NEW QUERY</Property></Record>", "entity extracted");

    form_elements.dismiss_form(form);

});

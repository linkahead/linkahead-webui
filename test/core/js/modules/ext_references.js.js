/*
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
/* testing webcaosdb's javascript sources */

/* SETUP ext_references module */
QUnit.module("ext_references.js", {
    before: function(assert) {

        // dummy test functions
        resolve_references.retrieve = async function (id) {
            return transformation.transformEntities(str2xml(
                `<Response>
                  <Record id="${id}" name="TestReferenceObject-${id}">
                    <Parent name="TestReferenced"/>
                  </Record>
                </Response>`
            ));
        };

        /**
         * Example implementation of a function which returns a reference_info
         * for referenced `TestReferenceObject` entities.
         * `TestReferenceObject` is a mock-up entity which is used to test this
         * module.
         */
        this.test_resolver = function (with_cb) {
            return function (entity) {
                callback = !with_cb || function (ref_infos) {
                    console.log(ref_infos);
                    var ret = $('<div/>').append("Summary: ");
                    var array = [];
                    for (const ref_info of ref_infos) {
                        array.push(parseInt(ref_info["data"]["number"],
                            10));
                    }
                    ret.append(reference_list_summary
                        .simplify_integer_numbers(array));
                    return ret[0];
                }
                return {
                    "text": getEntityName(entity),
                    "data": {
                        "id": getEntityID(entity),
                        "name": getEntityName(entity),
                        "number": getEntityName(entity).replace(
                            "TestReferenceObject-", "")
                    },
                    "callback": callback
                };
            };
        }


    },
    beforeEach: function(assert) {
        resolve_references.getParents = getParents;
        resolve_references.test_resolver = this.test_resolver(true);
        resolve_references.is_in_viewport_horizontally = () => true;
        resolve_references.is_in_viewport_vertically = () => true;
    }
});

QUnit.test("available", function(assert) {
    assert.ok(resolve_references);
});

QUnit.test("init", function(assert){
    assert.ok(resolve_references.init);
});

QUnit.test("is_child", function(assert){
    resolve_references.getParents = (entity) => [{"name": "par1"}, {"name": "par2"}];
    const ent = transformation.transformEntities(str2xml(
                `<Response>
                  <Record id="50023">
                    <Parent name="TestReferenced"/>
                  </Record>
                </Response>`
            ));

    assert.ok(resolve_references.is_child(ent, "par1"));
    assert.ok(resolve_references.is_child(ent, "par2"));
    assert.ok(!resolve_references.is_child(ent, "par3"));
});

QUnit.test("get_person_str", function(assert){
    assert.ok(caosdb_default_person_reference.get_person_str);
});

QUnit.test("update_visible_references_without_summary", async function(assert){
    resolve_references.test_resolver = this.test_resolver(false);
    resolve_references.is_in_viewport_horizontally = () => false;
    const update_visible_refs = resolve_references.update_visible_references;

    const test_property = $(`<div class="caosdb-f-property-value">
        <div data-entity-id="15"
            class="${resolve_references._unresolved_class_name}">
        </div>
    </div><div class="caosdb-f-property-value">
        <div class="caosdb-value-list">
            <div data-entity-id="16"
                class="${resolve_references._unresolved_class_name}">
            </div>
            <div data-entity-id="17"
                class="${resolve_references._unresolved_class_name}">
            </div>
        </div>
    </div>`);

    assert.equal(test_property.find(`.${resolve_references._unresolved_class_name}`).length, 3, "is unresolved");

    await update_visible_refs($("<div/>").append(test_property));

    assert.equal(test_property.find(`.${resolve_references._unresolved_class_name}`).length, 2, "only the first of the list is resolved");
    assert.ok(test_property.find(`[data-entity-id='15']`).is(`.${resolve_references._unresolved_class_name}`), "single is not resolve (not visible horizontally).");
    assert.notOk(test_property.find(`[data-entity-id='16']`).is(`.${resolve_references._unresolved_class_name}`), "first of list is resolved");
    assert.ok(test_property.find(`[data-entity-id='17']`).is(`.${resolve_references._unresolved_class_name}`), "second of list is not resolved");

    // target element was generated
    const target_elem = test_property
        .find(`.${resolve_references._target_class}`);
    assert.equal(target_elem.length, 1, "one target is there");

    // no summary has been generated
    const summary_elem = test_property
        .find(`.${resolve_references._summary_class}`);
    assert.equal(summary_elem.length, 0, "no summary is there");

});

QUnit.test("update_visible_references_with_summary", async function(assert){
    const update_visible_refs = resolve_references.update_visible_references;

    const test_property = $(`<div class="caosdb-f-property-value">
        <div data-entity-id="15"
            class="${resolve_references._unresolved_class_name}">
        </div>
    </div><div class="caosdb-f-property-value">
        <div class="caosdb-value-list">
            <div data-entity-id="16"
                class="${resolve_references._unresolved_class_name}">
            </div>
            <div data-entity-id="17"
                class="${resolve_references._unresolved_class_name}">
            </div>
        </div>
    </div>`);

    assert.equal(test_property.find(`.${resolve_references._unresolved_class_name}`).length, 3, "is unresolved");

    // the summary is created asyncronously and dispatches an event when it's
    // ready.
    var done = assert.async();
    test_property[1].addEventListener(resolve_references.summary_ready_event.type, (e) => {
        assert.equal(e.target.textContent, "Summary: 16, 17", "summary text is correct");
        test_property.remove();
        done();
    }, true);

    await update_visible_refs($(document.body).append(test_property));

    assert.equal(test_property.find(`.${resolve_references._unresolved_class_name}`).length, 0, "is resolved");


    // target element was generated
    const target_elem = test_property
        .find(`.${resolve_references._target_class}`);
    assert.equal(target_elem.length, 3, "three targets are there");

    // summary container is there, this is the case even before the summary
    // itself is ready.
    const summary_elem = test_property
        .find(`.${resolve_references._summary_class}`);
    assert.equal(summary_elem.length, 1, "one summary is there");


});


/**
 * Test the generation of ref_info objects from resolvable references.
 */
QUnit.test("update_single_resolvable_reference", async function(assert) {
    const update_single = resolve_references.update_single_resolvable_reference;

    const test_property = $(`<div class="caosdb-f-property-value">
        <div class="caosdb-id ${resolve_references._unresolved_class_name}">15</div></div>`);

    const ref_info = await update_single(test_property[0]);

    // ref_info has expected fields, data and callback
    assert.equal(ref_info.data.name, "TestReferenceObject-15", "has data");
    assert.equal(ref_info.data.id, "15", "id was parsed correctly");
    assert.equal(typeof ref_info.callback, "function", "has callback");

});


/*
 * This test checks whether all required
 * HTML elements are present.
 */
QUnit.test("check_structure_html", function(assert){
    var done = assert.async();
    let string_test_document = `
<Response>
<Record description="This record has no name.">
    <Parent name="bla" />

    <Property name="A" datatype="INTEGER">245</Property>

    <Property name="A" datatype="Uboot">245</Property>

    <Property name="A" datatype="LIST&#60;Uboot&#62;">
      <Value>245</Value>
      <Value>245</Value>
    </Property>
  </Record>
</Response>
`;
    var xmltestdocument = str2xml(string_test_document);
    transformation.transformEntities(xmltestdocument).then(x => {
        console.log(x);

        // Check that there are only three resolvable references:
        var elms = x[0].getElementsByClassName("caosdb-resolvable-reference");
        assert.equal(elms.length, 3);

        // Check that each of the elements of the list has a grand parent with a specific class:
        for (var i=1; i<elms.length; i++) {
            assert.equal(elms[i].parentElement.parentElement.classList.contains("caosdb-value-list"), true);
        }
        // but the first element does NOT:
        assert.equal(elms[0].parentElement.parentElement.classList.contains("caosdb-value-list"), false);

        done();
    }, err => {console.log(err);});
});

QUnit.module("reference_list_summary");

QUnit.test("simplify_integer_numbers", function(assert) {
    var f = reference_list_summary.simplify_integer_numbers;
    assert.equal(f([1]), "1");
    assert.equal(f([1,1,1]), "1");
    assert.equal(f([1,2]), "1, 2");
    assert.equal(f([2,1]), "1, 2");
    assert.equal(f([2,1,2]), "1, 2");
    assert.equal(f([2,1,2]), "1, 2");
    assert.equal(f([1,2,4]), "1-2, 4");
    assert.equal(f([1,3,4,5]), "1, 3-5");
    assert.equal(f([1,2,3,5,8,9,10]), "1-3, 5, 8-10");
    assert.equal(f([1,2,3,5,7,8,10]), "1-3, 5, 7-8, 10");
    assert.equal(f([1,3,5,7,8,10]), "1, 3, 5, 7-8, 10");
    assert.equal(f([1,2,4,5,7,8,10]), "1-2, 4-5, 7-8, 10");
    assert.equal(f([1,2,4,5,7,9,10]), "1-2, 4-5, 7, 9-10");
});


QUnit.test("generate", function(assert) {
    const f = reference_list_summary.generate;
    const summary_container = $("<div/>");


    // without callback function
    const ref_infos_no_cb = [ {
        "data": { "dummy": "dummy_data1" },
    }, {
        "data": { "dummy": "dummy_data2" },
    }, ];

    assert.ok(typeof f(ref_infos_no_cb) === "undefined", "no cb returns undefined");
    f(ref_infos_no_cb, summary_container);
    assert.equal(summary_container.children().length, 0, "no cb doesn't append to summary container.");
    assert.equal(summary_container.text(), "", "no text in container");

    // with callback function
    const ref_infos_with_cb = [ {
        "callback": function (ref_infos) { 
            return ref_infos.map(i => i.data.dummy).join(",");
        },
        "data": { "dummy": "dummy_data3" },
    }, {
        "data": { "dummy": "dummy_data4" },
    }, ];

    const summary = "dummy_data3,dummy_data4";

    assert.equal(f(ref_infos_with_cb, summary_container), summary, "callback returns summary");
    assert.equal(summary_container.text(), summary, "summary in container");


});

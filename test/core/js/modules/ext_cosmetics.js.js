/*
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2021-2024 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2023 Daniel Hornung <d.hornung@indiscale.com>
 * Copyright (C) 2024 Joscha Schmiedt <joscha@schmiedt.dev>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

'use strict';

QUnit.module("ext_cosmetics.js", {
    before: function (assert) {
        cosmetics.init();
        // setup before module
    },
    beforeEach: function (assert) {
        // setup before each test
    },
    afterEach: function (assert) {
        // teardown after each test
    },
    after: function (assert) {
        // teardown after module
    }
});

QUnit.test("custom datetime", function (assert) {
    assert.ok(cosmetics.custom_datetime, "custom_datetime available");
    var test_cases = [
        ["1234-56-78", "1234-56-78"],
        ["9876-54-32T12:34:56", "9876-54-32 12:34:56"],
        ["2023-03-78T99:99:99+0800", "2023-03-78 99:99:99"],
    ];

    for (let test_case of test_cases) {
        const container = $('<div></div>');
        $(document.body).append(container);
        const text_value = $(`<span class="caosdb-f-property-datetime-value">${test_case[0]}</span>`);
        container.append(text_value);
        assert.equal($(container).find(" ").length, 0, "Test original datetime.");
        cosmetics.custom_datetime();
        const newValueElement =
            container[0].querySelector("span.caosdb-v-property-datetime-customized-newvalue");
        assert.ok(newValueElement, "Datetime customization: Test if result exists.");
        assert.equal(newValueElement.innerHTML, test_case[1],
            "Datetime customization: compared result.");
        container.remove();
    }
});

QUnit.test("linkify - https", function (assert) {
    assert.ok(cosmetics.linkify, "linkify available");
    var test_cases = [
        ["https://link", 1, "https://link",],
        ["this is other text https://link.com", 1, "https://link.com"],
        ["https://link; this is other text", 1, "https://link"],
        ["this is other text https://link.de and this as well", 1, "https://link.de"],
        ["this is other text https://link:3000", 1, "https://link:3000"],
        ["this is other text https://link.org/test, and here comes another link https://link.org/test and more text", 2, "https://link.org/test"],
    ];

    for (let test_case of test_cases) {
        var container = $('<div></div>');
        $(document.body).append(container);
        const text_value = $(`<div class="caosdb-f-property-text-value">${test_case[0]}</div>`);
        container.append(text_value);
        assert.equal($(container).find(`a[href='${test_case[2]}']`).length, 0, "no link present");
        cosmetics.linkify();
        assert.equal($(container).find(`a[href='${test_case[2]}']`).length, test_case[1], `link is present: ${$(container)[0].outerHTML}`);
        container.remove();
    }
});

QUnit.test("linkify - http", function (assert) {
    var test_cases = [
        ["http://link", 1],
        ["this is other text http://link", 1],
        ["http://link this is other text", 1],
        ["this is other text http://link and this as well", 1],
        ["this is other text http://link", 1],
        ["this is other text http://link and here comes another link http://link and more text", 2],
    ];
    for (let test_case of test_cases) {
        const container = $('<div></div>');
        $(document.body).append(container);
        const text_value = $(`<div class="caosdb-f-property-text-value">${test_case[0]}</div>`);
        $(container).append(text_value);
        assert.equal($(container).find("a[href='http://link']").length, 0, "no link present");
        cosmetics.linkify();
        assert.equal($(container).find("a[href='http://link']").length, test_case[1], "link is present");
        container.remove();
    }
});

QUnit.test("linkify cut-off (40)", function (assert) {
    const container = $('<div></div>');
    $(document.body).append(container);
    const test_case = "here is some text https://this.is.a.link/with/more/than/40/characters/ this is more text";
    const text_value = $(`<div class="caosdb-f-property-text-value">${test_case}</div>`);
    $(container).append(text_value);
    assert.equal($(container).find("a").length, 0, "no link present");
    cosmetics.linkify();
    assert.equal($(container).find("a").length, 1, "link is present");
    assert.equal($(container).find("a").text(), "https://this.is.a.link/with/more/th[...] ", "link text has been cut off");
    container.remove();
});

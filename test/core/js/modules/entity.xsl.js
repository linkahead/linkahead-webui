/*
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
/* Testing the construction of the html elements for entities via xsl transformation */

/* SETUP */
QUnit.module("entity.xsl", {
    before: function(assert) {
        // load entity.xsl
        var done = assert.async();
        var qunit_obj = this;
        _retrieveEntityXSL().then(function(xsl) {
            qunit_obj.entityXSL = xsl
            done();
        });
    }
});

async function _retrieveEntityXSL() {
    var entityXsl = await transformation.retrieveXsltScript("entity.xsl");
    var commonXsl = await transformation.retrieveXsltScript("common.xsl");
    var xsl = transformation.mergeXsltScripts(entityXsl, [commonXsl]);
    insertParam(xsl, "entitypath", "/entitypath/");
    insertParam(xsl, "filesystempath", "/filesystempath/");
    return xsl;
}


/* TESTS */
QUnit.test("availability", function(assert) {
    assert.ok(this.entityXSL);
});

QUnit.test("Property names are not links anymore", function(assert) {
    var xsl = injectTemplate(this.entityXSL, '<xsl:template match="/"><xsl:apply-templates select="Property" mode="property-collapsed"><xsl:with-param name="collapseid" select="5678"/></xsl:apply-templates></xsl:template>');
    var xml_str = '<Property name="pname" id="2345" datatype="TEXT">pvalue</Property>';
    var xml = str2xml(xml_str);
    var html = xslt(xml, xsl);
    assert.equal(html.firstElementChild.getElementsByClassName("caosdb-property-name")[0].outerHTML, '<span class=\"caosdb-property-name\">pname</span>', "link there");
});

QUnit.test("TestRecordType data type is recognized as a reference", function(assert) {
    var tmpl = '<xsl:template match="/"><xsl:apply-templates select="Property" mode="property-value"/></xsl:template>';
    var xsl = injectTemplate(this.entityXSL, tmpl);

    var xml_str = '<Property name="TestProperty" id="1234" description="DESC" type="TestRecordType">5678</Property>';
    var xml = str2xml(xml_str);
    var params = {
        entitypath: "/entitypath/"
    };
    html = xslt(xml, xsl, params);
    assert.ok(html, "html is ok.");

    var link_e = html.firstElementChild;
    assert.equal(link_e.tagName, "A", "<a> tag is there.");
    assert.equal(link_e.getAttribute("href"), "/entitypath/5678", "href location");
});

QUnit.test("Property with Permissions tag and type='ExperimentSeries' is recognized as a reference", function(assert) {
    // inject an entrance rule
    var xsl = injectTemplate(this.entityXSL, '<xsl:template match="/"><xsl:apply-templates select="Property" mode="property-value"/></xsl:template>');

    var xml_str = '<Property id="129954" name="ExperimentSeries" description="A collection of [Experiment] records which have the same [aim]." type="ExperimentSeries" importance="FIX">' +
        '\n129950' +
        '\n<Permissions>' +
        '\n<Permission name="USE:AS_REFERENCE" />' +
        '\n<Permission name="UPDATE:QUERY_TEMPLATE_DEFINITION" />' +
        '\n</Permissions>' +
        '\n</Property>';
    var xml = str2xml(xml_str);
    var params = {
        entitypath: "/entitypath/"
    };
    var html = xslt(xml, xsl, params);
    assert.ok(html, "html is ok.");

    var link_e = html.firstElementChild;
    assert.equal(link_e.tagName, "A", "<a> tag is there.");
    assert.equal(link_e.getAttribute("href"), "/entitypath/129950", "href location");
});

QUnit.test("back references", function(assert) {
    // inject an entrance rule
    var xsl = injectTemplate(this.entityXSL, '<xsl:template match="/"><xsl:apply-templates select="*/@id" mode="backreference-link"/></xsl:template>');

    var xml_str = '<Entity id="265"/>';
    var xml = str2xml(xml_str);

    var link = xslt(xml, xsl).firstChild;

    assert.equal(link.tagName, "A", "is a link");
    assert.equal(link.getAttribute("href"), "/entitypath/?P=0L10&query=FIND+Entity+which+references+%22265%22", "query correct.");
});

QUnit.test("Entities have a caosdb-annotation-section", function(assert) {
    let xml_str = '<Record id="2345"></Record>';
    let xml = str2xml(xml_str)
    let html = applyTemplates(xml, this.entityXSL, "entities");
    // inject an entrance rule
    //var xsl = injectTemplate(this.entityXSL, '<xsl:template match="/"><xsl:apply-templates select="Record" mode="top-level-data"/></xsl:template>');

    let secs = html.firstChild.getElementsByClassName("caosdb-annotation-section");
    assert.equal(secs.length, 1, "found one.");
    assert.equal(secs[0].tagName, "UL", "section element is DIV");

    assert.equal(secs[0].getAttribute("data-entity-id"), "2345", "data-entity-id is correct.");
});

QUnit.test("LIST Property", function(assert) {
    var done = assert.async();
    var entityXSL = this.entityXSL;
    assert.expect(4);
    $.ajax({
        cache: true,
        dataType: 'xml',
        url: "xml/test_case_list_of_myrecordtype.xml",
    }).done(function(data, textStatus, jdXHR) {
        console.log(entityXSL);
        var xsl = injectTemplate(entityXSL, '<xsl:template match="/"><xsl:apply-templates select="Property" mode="property-value"/></xsl:template>');
        var params = {
            entitypath: "/entitypath/"
        };
        var ret = xslt(data, xsl, params);
        assert.ok(ret);
        assert.equal(ret.firstChild.className, "caosdb-value-list", "property value contains a list.")
        assert.equal($(ret.firstChild).find(".caosdb-f-property-single-raw-value").length, 29, "29 values in the list");
        assert.equal($(ret.firstChild).find(".caosdb-f-reference-value").length, 28, "28 reference values in the list");
    }).always(function() {
        done();
    });
});

QUnit.test("single-value template with reference property.", function(assert) {
    /* DEPRECATED css class .caosdb-property-text-value - Use
    * .caosdb-f-property-single-raw-value or introduce new
    * .caosdb-v-property-text-value */
    assert.equal(xml2str(callTemplate(this.entityXSL, 'single-value', {
        'value': '',
        'reference': 'true',
        'boolean': 'false'
    })), "<span xmlns=\"http://www.w3.org/1999/xhtml\" class=\"caosdb-f-property-single-raw-value caosdb-property-text-value caosdb-f-property-text-value caosdb-v-property-text-value\"></span>", "empty value produces empty span.");
    let link = callTemplate(this.entityXSL, 'single-value', {
        'value': '1234',
        'reference': 'true',
        'boolean': 'false'
    }).firstElementChild;
    assert.equal(link.tagName, 'A', "is link");
    assert.equal(link.getAttribute('href'), "/entitypath/1234", 'link to 1234');
    assert.equal($(link).find('.caosdb-id').length, 1, 'has caosdb-id span');
})

QUnit.test("single-value template with long text property.", function(assert) {
    assert.equal(xml2str(callTemplate(this.entityXSL, 'single-value', {
        'value': 'the feature is disabled',
        'reference': 'false',
        'boolean': 'false',
        'long-text-threshold': 'DISABLED'
    })), "<span xmlns=\"http://www.w3.org/1999/xhtml\" class=\"caosdb-f-property-single-raw-value caosdb-property-text-value caosdb-f-property-text-value caosdb-v-property-text-value\">the feature is disabled</span>", "disabled -> not in details tag");
    assert.equal(xml2str(callTemplate(this.entityXSL, 'single-value', {
        'value': 'this is too long',
        'reference': 'false',
        'boolean': 'false',
        'long-text-threshold': '4'
    })), "<details xmlns=\"http://www.w3.org/1999/xhtml\"><span class=\"caosdb-f-property-single-raw-value caosdb-property-text-value caosdb-f-property-text-value caosdb-v-property-text-value\">this is too long</span><span class=\"spacer\"></span><summary><div title=\"show more\"><span class=\"caosdb-f-property-text-value\">this</span></div></summary></details>", "too long -> shorted to 'this'");
    assert.equal(xml2str(callTemplate(this.entityXSL, 'single-value', {
        'value': 'this is not too long',
        'reference': 'false',
        'boolean': 'false',
        'long-text-threshold': '140'
    })), "<span xmlns=\"http://www.w3.org/1999/xhtml\" class=\"caosdb-f-property-single-raw-value caosdb-property-text-value caosdb-f-property-text-value caosdb-v-property-text-value\">this is not too long</span>", "not too long -> not details tag.");
})

QUnit.test("old version warning", function(assert) {
    // with successor tag
    var xmlstr = '<Record id="2345"><Version id="abcd1234"><Successor id="bcde2345"/></Version></Record>';
    var xml = str2xml(xmlstr);
    var html = applyTemplates(xml, this.entityXSL, "entities", "*");
    assert.equal($(html).find(".caosdb-entity-panel .caosdb-f-entity-version-old-warning").length, 1, "warning present");

    // with version tag, without successor
    xmlstr = '<Record id="2345"><Version id="abcd1234"/></Record>';
    xml = str2xml(xmlstr);
    html = applyTemplates(xml, this.entityXSL, "entities", "*");
    assert.equal($(html).find(".caosdb-f-entity-version-old-warning").length, 0, "warning not present");

    // without version tag
    xmlstr = '<Record id="2345"></Record>';
    xml = str2xml(xmlstr);
    html = applyTemplates(xml, this.entityXSL, "entities", "*");
    assert.equal($(html).find(".caosdb-f-entity-version-old-warning").length, 0, "warning not present");
});

QUnit.test("version button", function(assert) {
    // with version tag
    var xmlstr = '<Record id="2345"><Version id="abcd1234"/></Record>';
    var xml = str2xml(xmlstr);
    var html = applyTemplates(xml, this.entityXSL, "entities", "*");

    assert.equal($(html).find("div.caosdb-entity-panel button.caosdb-f-entity-version-button").length, 1, "button present");

    // without version tag
    xmlstr = '<Record id="2345"></Record>';
    xml = str2xml(xmlstr);
    html = applyTemplates(xml, this.entityXSL, "entities", "*");
    assert.equal($(html).find(".caosdb-f-entity-version-button").length, 0, "button not present");
});

QUnit.test("version info modal", function(assert) {
    // with version tag
    var xmlstr = '<Record id="2345"><Version id="abcd1234"/></Record>';
    var xml = str2xml(xmlstr);
    var html = applyTemplates(xml, this.entityXSL, "entities", "*");

    assert.equal($(html).find("div.caosdb-entity-panel div.caosdb-f-entity-version-info").length, 1, "info present");

    // without version tag
    xmlstr = '<Record id="2345"></Record>';
    xml = str2xml(xmlstr);
    html = applyTemplates(xml, this.entityXSL, "entities", "*");
    assert.equal($(html).find(".caosdb-f-entity-version-info").length, 0, "info not present");
});

QUnit.test("data-version-id attribute", function(assert) {
    // with version tag
    var xmlstr = '<Record id="2345"><Version id="abcd1234"/></Record>';
    var xml = str2xml(xmlstr);
    var html = applyTemplates(xml, this.entityXSL, "entities", "*");
    assert.equal($(html).find("div.caosdb-entity-panel[data-version-id='abcd1234']").length, 1, "data-version-id attribute present");

    // without version tag
    xmlstr = '<Record id="2345"></Record>';
    xml = str2xml(xmlstr);
    html = applyTemplates(xml, this.entityXSL, "entities", "*");
    assert.equal($(html).find("div.caosdb-entity-panel[data-version-id]").length, 0, "data-version-id attribute not present");
});

QUnit.test("data-version-successor attribute", function(assert) {
    // with successor tag
    var xmlstr = '<Record id="2345"><Version id="abcd1234"><Successor id="bcde2345"/></Version></Record>';
    var xml = str2xml(xmlstr);
    var html = applyTemplates(xml, this.entityXSL, "entities", "*");
    assert.equal($(html).find("div.caosdb-entity-panel[data-version-successor='bcde2345']").length, 1, "data-version-successor attribute present");

    // with version tag, without successor
    xmlstr = '<Record id="2345"><Version id="abcd1234"/></Record>';
    xml = str2xml(xmlstr);
    html = applyTemplates(xml, this.entityXSL, "entities", "*");
    assert.equal($(html).find("div.caosdb-entity-panel[data-version-successor]").length, 0, "data-version-successor attribute not present");

    // without version tag
    xmlstr = '<Record id="2345"></Record>';
    xml = str2xml(xmlstr);
    html = applyTemplates(xml, this.entityXSL, "entities", "*");
    assert.equal($(html).find("div.caosdb-entity-panel[data-version-successor]").length, 0, "data-version-successor attribute not present");
});

QUnit.test("version full history", function (assert) {
    var xmlstr = `
    <Response username="user1" realm="Realm1" srid="31ce8ea1-6c9b-4a82-82ec-9f6f3edd2622" timestamp="1606225647516" baseuri="https://localhost:10443" count="1">
  <Record id="3373" name="TestRecord1-10thVersion" description="This is the 10th version.">
    <Permissions>
      <Permission name="RETRIEVE:HISTORY" />
    </Permissions>
    <Version id="vid6" username="user1" realm="Realm1" date="date6" completeHistory="true">
      <Predecessor id="vid5" username="user1" realm="Realm1" date="date5">
        <Predecessor id="vid4" username="user1" realm="Realm1" date="date4">
          <Predecessor id="vid3" username="user1" realm="Realm1" date="date3">
            <Predecessor id="vid2" username="user1" realm="Realm1" date="date2">
              <Predecessor id="vid1" username="user1" realm="Realm1" date="date1" />
            </Predecessor>
          </Predecessor>
        </Predecessor>
      </Predecessor>
      <Successor id="vid7" username="user1" realm="Realm1" date="date7">
        <Successor id="vid8" username="user1" realm="Realm1" date="date8">
          <Successor id="vid9" username="user1" realm="Realm1" date="date9">
            <Successor id="vid10" username="user1" realm="Realm1" date="date10" />
          </Successor>
        </Successor>
      </Successor>
    </Version>
    <Parent id="3372" name="TestRT" />
  </Record>
</Response>
`;
    var xml = str2xml(xmlstr);
    var html = applyTemplates(xml, this.entityXSL, "entities", "*");
    var version_info = $(html).find(".caosdb-f-entity-version-info");
    var table_elem = $(version_info).find("table");

    var TAB = "%09", NEWL = "%0A", usr = "user1@Realm1",
      path = "/entitypath/3373";
    var export_table = `data:text/csv;charset=utf-8,Entity ID${TAB}Version ID${TAB}Date${TAB}User${TAB}URI${NEWL}3373${TAB}vid10${TAB}date10${TAB}${usr}${TAB}${path}@vid10${NEWL}3373${TAB}vid9${TAB}date9${TAB}${usr}${TAB}${path}@vid9${NEWL}3373${TAB}vid8${TAB}date8${TAB}${usr}${TAB}${path}@vid8${NEWL}3373${TAB}vid7${TAB}date7${TAB}${usr}${TAB}${path}@vid7${NEWL}3373${TAB}vid6${TAB}date6${TAB}${usr}${TAB}${path}@vid6${NEWL}3373${TAB}vid5${TAB}date5${TAB}${usr}${TAB}${path}@vid5${NEWL}3373${TAB}vid4${TAB}date4${TAB}${usr}${TAB}${path}@vid4${NEWL}3373${TAB}vid3${TAB}date3${TAB}${usr}${TAB}${path}@vid3${NEWL}3373${TAB}vid2${TAB}date2${TAB}${usr}${TAB}${path}@vid2${NEWL}3373${TAB}vid1${TAB}date1${TAB}${usr}${TAB}${path}@vid1`
    assert.equal(version_info.length, 1);
    assert.equal(table_elem.length, 1);
    assert.equal(version_history.get_history_tsv(table_elem[0]), export_table);
});

QUnit.test("Transforming abstract properties", function (assert) {
    var xmlstr = `<Property id="3842" description="bla" name="reftotestrt" datatype="TestRT">
    <Version id="04ad505da057603a9177a1fcf6c9efd5f3690fe4" date="2020-11-23T10:38:02.936+0100" />
  </Property>`;
    var xml = str2xml(xmlstr);
    var html = applyTemplates(xml, this.entityXSL, "entity-body", "Property");
    var prop = getPropertyFromElement(html.firstElementChild);
    assert.propEqual(prop, {
        "datatype": "TestRT",
        "html": {},
        "id": "3842",
        "description": "bla",
        "list": false,
        "name": "reftotestrt",
        "reference": true,
        "unit": undefined,
        "value": ""}
    );
});

/* MISC FUNCTIONS */
function applyTemplates(xml, xsl, mode, select = "*") {
    let entryRule = '<xsl:template priority="9" match="/"><xsl:apply-templates select="' + select + '" mode="' + mode + '"/></xsl:template>';
    let modXsl = injectTemplate(xsl, entryRule);
    return xslt(xml, modXsl);
}

function callTemplate(xsl, template, params, wrap_call, root) {
    let entryRuleStart = '<xsl:call-template name="' + template + '">';
    let entryRuleEnd = '</xsl:call-template>';
    var entryRule = entryRuleStart;
    for (name in params) {
        entryRule += '<xsl:with-param name="' + name + '"><xsl:value-of select="\'' + params[name] + '\'"/></xsl:with-param>';
    }
    entryRule += entryRuleEnd;
    if (typeof wrap_call == "function") {
        entryRule = wrap_call(entryRule);
    }
    entryRule = '<xsl:template xmlns="http://www.w3.org/1999/xhtml" priority="9" match="/">' +
        entryRule + '</xsl:template>';
    let modXsl = injectTemplate(xsl, entryRule);
    root = root || '<root/>';
    return xslt(str2xml(root), modXsl);
}

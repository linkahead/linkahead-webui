/**
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2019 IndiScale GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
'use strict';

/**
 * Transform the server's response of a select query request into the html
 * tabular representation. The parameter `xml` may also be a Promise.
 *
 * Only used for testing purposes.
 *
 * @async
 * @param {XMLDocument} xml
 * @return {HTMLElement} DIV.caosdb-query-response
 */
transformation.transformSelectTable = async function _tST (xml) {
    var root_template = '<xsl:template xmlns="http://www.w3.org/1999/xhtml" match="/"><div class="root"><xsl:apply-templates select="Response/Query" mode="query-results"/></div></xsl:template>';
    var queryXsl = await transformation.retrieveXsltScript("query.xsl");
    var entityXsl = await transformation.retrieveEntityXsl(root_template);
    insertParam(entityXsl, "uppercase", 'abcdefghijklmnopqrstuvwxyz');
    insertParam(entityXsl, "lowercase", 'ABCDEFGHIJKLMNOPQRSTUVWXYZ');
    var xsl = transformation.mergeXsltScripts(entityXsl, [queryXsl]);
    var html = await asyncXslt(xml, xsl);
    return $(html).find('div.root')[0];
}

QUnit.module("ext_xls_download", {
    before: function (assert) {
        var done = assert.async();
        var testCase = "table_export/test_case_select_table_1.xml";
        connection
            .get("xml/"+ testCase)
            .then(xml => {
                return transformation.
                    //transformEntities(xml);
                    transformSelectTable(xml);
            }).then(entities => {
                this.test_case_1 = entities;
                done();
            }).catch(err => {
                console.error(err);
                done();
            });

    }
});



QUnit.test("call downloadXLS", async function(assert) {
  var done = assert.async(2);

  // mock server response (successful)
  connection.runScript = async function(exec, param){
      assert.equal(exec, "xls_from_csv.py", "call xls_from_csv.py");
      done();
      return str2xml('<response><script code="0" /><stdout>bla</stdout></response>');
  }

  caosdb_table_export.go_to_script_results = function(filename) {
      assert.equal(filename, "bla", "filename correct");
      done();
  }

  var tsv_data = $('<a id="caosdb-f-query-select-data-tsv" />');
  var modal = $('<div id="downloadModal"><div>');
  $(document.body).append([tsv_data, modal]);


  var xsl_link = $("<a/>");
  downloadXLS(xsl_link[0]);

  await sleep(500);

  tsv_data.remove();
  modal.remove();
});

QUnit.test("_clean_cell", function(assert) {
    assert.equal(caosdb_table_export._clean_cell("\n\t\n\t"), "    ", "No valid content");
});

QUnit.test("_get_property_value", function(assert) {
    var f = caosdb_table_export._get_property_value;

    assert.equal(f().pretty, "", "No pretty content");
    assert.equal(f().raw, "", "No raw content");
    assert.equal(f().summary, "", "No summary");
});


QUnit.test("_get_tsv_string", function(assert) {
    const table = this.test_case_1;
    const entities = $(table).find("tbody tr").toArray();
    assert.equal(entities.length, 3, "three example entities");

    var f = caosdb_table_export._create_tsv_string
    var tsv_string = f(entities, ["Bag", "Number"], true);
    var prefix = "data:text/csv;charset=utf-8,"
    assert.equal(tsv_string,
        "ID\tVersion\tBag\tNumber\n242\tabc123\t6366, 6406, 6407, 6408, 6409, 6410, 6411, 6412, 6413\t02 8   4aaa a\n2112\tabc124\t\t1101\n2112\tabc125\t\t1102", "tsv generated");
    tsv_string = caosdb_table_export._encode_tsv_string(tsv_string);
    assert.equal(tsv_string.slice(0,prefix.length), prefix);
    assert.equal(decodeURIComponent(tsv_string.slice(prefix.length, tsv_string.length)),
        "ID\tVersion\tBag\tNumber\n242\tabc123\t6366, 6406, 6407, 6408, 6409, 6410, 6411, 6412, 6413\t02 8   4aaa a\n2112\tabc124\t\t1101\n2112\tabc125\t\t1102", "tsv generated");
});

QUnit.test("_get_property_value", function (assert) {
    const table = this.test_case_1;
    const entity = $(table).find("tbody tr")[0];

    const property = getProperties(entity)[0].html;

    var f = caosdb_table_export._get_property_value;
    var ret = f(property);
    assert.equal(ret.pretty, "", "pretty is empty");
    assert.equal(ret.summary, "", "summary is empty");
    assert.equal(ret.raw, "6366, 6406, 6407, 6408, 6409, 6410, 6411, 6412, 6413", "raw contains ids");

    var unresolved = property
        .getElementsByClassName(
            resolve_references
                ._unresolved_class_name);

    for (const el of unresolved) {
        var target = resolve_references
            .add_target(el);
        $(target).append("bla");
    }

    ret = f(property);
    assert.equal(ret.pretty, "bla, bla, bla, bla, bla, bla, bla, bla, bla", "pretty is empty");
    assert.equal(ret.summary, "", "summary is empty");
    assert.equal(ret.raw, "6366, 6406, 6407, 6408, 6409, 6410, 6411, 6412, 6413", "raw contains ids");


    var summary = resolve_references.add_summary_field(property);

    ret = f(property);
    assert.equal(ret.pretty, "bla, bla, bla, bla, bla, bla, bla, bla, bla", "pretty shows list of reference info text");
    assert.equal(ret.summary, "", "summary is empty");
    assert.equal(ret.raw, "6366, 6406, 6407, 6408, 6409, 6410, 6411, 6412, 6413", "raw contains ids");

});

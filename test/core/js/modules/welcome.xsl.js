/*
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019 IndiScale GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
/* Testing the construction of the welcome screen */

/* SETUP */
QUnit.module("welcome.xsl", {
	beforeEach : function(assert) {
		// load welcome.xsl
		var done = assert.async();
		var qunit_obj = this;
		$.ajax({
			cache : true,
			dataType : 'xml',
			url : "xsl/welcome.xsl",
		}).done(function(data, textStatus, jdXHR) {
			qunit_obj.welcomeXSL = data;
		}).always(function() {
			done();
		});
	}
});

/* TESTS */
QUnit.test("availability", function(assert) {
	assert.ok(this.welcomeXSL);
})

QUnit.test("welcome template produces .caosdb-v-welcome-panel", function(assert) {
    var xsl = injectTemplate(this.welcomeXSL, '<xsl:template match="/"><xsl:call-template name="welcome"/></xsl:template>');
    var xml_str = '<root>';
    var xml = str2xml(xml_str);
    var html = xslt(xml, xsl);
    assert.ok($(html.firstElementChild).hasClass("caosdb-v-welcome-panel"), "has class .caosdb-v-welcome-panel");
});

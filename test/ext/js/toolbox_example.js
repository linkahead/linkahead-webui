var toolbox_example = function() {

    var init = function() {
        navbar.add_button("Single Button", {callback: ()=>{alert("Single Button");}, title: "Click me!"});
        navbar.add_tool("Tool 1", "Tools", {callback: ()=>{alert("Tool 1");}, title: "Tooltip 1"});
        navbar.add_tool("Tool 2", "Tools", {callback: ()=>{alert("Tool 2");}, title: "Tooltip 2"});
        navbar.add_tool("Tool 3", "Tools", {callback: ()=>{alert("Tool 3");}, title: "Tooltip 3"});

        navbar.add_tool($('<a href="https://indiscale.com">Link1</a>')[0], "Useful Links", {title: "Browse to indiscale.com"});


        const script = "crawler.py"
        const args = {
            "-p0": "positional argument 1",
            "-p1": "positional argument 2",
            "-Ooption1": "option value 1",
            "-Ooption2": "option value 2",
        };
        const button_name = "Trigger Crawler";
        const title = "Trigger the crawler.";

        const crawler_form = make_scripting_caller_form(
            script, args, button_name);

        navbar.add_tool(crawler_form, "Server-side Scripts", {title: title});
    }

    var make_scripting_caller_form = function (script, args, button_name) {
        const scripting_caller = $(`
        <form method="POST" action="/scripting">
          <input type="hidden" name="call" value="${script}"/>
          <input type="submit"
              class="btn btn-link" value="${button_name}"/>
        </form>`);

        // add arguements
        for (const arg in args) {
            scripting_caller.append(`<input type="hidden" name="${arg}"
                value="${args[arg]}"/>`);
        }

        return scripting_caller[0];
    }

    return {
        init: init
    };

}();

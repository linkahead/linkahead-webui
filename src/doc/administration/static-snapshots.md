# Creating Static WebUI Snapshots

It can be helpful to generate static snapshots of WebUI contents, e.g. for reviewing layouts or for presentation purposes. This is possible with a little bit of effort. Excitingly not only the layout can be exported, but also a lot of the javascript functionality can be maintained in the static pages.

**NOTE: This manual page is currently work in progress.**

## Create the static webui folder in the docker container

We need a static version of the LinkAhead webui. In principle it can be simply copied from e.g. a running docker container or from the public-directory. As it contains self-referencing (cyclic) symlinks a little bit of care has to be taken.

### Using Docker

Login to the linkahead docker container as root:
```bash
docker exec -u 0 -ti linkahead /bin/bash
```

We need to be root (`-u 0`) in order to be able to create a copy of linkahead-webui within the container.

Create the copy using `cp` and the option for following symlinks `-L`:

```bash
cp -L git/linkahead-server/linkahead-webui/public/ webui-copy
```

It will warn you that two symlinks (which are cyclic) cannot be created. That's fine, we will create these two symlinks later.

```
cp: cannot copy cyclic symbolic link 'git/linkahead-server/linkahead-webui/public/1602145811'  <- The number here is a "unique" build number
cp: cannot copy cyclic symbolic link 'git/linkahead-server/linkahead-webui/public/webinterface'
```

**Please copy the build number somewhere, or make sure your terminal history does not get wiped.**

Copy webui-copy from the docker container to the location where you want to store the snapshots:
`docker cp linkahead:/opt/caosdb/webui-copy/ .`

Create the two missing symlinks in webui-copy/public:
```
ln -s webui-copy/public webui-copy/public/1602145811
ln -s webui-copy/public webui-copy/public/webinterface
```

You can now use the included xslt stylesheet to convert xml files to html using:
```bash
xsltproc webui-copy/public/webcaosdb.xsl test.xml > test.html
```

As the generated html file still contains invalid references to `/webinterface/1602145811`
you have to replace all occurences of `/webinterface` with webui-copy/public`.

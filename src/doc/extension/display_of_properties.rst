Tweaking the display of properties
==================================

Hide or show properties for specific roles and users
****************************************************

.. note::

   This feature was introduced LinkAhead WebUI 0.10.

.. warning::

   Hiding properties is purely cosmetics and should **never** be considered a
   security feature. The hidden properties are still communicated with the server.

Sometimes it is desirable to hide certain properties for specific users or
roles, e.g., when they might be irrelevant or confusing. For example, an
internal id might only be of interest to curators or administrators, whereas it
is entirely meaningless to most other users.

To configure the hiding of properties, you first need to enable the build
variable ``BUILD_MODULE_EXT_PROPERTY_DISPLAY``. Then, the display of the
properties is configured in ``conf/ext/json/ext_prop_display.json``. In there,
properties of a specific RecordType can be hidden by specifying the name of the
property, and the names of the roles and/or users from whom it should be hidden.

.. code-block:: json

   {
     "RecordTypeName": {
       "hide": [
         {
           "name": "property name",
           "roles": ["list", "of", "roles"],
           "users": ["list", "of", "users"]
         },
         // ...
       ]
     },
     // ...
   }

In the same way but using the ``show`` keyword, properties can be hidden for
everyone **but** the specified users/roles:

.. code-block:: json

   {
     "RecordTypeName": {
       "show": [
         {
           "name": "property name",
           "roles": ["list", "of", "roles"],
           "users": ["list", "of", "users"]
         },
         // ...
       ]
     },
     // ...
   }
 

For example, using the data from demo.indiscale.com, the following would hide
the ``price`` of all ``MusicalInstruments`` from ``anonymous`` and their
``Manufacturer`` from the ``admin`` user.

.. code-block:: json

   {
     "MusicalInstrument": {
       "hide": [
         {"name": "price", "roles": ["anonymous"], "users": []},
         {"name": "Manufacturer", "roles": [], "users": ["admin"]}
       ]
     }
   }

Defining the order of properties
********************************

Similar to above, the order in which properties are displayed can also be
specified. Again, the build variable ``BUILD_MODULE_EXT_PROPERTY_DISPLAY`` has
to be enabled. Then, the order in which the properties are displayed can be
configured in the same configuration file as above, i.e., in
``conf/ext/json/ext_prop_display.json``:

.. code-block:: json

   {
     "RecordTypeName": {
       "order": ["ordered", "list", "of", "properties"]
     },
     // ...
   }

This ensures that the properties of all entities with the declared types are
displayed in the defined order if present. Additional properties, that are not
part of this list are appended in their original order as returned from the LinkAhead
server.

Using again the data of demo.indiscale.com for an example, a configuration might
look the following:

.. code-block:: json

   {
     "MusicalInstrument": {
       "order": ["price", "Manufacturer"]
     }
   }

In all ``MusicalInstrument`` entities, the ``price`` would then be shown first,
then the ``Manufacturer``, and then all remaining properties. If it doesn't have
a ``price`` property, ``Manufacturer`` is shown on top.

Of course, this feature can be combined with the hiding of properties:

.. code-block:: json

   {
     "MusicalInstrument": {
       "hide": [
         {"name": "price", "roles": ["anonymous"], "users": []},
         {"name": "Manufacturer", "roles": [], "users": ["admin"]}
       ],
       "order": ["price", "Manufacturer"]
     }
   }


In this example, ``price`` would still be displayed on top of the list of
property in every ``MusicalInstrument`` entity, but it is hidden for the
``anonymous`` role.

Future
******

In the future, this feature will be extended to allow to `toggle
<https://gitlab.com/linkahead/linkahead-webui/-/issues/190>`__ properties of
predifined RecordTypes.

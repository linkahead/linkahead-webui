Enable GRPC Entity Service
==========================

You code against the LinkAhead GRPC API via the grpc-entity-service module.

Enable it via the build variable:

.. code-block::

    BUILD_MODULE_EXT_GRPC_ENTITY_SERVICE=ENABLED

You can, among other things,

1. create a new TransactionService:

.. code-block:: javascript

    var service = new window.entityService.TransactionService(
        window.connection.getBasePath() + "api");

2. execute a query:

.. code-block:: javascript

    var response = await service.executeQuery("FIND Something");

3. Use the Entity class to wrapp the "Entity" WebGRPC-Message:

.. code-block:: javascript

    var entity = new windown.entityService.Entity(
        response.getResponsesList()[0]
                .getRetrieveResponse()
                .getFindQueryResult()
                .getResultSetList()[0]
                .getEntityResponse()
                .getEntity());


More information on the TransactionService class: https://gitlab.indiscale.com/caosdb/src/caosdb-webui-entity-service/-/blob/main/src/TransactionService.js

More information on the API: https://docs.indiscale.com/caosdb-proto/packages/caosdb.entity.v1.html#caosdb-entity-v1-main-proto

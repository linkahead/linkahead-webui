# XSLT Debugging

The LinkAhead WebUI uses [XSLT](https://en.wikipedia.org/wiki/XSLT) to transform the servers response into a web page.
In the webui-repository these XSLT stylesheets can be found in `src/core/` and `src/core/xsl`.

The XSLT stylesheet is typically interpreted on the client side, e.g. in Mozilla Firefox. Error output of the browser regarding XSLT problems are typically hard to debug. For example, Firefox typically does not print detailed information about the location of an exception in the sourcecode.

So what options do we have to debug xslt stylesheets?

* So called "printf-style" debugging
* Using a different xslt processor

I found this thread on Stack Overflow very helpful:
https://stackoverflow.com/questions/218522/tools-for-debugging-xslt

# "printf-style" debugging

As mentioned in the Stack Overflow thread referenced above, `<xsl:message>` can be used to output debugging messages during XSLT processing.

# Using different XSLT processors

## xsltproc from libxslt

`xsltproc` is a tool from libxslt that allows transforming XML using XSLT stylesheets on the command line. It is called using:
```bash
xsltproc <stylesheet> <xmlfile>
```

So a possible workflow for debugging an xslt script could be:
* Save the test response from the server as `test.xml`.
* Run `make` in repository `linkahead-webui`
* Go to folder `public` in `linkahead-webui`
* Run: `xsltproc webcaosdb.xsl test.xml`

# How to add a module to LinkAhead WebUI
The LinkAhead WebUI is organized in modules which can easily be added and enabled or disabled per module.

Only a few steps are necessary to create a new module.

## Create the module file

Create a new file for each new module.  We have the convention that extensions which are optional
(and also custom extensions for special purposes) are saved in files starting with `ext_`,
e.g. `ext_flight_preview.js`.

This file should define one function that wraps everything, this function is then
enabled at the bottom of the file:

```js
/*
 * ** header with license infoc
 * ...
 */

'use strict';

/**
 * description of the module ...
 *
 * @module ext_flight_preview
 * @version 0.1
 *
 * @requires somelibrary
 * (pass the dependencies as arguments)
 */
const ext_flight_preview = function (libA, libB) {

    const init = function () {
        /* initialization of the module */
    }

    /* doc string */
    const some_function = function (arg1, arg2) {
    }

    /* the main function must return the initialization of the module */
    return {
        init: init,
    };
//pass the dependencies as arguments here as well
}(libA, libB);

// this will be replaced by require.js in the future.
$(document).ready(function() {
    // use a variable starting with `BUILD_MODULE_` to enable your module
    // the build variable has to be enabled in the `build.properties.d/` directory.
    // Otherwise the module will not be activated.
    if ("${BUILD_MODULE_EXT_BOTTOM_LINE}" === "ENABLED") {
        caosdb_modules.register(ext_flight_preview);
    }
});
```

## Install the module

The new new file should be placed in `src/core/js` if it is intended to be merged into the main repository eventually. For development purposes and for custom extensions which are not to be published you may place it in `src/ext/js`.

Everything inside `src/core/js` and `src/ext/js` will eventually being loaded.
So, if there are no other modules which depend on this particular new module,
you are done.

Otherwise, when we need to configure the order in which the
module is being loaded.


### Dependency order

#### For Upstream Code

For modules which are about to be merged into the main or dev branch of this
repository, add the module's file to `build.properties.d/00_default.properties`
at the right location in the list of module files (Array
`MODULE_DEPENDENCIES`). The list defines the order in which module files are
being loaded.

#### For Custom Extensions

For modules which will not be published and merged with the main repository you
may append all your module files in the desired order to the
`MODULE_DEPENDENCIES` array in a new `*.properties` file (e.g.
`build.properties.d/99_local_stuff`):

    MODULE_DEPENDENCIES+=(libA.js libB.js ext_flight_preview.js)

In this example, `libA.js`, `libB.js` and `ext_flight_preview.js` are custom modules developed for this particular LinkAhead webui instance.

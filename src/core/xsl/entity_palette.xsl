<?xml version="1.0" encoding="UTF-8"?>
<!-- Simplified interface for editing data models --><!-- A. Schlemmer, 01/2019 --><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html"/>

  <xsl:template match="/Response">
    <div title="Drag and drop Properties from this panel to the Entities on the left." class="caosdb-v-editmode-existing caosdb-f-edit-mode-existing d-none">
      <div class="card-header">
        <span class="card-title">Existing Properties</span>
      </div>
      <div class="card">
        <div class="input-group" style="width: 100%;">
          <input class="form-control" placeholder="filter..." title="Type a name (full or partial)." oninput="edit_mode.filter('properties');" id="caosdb-f-filter-properties" type="text"/>
        </div>
        <ul class="caosdb-v-edit-list list-group">
          <xsl:apply-templates select="./Property"/>
        </ul>
      </div>
    </div>
    <div title="Drag and drop RecordTypes from this panel to the Entities on the left." class="caosdb-v-editmode-existing caosdb-f-edit-mode-existing d-none">
      <div class="card-header">
        <span class="card-title">Existing RecordTypes</span>
      </div>
      <div class="card">
        <div class="input-group" style="width: 100%;">
          <input class="form-control" placeholder="filter..." title="Type a name (full or partial)." oninput="edit_mode.filter('recordtypes');" id="caosdb-f-filter-recordtypes" type="text"/>
        </div>
        <ul class="caosdb-v-edit-list list-group">
          <xsl:apply-templates select="./RecordType"/>
        </ul>
    </div>
  </div>
  </xsl:template>

  <xsl:template match="RecordType">
    <xsl:if test="string-length(@name)>0">
        <li draggable="true" class="caosdb-f-edit-drag list-group-item caosdb-v-edit-drag">
        <xsl:attribute name="id">caosdb-f-edit-rt-<xsl:value-of select="@id"/></xsl:attribute>
        <xsl:value-of select="@name"/>
        </li>
    </xsl:if>
  </xsl:template>

  <xsl:template match="Property">
    <xsl:choose>
      <xsl:when test="@name='name'">
          <!-- ignore name property -->
      </xsl:when>
      <xsl:when test="@name='description'">
          <!-- ignore description property -->
      </xsl:when>
      <xsl:when test="@name='unit'">
          <!-- ignore unit property -->
      </xsl:when>
      <xsl:otherwise>
        <li draggable="true" class="caosdb-f-edit-drag list-group-item caosdb-v-edit-drag">
          <xsl:attribute name="id">caosdb-f-edit-p-<xsl:value-of select="@id"/></xsl:attribute>
          <xsl:value-of select="@name"/>
        </li>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>

<?xml version="1.0" encoding="utf8"?>
<!--
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html"/>
  <xsl:template match="/Response">
      <xsl:apply-templates select="./RecordType"/>
  </xsl:template>
  <xsl:template match="RecordType">
      <span class="badge caosdb-parent-item me-1">
    <xsl:attribute name="id">
    <xsl:value-of select="generate-id()"/>
    </xsl:attribute>
    <span class="caosdb-f-parent-actions-panel">
    </span>
    <a class="caosdb-parent-name">
      <xsl:attribute name="href">
        <xsl:value-of select="concat($entitypath, @id)"/>
      </xsl:attribute>
      <xsl:value-of select="@name"/>
    </a>
      </span>
  </xsl:template>
</xsl:stylesheet>

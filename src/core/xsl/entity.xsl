<?xml version="1.0" encoding="utf-8"?>
<!--
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2022 Indiscale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2022 Florian Spreckelsen <f.spreckelsen@indiscale.com>
 * Copyright (C) 2022 Daniel Hornung <d.hornung@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html"/>
  <!-- These little colored Rs, RTs, Ps, and Fs which hilite the beginning 
        of a new entity. -->
  <xsl:template match="Entity" mode="entity-heading-label">
    <span class="badge caosdb-f-entity-role caosdb-label-entity me-1"
    title="This is an entity. The role is not specified.">E</span>
  </xsl:template>
  <xsl:template match="Property" mode="entity-heading-label">
    <span class="badge caosdb-f-entity-role caosdb-label-property me-1"
    data-entity-role="Property" title="This entity is a Property.">P</span>
  </xsl:template>
  <xsl:template match="Record" mode="entity-heading-label">
    <span class="badge caosdb-f-entity-role caosdb-label-record me-1"
    data-entity-role="Record" title="This entity is a Record.">R</span>
  </xsl:template>
  <xsl:template match="RecordType" mode="entity-heading-label">
    <span class="badge caosdb-f-entity-role caosdb-label-recordtype me-1"
    data-entity-role="RecordType" title="This entity is a Record Type.">RT</span>
  </xsl:template>
  <xsl:template match="File" mode="entity-heading-label">
    <span class="badge caosdb-f-entity-role caosdb-label-file me-1"
    data-entity-role="File" title="This entity is a File.">F</span>
  </xsl:template>
  <xsl:template match="@id" mode="backreference-link">
    <a class="caosdb-backref-link btn caosdb-id-button" title="Find all entities which reference this one.">
      <xsl:attribute name="href">
        <xsl:value-of select="concat($entitypath, '?P=0L10&amp;query=FIND+Entity+which+references+%22', current(), '%22')"/>
      </xsl:attribute>
      <span class="bg-dark badge d-none d-sm-inline">
        <i class="bi-link"></i> References
      </span>
      <i class="bi-link d-inline d-sm-none"></i>
    </a>
  </xsl:template>
  <!-- special entity properties like type, checksum, path... -->
  <xsl:template match="@datatype" mode="entity-heading-attributes-datatype">
    <p class="caosdb-entity-heading-attr small">
      <em class="caosdb-entity-heading-attr-name">data type:</em>
      <xsl:value-of select="."/>
    </p>
  </xsl:template>
  <xsl:template match="@checksum" mode="entity-heading-attributes-checksum">
    <p class="caosdb-entity-heading-attr caosdb-overflow-box small">
      <em class="caosdb-entity-heading-attr-name">
        <xsl:value-of select="concat(name(),':')"/>
      </em>
      <span class="caosdb-checksum caosdb-overflow-content">
        <xsl:value-of select="."/>
      </span>
    </p>
  </xsl:template>
  <xsl:template match="@path" mode="entity-heading-attributes-path">
    <p class="caosdb-entity-heading-attr small">
      <em class="caosdb-entity-heading-attr-name">
        <xsl:value-of select="concat(name(),':')"/>
      </em>
      <a>
        <xsl:call-template name="make-filesystem-link">
          <xsl:with-param name="href" select="."/>
        </xsl:call-template>
      </a>
    </p>
  </xsl:template>
  <!-- Any further entity attributes -->
  <xsl:template match="@*" mode="entity-heading-attributes">
    <p class="caosdb-entity-heading-attr small">
      <em class="caosdb-entity-heading-attr-name">
        <xsl:value-of select="concat(name(),':')"/>
      </em>
      <xsl:value-of select="."/>
    </p>
  </xsl:template>
  <xsl:template match="*" mode="entity-action-panel">
    <div class="caosdb-entity-actions-panel text-end btn-group-sm">
        <xsl:apply-templates select="Version/Successor" mode="entity-action-panel-version">
          <xsl:with-param name="entityId" select="@id"/>
        </xsl:apply-templates>
    </div>
  </xsl:template>
  <!-- Main entry for ENTITIES -->
  <xsl:template match="Property|Record|RecordType|File|Response/Entity" mode="entities">
    <div class="card caosdb-entity-panel mb-2">
      <xsl:apply-templates select="Version" mode="entity-version-marker"/>
      <xsl:attribute name="id">
        <xsl:value-of select="@id"/>
      </xsl:attribute>
      <xsl:attribute name="data-entity-id">
        <xsl:value-of select="@id"/>
      </xsl:attribute>
      <xsl:if test="State">
        <xsl:attribute name="data-state-model"><xsl:value-of select="State/@model"/></xsl:attribute>
        <xsl:attribute name="data-state-name"><xsl:value-of select="State/@name"/></xsl:attribute>
        <xsl:attribute name="data-state-id"><xsl:value-of select="State/@id"/></xsl:attribute>
      </xsl:if>
      <xsl:apply-templates mode="entity-permissions" select="Permissions"/>
      <!-- A page-unique ID for this entity -->
      <xsl:variable name="entityid" select="concat('entity_',generate-id())"/>
      <div class="card-header caosdb-entity-panel-heading">
        <xsl:attribute name="data-entity-datatype">
          <xsl:value-of select="@datatype"/>
        </xsl:attribute>
        <div class="d-flex flex-wrap align-items-baseline">
              <xsl:apply-templates mode="entity-heading-label" select="."/>
              <!-- Parents -->
              <span class="caosdb-f-parent-list">
                <xsl:if test="Parent">
                  <!-- <xsl:apply-templates select="Parent" mode="entity-body" /> -->
                  <xsl:for-each select="Parent">
                    <span class="badge caosdb-parent-item me-1">
                      <!-- TODO lots of code duplication with parent.xsl -->
                      <xsl:attribute name="id">
                        <xsl:value-of select="generate-id()"/>
                      </xsl:attribute>
                      <span class="caosdb-f-parent-actions-panel"></span>
                      <a class="caosdb-parent-name" title="Go to this parent of the Entity.">
                        <xsl:attribute name="href">
                          <xsl:value-of select="concat($entitypath, @id)"/>
                        </xsl:attribute>
                        <xsl:value-of select="@name"/>
                      </a>
                    </span>
                  </xsl:for-each>
                </xsl:if>
              </span>
              <a class="caosdb-label-link" title="Open this Entity separately.">
                <xsl:attribute name="href">
                  <xsl:value-of select="concat($entitypath, @id)"/>
                </xsl:attribute>
                <span class="caosdb-label-id"><xsl:value-of select="@id"/></span>
                <span class="caosdb-label-name">
                  <xsl:value-of select="@name"/>
                </span>
              </a>
            <div class="caosdb-v-entity-header-buttons-list ms-auto">
              <xsl:apply-templates mode="entity-heading-attributes-state" select="State">
                <xsl:with-param name="entityId" select="@id"/>
                <xsl:with-param name="hasSuccessor" select="Version/Successor"/>
              </xsl:apply-templates>
              <xsl:apply-templates mode="backreference-link" select="@id"/>
              <!-- Button for expanding/collapsing the comments section-->
              <button class="btn caosdb-v-entity-comment-badge" data-bs-toggle="collapse" title="Toggle the comments section at the bottom of this entity.">
                <xsl:attribute name="data-bs-target">
                  <xsl:value-of select="concat('#', 'comment_', $entityid)"/>
                </xsl:attribute>
                <i class="bi-chat-left-fill"/>
              </button>
              <span class="badge bg-dark caosdb-id caosdb-id-button d-none">
                <xsl:value-of select="@id"/>
              </span>
              <button class="btn caosdb-v-bookmark-button">
                <xsl:attribute name="data-bmval">
                  <xsl:value-of select="@id"/>
                  <xsl:if test="Version/Successor">
                    <!-- this is not the head -->
                    <xsl:value-of select="concat('@', Version/@id)"/>
                  </xsl:if>
                </xsl:attribute>
                <i class="bi-bookmark-fill"></i>
              </button>
              <xsl:apply-templates mode="entity-heading-attributes-version" select="Version">
                <xsl:with-param name="entityId" select="@id"/>
              </xsl:apply-templates>
            </div>
        </div>
        <xsl:apply-templates mode="entity-heading-attributes" select="@description"/>
        <xsl:apply-templates mode="entity-heading-attributes-datatype" select="@datatype"/>
        <xsl:apply-templates mode="entity-heading-attributes" select="@*[not(contains('+checksum+size+cuid+id+name+description+datatype+path+',concat('+',name(),'+')))]"/>
        <xsl:if test="@*[contains('+path+checksum+size+',concat('+',name(),'+'))]">
          <details>
            <summary><small>File Details</small></summary>
            <xsl:apply-templates mode="entity-heading-attributes-path" select="@path"/>
            <xsl:apply-templates mode="entity-heading-attributes-checksum" select="@checksum"/>
            <xsl:apply-templates mode="entity-heading-attributes" select="@size"/>
          </details>
        </xsl:if>
      </div>
      <xsl:apply-templates mode="entity-action-panel" select="."/>
      <div class="card-body caosdb-entity-panel-body">
        <!-- Messages -->
        <div class="caosdb-messages">
          <xsl:apply-templates select="Error">
            <xsl:with-param name="class" select="'alert-danger'"/>
          </xsl:apply-templates>
          <xsl:apply-templates select="Warning">
            <xsl:with-param name="class" select="'alert-warning'"/>
          </xsl:apply-templates>
          <xsl:apply-templates select="Info">
            <xsl:with-param name="class" select="'alert-info'"/>
          </xsl:apply-templates>
        </div>
        <!-- Properties -->
        <ul class="list-group caosdb-properties">
          <xsl:if test="Property">
            <xsl:apply-templates mode="entity-body" select="Property"/>
          </xsl:if>
        </ul>
      </div>
      <!-- Annotations -->
      <xsl:call-template name="annotation-section">
        <xsl:with-param name="entityId" select="@id"/>
        <xsl:with-param name="collapseId" select="concat('comment_', $entityid)"/>
      </xsl:call-template>
    </div>
  </xsl:template>
  <!-- PROPERTIES -->
  <xsl:template match="Property" mode="entity-body">
    <li>
      <xsl:choose>
        <xsl:when test="'${BUILD_MODULE_EXT_PROPERTY_DISPLAY}'='ENABLED'">
          <xsl:attribute name="class">list-group-item caosdb-v-property-row caosdb-f-entity-property caosdb-v-hidden-property</xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="class">list-group-item caosdb-v-property-row caosdb-f-entity-property</xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:attribute name="id">
        <xsl:value-of select="generate-id()"/>
      </xsl:attribute>
      <xsl:variable name="collapseid" select="concat('collapseid-',generate-id())"/>
      <!-- property heading and value -->
      <xsl:apply-templates mode="property-collapsed" select=".">
        <xsl:with-param name="collapseid" select="$collapseid"/>
      </xsl:apply-templates>
      <!-- messages -->
      <xsl:apply-templates select="Error">
        <xsl:with-param name="class" select="'alert-danger'"/>
      </xsl:apply-templates>
      <xsl:apply-templates select="Warning">
        <xsl:with-param name="class" select="'alert-warning'"/>
      </xsl:apply-templates>
      <xsl:apply-templates select="Info">
        <xsl:with-param name="class" select="'alert-info'"/>
      </xsl:apply-templates>
        </li>
  </xsl:template>
  <xsl:template match="Property" mode="property-collapsed">
    <xsl:param name="collapseid"/>
    <div class="row">
      <div class="col-sm-6 col-md-4 caosdb-v-property-left-col">
          <xsl:if test="@*[not(contains('+cuid+id+name+',concat('+',name(),'+')))]">
            <i data-bs-toggle="collapse" style="position: absolute; left: 1rem;" class="bi-caret-down-square fs-6 caosdb-clickable">
              <xsl:attribute name="data-bs-target">
                <xsl:value-of select="concat('#',$collapseid)"/>
              </xsl:attribute>
            </i>
          </xsl:if>
          <span class="caosdb-property-name"> <xsl:value-of select="@name"/></span>
      </div>
      <!-- collapsed data -->
      <div class="collapse order-sm-last">
        <xsl:attribute name="id">
          <xsl:value-of select="$collapseid"/>
        </xsl:attribute>
        <hr class="caosdb-subproperty-divider"/>
        <dl class="row caosdb-v-entity-property-attributes">
        <xsl:apply-templates mode="property-attributes-desc" select="@description"/>
        <xsl:apply-templates mode="property-attributes-id" select="@id"/>
        <xsl:apply-templates mode="property-attributes-type" select="@datatype"/>
        <xsl:apply-templates mode="property-attributes" select="@*[not(contains('+cuid+id+name+description+datatype+',concat('+',name(),'+')))]"/>
        </dl>
      </div>
      <!-- property value -->
      <div class="col-sm-6 col-md-8 caosdb-f-property-value">
        <xsl:apply-templates mode="property-value" select="."/>
      </div>
    </div>
  </xsl:template>
  <xsl:template name="single-value">
    <xsl:param name="value"/>
    <xsl:param name="reference"/>
    <xsl:param name="boolean"/>
    <xsl:param name="datetime"/>
    <xsl:param name="long-text-threshold"/>
    <xsl:choose>
      <xsl:when test="normalize-space($value)!=''">
        <xsl:choose>
          <xsl:when test="$reference='true' and normalize-space($value)!=''">
            <!-- this is a reference -->
            <a class="btn btn-outline-dark btn-sm caosdb-f-reference-value caosdb-resolvable-reference">
              <xsl:attribute name="href">
                <xsl:value-of select="concat($entitypath,normalize-space($value))"/>
              </xsl:attribute>
              <xsl:element name="span">
              <xsl:attribute name="class">
                <xsl:value-of select="'caosdb-f-property-single-raw-value caosdb-id caosdb-id-button'"/>
                </xsl:attribute>
                <xsl:value-of select="normalize-space($value)"/>
              </xsl:element>
            </a>
          </xsl:when>
          <xsl:when test="$boolean='true'">
            <xsl:element name="span">
              <xsl:attribute name="class">
                <xsl:value-of select="concat('caosdb-f-property-single-raw-value caosdb-boolean-',normalize-space($value)='TRUE')"/>
              </xsl:attribute>
              <xsl:value-of select="normalize-space($value)"/>
            </xsl:element>
          </xsl:when>
          <xsl:when test="$datetime='true'">
            <xsl:element name="span">
              <xsl:attribute name="class">
                <xsl:value-of select="'caosdb-f-property-single-raw-value caosdb-property-datetime-value caosdb-f-property-datetime-value caosdb-v-property-datetime-value'"/>
              </xsl:attribute>
              <xsl:value-of select="$value"/>
            </xsl:element>
          </xsl:when>
          <!-- DEPRECATED css class .caosdb-property-text-value - Use
               .caosdb-f-property-single-raw-value or introduce new 
               .caosdb-v-property-text-value -->
          <xsl:otherwise>
            <xsl:choose>
              <xsl:when test="$long-text-threshold != 'DISABLED' and string-length(normalize-space($value))&gt;$long-text-threshold">
                <details>
                  <span class="caosdb-f-property-single-raw-value caosdb-property-text-value caosdb-f-property-text-value caosdb-v-property-text-value">
                    <xsl:call-template name="trim">
                      <xsl:with-param name="str">
                        <xsl:value-of select="$value"/>
                      </xsl:with-param>
                    </xsl:call-template>
                  </span>
                  <span class="spacer"></span>
                    <summary><div title="show more"><span class="caosdb-f-property-text-value"><xsl:value-of select="substring(normalize-space($value),0,$long-text-threshold + 1)"/></span></div></summary>
                </details>
              </xsl:when>
              <xsl:otherwise>
                <span class="caosdb-f-property-single-raw-value caosdb-property-text-value caosdb-f-property-text-value caosdb-v-property-text-value">
                  <xsl:call-template name="trim">
                    <xsl:with-param name="str">
                      <xsl:value-of select="$value"/>
                    </xsl:with-param>
                  </xsl:call-template>
                </span>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <!-- this is for empty values -->
      <xsl:otherwise>
        <!-- DEPRECATED css class .caosdb-property-text-value - Use
             .caosdb-f-property-single-raw-value or introduce new 
             .caosdb-v-property-text-value -->
        <span class="caosdb-f-property-single-raw-value caosdb-property-text-value caosdb-f-property-text-value caosdb-v-property-text-value"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template match="Property" mode="property-reference-value-list">
    <div class="caosdb-value-list">
      <xsl:element name="div">
        <xsl:attribute name="class">btn-group btn-group-sm caosdb-overflow-content</xsl:attribute>
        <xsl:for-each select="Value">
          <xsl:call-template name="single-value">
            <xsl:with-param name="reference">
              <xsl:value-of select="'true'"/>
            </xsl:with-param>
            <xsl:with-param name="value">
              <xsl:value-of select="text()"/>
            </xsl:with-param>
            <xsl:with-param name="boolean">
              <xsl:value-of select="'false'"/>
            </xsl:with-param>
            <xsl:with-param name="datetime">
              <xsl:value-of select="'false'"/>
            </xsl:with-param>
          </xsl:call-template>
        </xsl:for-each>
        <xsl:for-each select="Record|RecordType|File|Property">
          <xsl:call-template name="single-value">
            <xsl:with-param name="reference">
              <xsl:value-of select="'true'"/>
            </xsl:with-param>
            <xsl:with-param name="value">
              <xsl:value-of select="@id"/>
            </xsl:with-param>
            <xsl:with-param name="boolean">
              <xsl:value-of select="'false'"/>
            </xsl:with-param>
            <xsl:with-param name="datetime">
              <xsl:value-of select="'false'"/>
            </xsl:with-param>
          </xsl:call-template>
        </xsl:for-each>
      </xsl:element>
    </div>
  </xsl:template>
  <xsl:template match="Property" mode="property-value-list">
    <xsl:param name="reference"/>
    <div class="caosdb-value-list">
      <xsl:element name="ol">
        <xsl:attribute name="class">list-group list-inline</xsl:attribute>
        <xsl:for-each select="Value">
          <xsl:element name="li">
            <xsl:attribute name="class">list-inline-item</xsl:attribute>
            <xsl:call-template name="single-value">
              <xsl:with-param name="reference">
                <xsl:value-of select="'false'"/>
              </xsl:with-param>
              <xsl:with-param name="value">
                <xsl:value-of select="text()"/>
              </xsl:with-param>
              <xsl:with-param name="boolean">
                <xsl:value-of select="../@datatype='LIST&lt;BOOLEAN>'"/>
              </xsl:with-param>
              <xsl:with-param name="datetime">
                <xsl:value-of select="../@datatype='LIST&lt;DATETIME>'"/>
              </xsl:with-param>
              <xsl:with-param name="long-text-threshold">
                <xsl:value-of select="'${BUILD_LONG_TEXT_PROPERTY_THRESHOLD_LIST}'"/>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:element>
        </xsl:for-each>
      </xsl:element>
    </div>
  </xsl:template>
  <xsl:template match="Property" mode="property-value">
    <xsl:choose>
      <!-- filter out collection data types -->
      <xsl:when test="contains(concat('&lt;',@datatype),'&lt;LIST&lt;')">
        <!-- list -->
        <xsl:choose>
          <xsl:when test="not(contains('+LIST&lt;INTEGER>+LIST&lt;DOUBLE>+LIST&lt;TEXT>+LIST&lt;BOOLEAN>+LIST&lt;DATETIME>+',concat('+',@datatype,'+')))">
            <xsl:apply-templates mode="property-reference-value-list" select="."/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:apply-templates mode="property-value-list" select="."/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <!-- hence, this is no collection -->
      <xsl:otherwise>
        <xsl:choose>
          <!-- the referenced entities have been returned. -->
          <xsl:when test="Record|RecordType|Property|File">
            <xsl:for-each select="Record|RecordType|Property|File">
              <xsl:call-template name="single-value">
                <xsl:with-param name="reference">
                  <xsl:value-of select="'true'"/>
                </xsl:with-param>
                <xsl:with-param name="value">
                  <xsl:value-of select="@id"/>
                </xsl:with-param>
                <xsl:with-param name="boolean">
                  <xsl:value-of select="'false'"/>
                </xsl:with-param>
              </xsl:call-template>
            </xsl:for-each>
          </xsl:when>
          <xsl:otherwise>
            <!-- only the ids are available -->
            <xsl:call-template name="single-value">
              <xsl:with-param name="value">
                <xsl:value-of select="text()"/>
              </xsl:with-param>
              <xsl:with-param name="reference">
                <xsl:value-of select="not(contains('+INTEGER+DOUBLE+TEXT+BOOLEAN+DATETIME+',concat('+',@datatype,'+')))"/>
              </xsl:with-param>
              <xsl:with-param name="boolean">
                <xsl:value-of select="@datatype='BOOLEAN'"/>
              </xsl:with-param>
              <xsl:with-param name="datetime">
                <xsl:value-of select="@datatype='DATETIME'"/>
              </xsl:with-param>
              <xsl:with-param name="long-text-threshold">
                <xsl:value-of select="'${BUILD_LONG_TEXT_PROPERTY_THRESHOLD_SINGLE}'"/>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
    <!-- unit -->
    <xsl:if test="@unit">
      <span class="caosdb-unit">
        <xsl:value-of select="@unit"/>
      </span>
    </xsl:if>
  </xsl:template>
  <xsl:template match="Property" mode="property-value-plain">
    <xsl:choose>
      <!-- filter out collection data types -->
      <xsl:when test="contains(concat('&lt;',@datatype),'&lt;LIST&lt;')">
              <!-- ignore for now -->
            </xsl:when>
      <!-- hence, this is no collection -->
      <xsl:otherwise>
        <xsl:value-of select="normalize-space(text())"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template match="@*" mode="property-attributes">
    <dt class="col-6 col-md-4 mb-0"><xsl:value-of select="name()"/></dt>
    <dd class="col-6 col-md-8 mb-0">
      <xsl:value-of select="."/>
    </dd>
  </xsl:template>
  <xsl:template match="@datatype" mode="property-attributes-type">
    <dt class="col-6 col-md-4 mb-0">data type</dt>
    <dd class="col-6 col-md-8 mb-0 caosdb-property-datatype">
      <xsl:value-of select="."/>
    </dd>
  </xsl:template>
  <xsl:template match="@description" mode="property-attributes-desc">
    <dt class="col-6 col-md-4 mb-0">description</dt>
    <dd class="col-6 col-md-8 mb-0 caosdb-property-description">
      <xsl:value-of select="."/>
    </dd>
  </xsl:template>
  <xsl:template match="@id" mode="property-attributes-id">
    <dt class="col-6 col-md-4 mb-0">id</dt>
    <dd class="col-6 col-md-8 mb-0">
      <a class="caosdb-property-id">
        <xsl:attribute name="href">
          <xsl:value-of select="concat($entitypath,.)"/>
        </xsl:attribute>
        <xsl:value-of select="."/>
      </a>
    </dd>
  </xsl:template>
  <!-- ANNOTATIONS -->
  <xsl:template name="annotation-section">
    <xsl:param name="entityId"/>
    <xsl:param name="collapseId"/>
    <ul class="collapse list-group caosdb-annotation-section">
      <xsl:attribute name="data-entity-id">
        <xsl:value-of select="$entityId"/>
      </xsl:attribute>
      <xsl:attribute name="id">
        <xsl:value-of select="$collapseId"/>
      </xsl:attribute>
      <li class="list-group-item caosdb-comments-heading d-flex">
        <i class="bi-chat-left-fill" style="margin-right: 1em;"/>
        <strong class="small">Comments</strong>
        <button class="btn btn-sm float-end caosdb-new-comment-button ms-auto">
          <strong>add new comment</strong>
        </button>
      </li>
    </ul>
  </xsl:template>

  <!-- ENTITY STATE -->
  <xsl:template mode="entity-heading-attributes-state" match="State">
    <!-- creates a state button in the header of an entity which opens a modal with more information buttons for transitions -->
    <xsl:param name="entityId"/>
    <xsl:param name="hasSuccessor"/>
    <xsl:param name="stateModalId">state-modal-<xsl:value-of select="generate-id()"/></xsl:param>
    <button title="State Info" class="btn" data-bs-toggle="modal">
      <xsl:attribute name="data-bs-target">#<xsl:value-of select="$stateModalId"/></xsl:attribute>
      <span class="badge label-info caosdb-v-state-label">
        <xsl:if test="@color">
          <xsl:attribute name="style">background-color: <xsl:value-of select="@color"/>;</xsl:attribute>
        </xsl:if>
        <xsl:value-of select="./@name"/>
      </span>
    </button>

    <!-- here comes the modal -->
    <div class="caosdb-f-entity-state-info modal fade" tabindex="-1" role="dialog">
      <xsl:attribute name="id"><xsl:value-of select="$stateModalId"/></xsl:attribute>
      <div class="modal-dialog" role="document">
        <div class="modal-content text-left">
          <div class="modal-header flex-wrap">
            <span class="modal-title">
              <span class="badge caosdb-v-state-model-label"><xsl:value-of select="@model"/>
                <span title="State Info" class="badge badge-info caosdb-v-state-label">
                  <xsl:attribute name="style">
                    <xsl:if test="@color">
                      background-color: <xsl:value-of select="@color"/>;
                    </xsl:if>
                  </xsl:attribute>
                <xsl:value-of select="@name"/></span>
              </span>
            </span>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" title="Close"></button>
            <div style="margin-top: 8px"><em><xsl:value-of select="@description"/></em></div>
          </div>
          <div class="modal-body">
            <xsl:choose>
              <xsl:when test="parent::RecordType">
                Every newly inserted Record with type "<xsl:value-of select="parent::RecordType/@name"/>" will initially be in this state.
              </xsl:when>
              <xsl:when test="$hasSuccessor">
                <p>You are currently viewing an old versions of this entity. <a>
                    <xsl:attribute name="href"><xsl:value-of select="$entityId"/></xsl:attribute>
                    Go to the latest version.</a></p>
              </xsl:when>
              <xsl:otherwise>
                <xsl:if test="not(Transition)">
                    You cannot perform any transitions. Maybe this is due to lack of permissions.
                </xsl:if>
                <dl class="row caosdb-f-transition">
                <xsl:for-each select="Transition">
                  <dt class="col-sm-4 mb-2"><button class="btn btn-secondary badge caosdb-f-entity-state-transition-button fs-6" type="button">
                        <xsl:attribute name="data-to-state"><xsl:value-of select="ToState/@name"/></xsl:attribute>
                        <xsl:attribute name="data-transition-name"><xsl:value-of select="@name"/></xsl:attribute>
                        <xsl:attribute name="title">Transition to state '<xsl:value-of select="ToState/@name"/>'. <xsl:if test="ToState/@description"><xsl:value-of select="ToState/@description"/></xsl:if></xsl:attribute>
                        <xsl:if test="@color">
                          <xsl:attribute name="style">
                            background-color: <xsl:value-of select="@color"/>;
                          </xsl:attribute>
                        </xsl:if>
                      <xsl:value-of select="@name"/></button></dt>
                  <dd class="col-sm-8"><xsl:value-of select="@description"/></dd>
                </xsl:for-each>
                </dl>
              </xsl:otherwise>
            </xsl:choose>
          </div>
          <div class="modal-footer">
            <a href="?query=FIND Record StateModel WITH name = Model1">
              <xsl:attribute name="href">?query=FIND RECORD StateModel WITH name = "<xsl:value-of select="@model"/>"</xsl:attribute>
              View state model</a>
          </div>
        </div>
      </div>
    </div>
  </xsl:template>

  <!--VERSIONING-->
  <xsl:template match="Version" mode="entity-heading-attributes-version">
    <xsl:param name="entityId"/>
    <xsl:param name="versionModalId">version-modal-<xsl:value-of select="generate-id()"/></xsl:param>
    <!-- the clock button which opens the window with the versioning info -->
    <button title="Versioning Info" type="button" data-bs-toggle="modal">
      <xsl:attribute name="data-bs-target">#<xsl:value-of select="$versionModalId"/></xsl:attribute>
      <xsl:attribute name="class">
        caosdb-f-entity-version-button caosdb-v-entity-version-button btn
        <xsl:if test="Successor">
          <!-- indicate old version by color and symbol -->
          <xsl:value-of select="' text-danger'"/>
        </xsl:if>
      </xsl:attribute>
      <i class="bi-clock-history"/>
    </button>

    <!-- the following div.modal is the window that pops up when the user clicks on the clock button -->
    <div class="caosdb-f-entity-version-info modal fade" tabindex="-1" role="dialog">
      <xsl:attribute name="id"><xsl:value-of select="$versionModalId"/></xsl:attribute>
      <xsl:attribute name="data-entity-versioned-id"><xsl:value-of select="concat($entityId, '@', @id)"/></xsl:attribute>
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content text-left">
          <!-- modal-header start -->
          <div>
            <xsl:attribute name="class">
              modal-header
              text-start
              <xsl:if test="not(@head='true')">
                <!-- indicate old version by color -->
                <xsl:value-of select="' bg-danger'"/>
              </xsl:if>
            </xsl:attribute>
              <p class="caosdb-entity-version-attr">
                <h4 class="modal-title">Version Info</h4>
                <em class="caosdb-entity-version-attr-name">
                This is
                <xsl:if test="not(@head='true')"><b>not</b></xsl:if>
                the latest version of this entity.
                <xsl:apply-templates mode="entity-version-modal-head" select="Successor">
                  <xsl:with-param name="entityId" select="$entityId"/>
                </xsl:apply-templates>
                </em>
              </p>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" title="Close"></button>
          </div>
          <!-- modal-header end -->
          <div class="caosdb-f-entity-version-history">
            <!-- modal-body and modal-footer are added by this template -->
            <xsl:apply-templates select="." mode="entity-version-history-table">
              <xsl:with-param name="entityId" select="$entityId"/>
            </xsl:apply-templates>
          </div>
        </div>
      </div>
    </div>
  </xsl:template>

  <xsl:template match="Version[@completeHistory='true']" mode="entity-version-history-table">
    <!-- contains the table of the full version history -->
    <xsl:param name="entityId"/>
    <div class="modal-body">
      <table class="table table-hover">
        <thead>
          <tr>
            <th></th>
            <th class="invisible"><div class="export-data">Entity ID</div></th>
            <th class="export-data">Version ID</th>
            <th class="export-data">Date</th>
            <th class="export-data">User</th>
            <th class="invisible"><div class="export-data">URI</div></th>
            <th></th>
          </tr></thead>
        <tbody>
          <xsl:apply-templates mode="entity-version-modal-successor" select="Successor">
            <xsl:with-param name="entityId" select="$entityId"/>
          </xsl:apply-templates>
          <tr>
            <td class="invisible"><div class="export-data"><xsl:value-of select="$entityId"/></div></td>
            <td class="caosdb-v-entity-version-hint caosdb-v-entity-version-hint-cur">This Version</td>
            <td><xsl:apply-templates select="@id" mode="entity-version-id"/>
            </td><td>
              <xsl:apply-templates select="@date" mode="entity-version-date"/>
            </td><td class="export-data">
              <xsl:value-of select="@username"/>@<xsl:value-of select="@realm"/>
            </td>
            <td class="invisible"><div class="export-data"><xsl:value-of select="concat($entitypath, $entityId, '@', @id)"/></div></td>
            <td>
              <xsl:if test="not(@head='true')">
              <button type="button" class="caosdb-f-entity-version-restore-btn btn btn-secondary d-none" title="Restore this version of the entity.">
                <xsl:attribute name="data-version-id"><xsl:value-of select="$entityId"/>@<xsl:value-of select="@id"/></xsl:attribute>
                <i class="bi-arrow-counterclockwise"></i></button>
              </xsl:if>
            </td>
          </tr>
          <xsl:apply-templates mode="entity-version-modal-predecessor" select="Predecessor">
            <xsl:with-param name="entityId" select="$entityId"/>
          </xsl:apply-templates>
        </tbody>
      </table>
    </div>
    <div class="modal-footer">
      <button type="button" class="caosdb-f-entity-version-export-history-btn btn btn-secondary" title="Export this history table as a CSV file.">Export history</button>
    </div>
  </xsl:template>

  <xsl:template match="Version[not(@completeHistory='true')]" mode="entity-version-history-table">
    <!-- contains the table of the simple version info (not the full history)-->
    <xsl:param name="entityId"/>
    <div class="modal-body">
      <table class="table">
        <thead><tr><th>Previous Version</th><th>This Version</th><th>Next Version</th></tr></thead>
        <tbody>
          <tr>
          <td>
            <xsl:if test="not(Predecessor)">
              <div class="caosdb-v-entity-version-no-related">No predecessor</div>
            </xsl:if>
            <xsl:apply-templates select="Predecessor/@id" mode="entity-version-link-to-other-version">
              <xsl:with-param name="entityId" select="$entityId"/>
            </xsl:apply-templates>
          </td>
          <td>
            <xsl:apply-templates select="@id" mode="entity-version-id"/>
          </td>
          <td>
            <xsl:if test="not(Successor)">
              <div class="caosdb-v-entity-version-no-related">No successor</div>
            </xsl:if>
            <xsl:apply-templates select="Successor/@id" mode="entity-version-link-to-other-version">
              <xsl:with-param name="entityId" select="$entityId"/>
            </xsl:apply-templates>
          </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="modal-footer">
      <button type="button" style="display: none" class="caosdb-f-entity-version-load-history-btn btn btn-secondary">Load full history</button>
    </div>
  </xsl:template>

  <xsl:template match="@id" mode="entity-version-id">
    <!-- a versions'id (abbreviated) -->
    <xsl:attribute name="title">Full Version ID: <xsl:value-of select="."/></xsl:attribute>
    <xsl:value-of select="substring(.,1,8)"/>
    <td class="invisible"><div class="export-data"><xsl:value-of select="."/></div></td>
  </xsl:template>

  <xsl:template match="@date" mode="entity-version-date">
    <!-- a version's date (abbreviated)-->
    <xsl:attribute name="title"><xsl:value-of select="."/></xsl:attribute>
    <xsl:value-of select="substring(.,0,11)"/>
    <xsl:value-of select="' '"/>
    <xsl:value-of select="substring(.,12,8)"/>
    <td class="invisible"><div class="export-data"><xsl:value-of select="."/></div></td>
  </xsl:template>

  <xsl:template match="Predecessor|Successor" mode="entity-version-modal-single-history-item">
    <!-- a single row of the version history table -->
    <xsl:param name="entityId"/>
    <xsl:param name="hint"/>
    <tr>
      <td class="invisible"><div class="export-data"><xsl:value-of select="$entityId"/></div></td>
      <td class="caosdb-v-entity-version-hint"><xsl:value-of select="$hint"/></td>
      <td>
        <xsl:apply-templates select="@id" mode="entity-version-link-to-other-version">
          <xsl:with-param name="entityId" select="$entityId"/>
        </xsl:apply-templates>
      </td><td>
        <xsl:apply-templates select="@date" mode="entity-version-date"/>
      </td><td class="export-data">
        <xsl:value-of select="@username"/>@<xsl:value-of select="@realm"/>
      </td>
      <td class="invisible"><div class="export-data"><xsl:value-of select="concat($entitypath, $entityId, '@', @id)"/></div></td>
      <td>
        <!-- include button if it is not head, i.e. Predecessors are always old and Successors if they do have a Successor Member -->
        <xsl:if test="(name()='Predecessor' or Successor)">
        <button type="button" class="caosdb-f-entity-version-restore-btn btn btn-secondary d-none" title="Restore this version of the entity.">
          <xsl:attribute name="data-version-id"><xsl:value-of select="$entityId"/>@<xsl:value-of select="@id"/></xsl:attribute>
          <i class="bi-arrow-counterclockwise"></i></button>
        </xsl:if>
      </td>
    </tr>
  </xsl:template>

  <xsl:template match="@id" mode="entity-version-link-to-other-version">
    <!-- link to other version (used by both version tables)-->
    <xsl:param name="entityId"/>
    <a><xsl:attribute name="href"><xsl:value-of select="$entityId"/>@<xsl:value-of select="."/></xsl:attribute>
      <xsl:apply-templates select="." mode="entity-version-id"/></a>
  </xsl:template>

  <xsl:template match="Predecessor" mode="entity-version-modal-predecessor">
    <!-- content of the versioning info (not the full history) -->
    <xsl:param name="entityId"/>
    <xsl:apply-templates mode="entity-version-modal-single-history-item" select=".">
      <xsl:with-param name="entityId" select="$entityId"/>
      <xsl:with-param name="hint" select="'Older Version'"/>
    </xsl:apply-templates>
    <xsl:apply-templates mode="entity-version-modal-predecessor" select="Predecessor">
      <xsl:with-param name="entityId" select="$entityId"/>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="Successor" mode="entity-version-modal-head">
    <!-- content of the versioning modal's header (if a newer version exists) -->
    <xsl:param name="entityId"/>
    View the newest version here:
    <a><xsl:attribute name="href"><xsl:value-of select="$entityId"/>@HEAD</xsl:attribute>
      <xsl:value-of select="$entityId"/>@HEAD
    </a>
  </xsl:template>

  <xsl:template match="Successor" mode="entity-version-modal-successor">
    <!-- content of the versioning info (not the full history) -->
    <xsl:param name="entityId"/>
    <xsl:apply-templates mode="entity-version-modal-successor" select="Successor">
      <xsl:with-param name="entityId" select="$entityId"/>
    </xsl:apply-templates>
    <xsl:apply-templates mode="entity-version-modal-single-history-item" select=".">
      <xsl:with-param name="entityId" select="$entityId"/>
      <xsl:with-param name="hint" select="'Newer Version'"/>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="Version/Successor" mode="entity-action-panel-version">
    <!-- clickable warning message in the entity actions panel when there exists a newer version -->
    <xsl:param name="entityId"/>
    <a class="caosdb-f-entity-version-old-warning alert-warning btn" title="Go to the latest version of this entity.">
      <xsl:attribute name="href"><xsl:value-of select="$entityId"/>@HEAD</xsl:attribute>
      <strong>Warning</strong> A newer version exists!
    </a>
  </xsl:template>

  <xsl:template match="Version" mode="entity-version-marker">
    <!-- content of the data-version-id attribute -->
    <xsl:attribute name="data-version-id">
      <xsl:value-of select="@id"/>
    </xsl:attribute>
    <xsl:apply-templates select="Successor" mode="entity-version-marker"/>
  </xsl:template>

  <xsl:template match="Successor" mode="entity-version-marker">
    <!-- content of the data-version-successor attribute
         This data-attribute marks entities which have a newer version.
    -->
    <xsl:attribute name="data-version-successor">
      <xsl:value-of select="@id"/>
    </xsl:attribute>
  </xsl:template>

  <!-- PERMISSIONS -->
  <xsl:template match="Permissions" mode="entity-permissions">
    <div style="display: none">
      <xsl:apply-templates select="Permission" mode="entity-permissions"/>
    </div>
  </xsl:template>

  <xsl:template match="Permission" mode="entity-permissions">
    <div>
      <xsl:attribute name="data-permission"><xsl:value-of select="@name"/></xsl:attribute>
    </div>
  </xsl:template>
</xsl:stylesheet>

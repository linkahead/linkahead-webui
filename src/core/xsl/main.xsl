<?xml version="1.0" encoding="UTF-8"?>
<!--
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019 IndiScale GmbH (info@indiscale.com)
 * Copyright (C) 2019 Timm Fitschen (t.fitschen@indiscale.com)
 * Copyright (C) 2019 Daniel Hornung (d.hornung@indiscale.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html"/>
  <xsl:variable name="basepath">
    <xsl:call-template name="uri_ends_with_slash">
      <xsl:with-param name="uri">
        <xsl:value-of select="/Response/@baseuri"/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:variable>
  <xsl:variable name="entitypath" select="concat($basepath,'Entity/')"/>
  <xsl:variable name="filesystempath" select="concat($basepath,'FileSystem')"/>
  <xsl:variable name="lowercase" select="'abcdefghijklmnopqrstuvwxyz'"/>
  <xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>

  <xsl:template name="caosdb-head-title">
      <title>${BUILD_TITLE_BRAND_NAME}</title>
  </xsl:template>

  <xsl:template name="caosdb-head-css">
    <xsl:element name="link">
      <xsl:attribute name="rel">stylesheet</xsl:attribute>
      <xsl:attribute name="href">
        <xsl:value-of select="concat($basepath,'webinterface/${BUILD_NUMBER}/css/bootstrap.css')"/>
      </xsl:attribute>
    </xsl:element>
    <xsl:element name="link">
      <xsl:attribute name="rel">stylesheet</xsl:attribute>
      <xsl:attribute name="href">
        <xsl:value-of select="concat($basepath,'webinterface/${BUILD_NUMBER}/css/webcaosdb.css')"/>
      </xsl:attribute>
    </xsl:element>
    <xsl:element name="link">
      <xsl:attribute name="rel">stylesheet</xsl:attribute>
      <xsl:attribute name="href">
        <xsl:value-of select="concat($basepath,'webinterface/${BUILD_NUMBER}/css/linkahead.css')"/>
      </xsl:attribute>
    </xsl:element>
    <xsl:element name="link">
      <xsl:attribute name="rel">stylesheet</xsl:attribute>
      <xsl:attribute name="href">
        <xsl:value-of select="concat($basepath,'webinterface/${BUILD_NUMBER}/css/dropzone.css')"/>
      </xsl:attribute>
    </xsl:element>
    <xsl:element name="link">
      <xsl:attribute name="rel">stylesheet</xsl:attribute>
      <xsl:attribute name="href">
        <xsl:value-of select="concat($basepath,'webinterface/${BUILD_NUMBER}/css/tour.css')"/>
      </xsl:attribute>
    </xsl:element>
    <xsl:element name="link">
      <xsl:attribute name="rel">stylesheet</xsl:attribute>
      <xsl:attribute name="href">
        <xsl:value-of select="concat($basepath,'webinterface/${BUILD_NUMBER}/css/leaflet.css')"/>
      </xsl:attribute>
    </xsl:element>
    <xsl:element name="link">
      <xsl:attribute name="rel">stylesheet</xsl:attribute>
      <xsl:attribute name="href">
        <xsl:value-of select="concat($basepath,'webinterface/${BUILD_NUMBER}/css/leaflet-coordinates.css')"/>
      </xsl:attribute>
    </xsl:element>
    <xsl:element name="link">
      <xsl:attribute name="rel">stylesheet</xsl:attribute>
      <xsl:attribute name="href">
        <xsl:value-of select="concat($basepath,'webinterface/${BUILD_NUMBER}/css/bootstrap-select.css')"/>
      </xsl:attribute>
    </xsl:element>
    <xsl:element name="link">
      <xsl:attribute name="rel">stylesheet</xsl:attribute>
      <xsl:attribute name="href">
        <xsl:value-of select="concat($basepath,'webinterface/${BUILD_NUMBER}/css/bootstrap-icons.css')"/>
      </xsl:attribute>
    </xsl:element>
    <!--CSS_EXTENSIONS-->
  </xsl:template>
  <xsl:template name="caosdb-data-container">
    <div class="container d-flex flex-column-reverse flex-lg-row caosdb-f-main">
        <div class="flex-grow-1 caosdb-f-main-entities">
          <xsl:call-template name="paging-panel"/>
          <xsl:apply-templates select="/Response/UserInfo"/>
          <xsl:apply-templates mode="top-level-data" select="/Response/*"/>
          <xsl:apply-templates mode="query-results" select="/Response/Query"/>
          <xsl:if test="not(/Response/Query/Selection)">
            <xsl:apply-templates mode="entities" select="/Response/*"/>
          </xsl:if>
          <xsl:call-template name="paging-panel"/>
        </div>
        <div class="caosdb-f-edit ms-2">
          <div class="card caosdb-v-edit-panel">
            <div class="card-header">
              <span class="card-title">Edit Mode Toolbox</span>
            </div>
            <div class="caosdb-f-edit-panel-body">
              <div class="list-group list-group-flush">
              <div class="list-group-item btn-group-vertical caosdb-v-editmode-btngroup caosdb-f-edit-mode-create-buttons">
                <button type="button" class="btn btn-secondary caosdb-f-edit-panel-new-button new-property">Create Property</button>
                <button type="button" class="btn btn-secondary caosdb-f-edit-panel-new-button new-recordtype">Create RecordType</button>
              </div>
              </div>
            </div>
          </div>
        </div>
    </div>
  </xsl:template>
  <xsl:template match="*" mode="entities"/>
  <xsl:template match="*" mode="top-level-data"/>
  <!-- assure that this uri ends with a '/' -->
  <xsl:template name="uri_ends_with_slash">
    <xsl:param name="uri"/>
    <xsl:choose>
      <xsl:when test="substring($uri,string-length($uri),1)!='/'">
        <xsl:value-of select="concat($uri,'/')"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$uri"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>

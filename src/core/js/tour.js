/*
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2019 IndiScale GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
'use strict';

/**
 * The tour module contains functionality for the demo tour through the
 * functionality of the caosdb web interface and some of caosdb's general
 * functionality.
 *
 * The content of a tour is loaded from the config file `tour.json`, which
 * defines chapters, sections and pages.  Each page corresponds to a popover
 * window with some content, which can be activated by hint buttons.
 *
 * For the sake of simplicity, in this documentation we will talk about
 * a corresponding yaml file (instead of json), which needs to be converted to 
 * a json file before usage.
 * The `tour.yaml` shall be placed in `conf/ext/json`
 * Please also consult `tour.example.yaml` stored in the `doc` folder.
 *
 * # Structure of the tour.yaml file #
 *
 * The top-level node "tour" contains a boolean `active` which specifies if the
 * tour shall be active initially and a node `elements` where the chapters etc.
 * are stored.  `elements` is a list of `page_set`s, which are the chapters of
 * the tour.
 *
 * ## `page_set` ##
 *
 * Each `page_set` (e.g. chapter or section) defines the following properties:
 *
 * - page_set :: Name of the page set.
 * - active :: If the page set is active initially.  For more than one active
 *             page set, only the last one wins.  Default = false
 * - persistent_state :: If the page set's state shall be saved in the
 *       sessionStorage.  Default = true
 * - deactivate_other :: If this page set is exclusive, i.e. if other page sets
 *       are deactivated upon activating this one.  Default = true
 * - id :: The id is used for referencing to this element.
 * - elements :: Contains elements of this page_set, either more page_sets or
 *     pages.
 *
 * ## `page` ##
 *
 * Pages are presented to the user via buttons, which show a popover window upon
 * click.  The buttons' position is defined relative to some other element,
 * their visibility may change by activity on other pages.  Pages share some
 * properties with page sets, some properties are new:
 *
 * - page :: Name, shown inside the page button.
 * - active :: Initial state, default = true
 * - id :: Same as for `page_set`.
 * - target :: Element selector for the element to which the page button should
 *             be attached.
 * - button_position :: Where the button is, relative to the target element.
 *       One of `top`, `bottom`, `left`, `right` or a combination thereof.
 * - button_size :: One of `small`, `medium`, `large`.  If not given, the button
 *       will have an automatic, not necessarily circular, size.
 *       Currently deactivated!
 * - title :: Title of the tour page.
 * - content :: The content of the page.  Rendered as Markdown.
 * - highlighters :: Makes HTML elements of the content highlight other
 *       elements.  Organized as key-value pairs, or rather ID-selector pairs.
 * - css :: Extra CSS attributes for the content frame box.
 * - activation / deactivation :: Specifies when this page button should be
 *       shown or hidden.  This is done by two sub-properties:
 *     - event :: Which kind of event triggers the (de)activation.  Typical
 *                events for pages are `open.tour.page`, `close.tour.page`.
 *     - target :: Selector at which element the event listener is being
 *       registered (if existent).
 *
 */
var ERROR = 1
var WARNING = 2
var INFO = 3
var DEBUG = 4
var TRACE = 5

var tour = new function () {

    ///////////////////////////////////////////////////////////////////////////
    //                         Improving a bit on jquery                     //
    ///////////////////////////////////////////////////////////////////////////

    // :Contains is case insensitive
    jQuery.expr[':'].Contains = function (a, i, m) {
        return jQuery(a).text().toUpperCase()
            .indexOf(m[3].toUpperCase()) >= 0;
    };

    var logger = log.getLogger("tour");

    this.PageSet = class {
        constructor(parent_set, config, idx) {
            if (typeof parent_set === "undefined") {
                throw new Error("param `parent_set` must not be undefined");
            }
            if (typeof config === "undefined") {
                throw new Error("param `config` must not be undefined");
            }

            this.isPageSet = true;
            this.parent_set = parent_set;
            this.config = config;
            this._elements = new Array();
            this.active = false;

            this.id = config.id || parent_set.id + "-psid-" + idx;

            if (config.elements) {
                for (const element of config.elements) {
                    const next = tour.add_tour_element(element, this, this._elements.length);
                    this._elements.push(next);
                }
            }

            // set some defaults
            if (typeof this.config.active === "undefined") {
                this.config.active = false;
            }
            if (typeof this.config.old_state_active === "undefined") {
                this.config.old_state_active = this.config.active;
            }
            if (typeof this.config.persistent_state === "undefined") {
                this.config.persistent_state = true;
            }
            if (typeof this.config.deactivate_other === "undefined") {
                this.config.deactivate_other = true;
            }
        }

        _tour_active () {
            return this.parent_set._tour_active();
        }

        _activate_by_id(id) {
            this.parent_set._activate_by_id(id);
        }

        set old_state_active(value) {
            if (this.config.old_state_active != value) {
                this.config.old_state_active = value;
                this.update();
            }
        }

        get old_state_active() {
            return this.config.old_state_active;
        }

        update() {
            this.parent_set.update();
        }

        get name() {
            return this.config.page_set;
        }

        get full_name() {
            return this.parent_set.full_name + "." + this.name;
        }

        get elements() {
            return this._elements;
        }

        /**
         * Create menu entry for a PageSet
         */
        create_menu_entry() {
            var menuid = 'tour-submenu-' + this.name.replace(/ /g, "");
            var page_set_entry = $("<li class='mb-1 caosdb-f-tour-overview-entry caosdb-v-tour-overview-entry-pageset'/><button class='caosdb-v-tour-toc-pageset btn' data-bs-toggle='collapse' data-bs-target='#" + menuid + "' aria-expanded='false'>" + this.name + "</button><div id='" + menuid + "'class='collapse'><ul class='btn-toggle-nav list-unstyled fw-normal pb-1'></ul></div></li>");
            var elements_list = page_set_entry.find("ul")[0];

            // store menu_entry for highlightng opened pages/chapters.
            this.menu_entry = page_set_entry[0];

            for (const element of this.elements) {
                const next = element.create_menu_entry();
                if (next) {
                    elements_list.append(next);
                }
            }
            return page_set_entry;
        }

        /**
         * Activate PageSet: Initialize activation of elements.
         */
        activate() {
            if (!this.active) {
                logger.info("PageSet.activate: tour element '" + this.full_name + "'.");

                this._activate();

                this._on_activation();

                // activation triggers the initialization of the children
                for (const element of this._elements) {
                    element.init_activation(this.config.persistent_state);
                }
            }
        }


        highlight_menu_entry() {
            $(this.menu_entry).toggleClass("caosdb-v-tour-menu-entry-highlight", true);
            $(this.menu_entry).find(".collapse").toggleClass("show", true);
        }

        unhighlight_menu_entry() {
            $(this.menu_entry).toggleClass("caosdb-v-tour-menu-entry-highlight", false);
            $(this.menu_entry).find(".collapse").toggleClass("show", false);
        }

        /**
         * _activate PageSet
         */
        _activate() {
            if (!this.active) {
                logger.debug("PageSet._activate tour element '" + this.full_name + "'.");
                this.active = true;
                this.old_state_active = true;
                this.highlight_menu_entry();

                // activation propagates to the parents
                this.parent_set._activate()
                this.parent_set.deactivate_other(this);
            }
        }

        /**
         * _on_activation PageSet
         */
        _on_activation() {
            let hiding = this.config.on_activation_hide;
            if (hiding) {
                logger.debug("Tour page set hiding:");
                hiding = tour.assert_array(hiding);
                hiding.forEach((selector) => {
                    $(selector).addClass("caosdb-f-tour-hidden");
                });
            }
        }

        deactivate_other(trigger) {
            logger.debug("Close pages other than '" + trigger.id + "'.");
            if (this.config.deactivate_other) {
                for (const element of this._elements) {
                    if (element.isPage && element !== trigger) {
                        element.deactivate();
                    }
                }
            }
        }

        /**
         * Initialize activation of PageSet.
         */
        init_activation(restore_old_state = false) {
            logger.debug("PageSet.init_activation tour element '" + this.full_name + "'.");
            if (restore_old_state) {
                if (this.old_state_active) {
                    this.activate();
                }
            } else if (this.config.active) {
                this.activate();
            }
        }

        deactivate() {
            if (this.active) {
                logger.info("PageSet.deactivate tour element '" + this.full_name + "'.");
                this.old_state_active = false;
                this._deactivate();
            }
        }

        _deactivate() {
            if (this.active) {
                logger.debug("PageSet._deactivate tour element '" + this.full_name + "'.");
                this.active = false;
                this.unhighlight_menu_entry();

                // deactivation propagates to the children
                for (const element of this._elements) {
                    element._deactivate();
                }
                // Unhide everything
                $(".caosdb-f-tour-hidden").removeClass("caosdb-f-tour-hidden");

            }
        }

        get_previous_tour_page(id) {
            return this.parent_set.get_previous_tour_page(id);
        }

        get_next_tour_page(id) {
            return this.parent_set.get_next_tour_page(id);
        }

        get_tour_page_by_id(id) {
            return this.parent_set.get_tour_page_by_id(id)
        }


    }

    this.Page = class {
        constructor(parent_set, config, idx) {
            if (typeof parent_set === "undefined") {
                throw new Error("param `parent_set` must not be undefined");
            }
            if (typeof config === "undefined") {
                throw new Error("param `config` must not be undefined");
            }

            this.config = config;
            this.isPage = true;
            this.initialized = false;

            this.parent_set = parent_set;
            this.active = false;
            if (typeof this.config.href === "undefined") {
                this.config.href = "/";
            }
            if (typeof this.config.id === "undefined") {
                this.config.id = (this.parent_set.id + "-p" + idx).replace(/ /g, "");
            }
            if (typeof this.config.show_button === "undefined") {
                this.config.show_button = false;
            }
            this.id = config.id

            // the argument button_size currently deactivated
            this.button = this._create_tour_button(config.page, config.content, config.title, config.id, config.button_size, this, config.css);
            if (!  this.config.show_button){
                $(this.button).hide()
            }
            this._init_on_trigger(this.button, config);
            this._deinit_on_trigger(this.button, config);

            this._setup_activation_listeners(this.button, this.config.activation, this.config.deactivation);

            // set some defaults
            if (typeof this.config.active === "undefined") {
                this.config.active = false;
            }
            if (typeof this.config.old_state_active === "undefined") {
                this.config.old_state_active = this.config.active;
            }
        }

        _on_popover_open() {
            caosdb_utils.assert_not_undefined(this.popover, "this.popover");
            caosdb_utils.assert_not_undefined(this.popover.tip, "this.popover.tip");
            this._scroll_into_view(this.popover.tip);

            // initialize close button
            const cb = $(this.popover.tip).find(".caosdb-f-tour-popover-close-button")
                .on("click", (e) => {
                    this.get_page_popover().hide();
                });

            // TODO move to styling/popover_template
            if (this.config.detour){
                const head = $(this.popover.tip).find(".popover-header")
                head.toggleClass("bg-warning", true);
            }

            // initialize detour button
            $(this.popover.tip).find('a[data-detour]').each((idx, element) => {
                const detour_start_page_id  = $(element).data("detour");
                const detour_page = this.get_tour_page_by_id(
                    detour_start_page_id);
                $(element)
                    .attr("href", detour_page.config.href+`#${detour_start_page_id}`)
                    .toggleClass(
                        ["btn", "btn-sm", "btn-warning", "fw-bold"], true);
                $(element).click((e) =>{
                    this._activate_by_id(detour_start_page_id);
                    const id_anchor = `#${detour_start_page_id}`;
                    if ($(id_anchor).length > 0) {
                        detour_page._open();
                        return false;
                    } else {
                        sessionStorage["tour-page-open-next"] = detour_start_page_id;
                        // follow link
                    }
                });
            });

            // initialize next/prev buttons
            var nb = $(this.popover.tip).find("button[data-role=next]")
            if (this.get_next()) {
                if (this.config.force_manual_action){
                    nb.toggleClass("disabled", true);
                    nb.parent().attr("title","Manual action required!")
                } else {
                    nb.on("click", (e) => {
                        const pn = this.get_next()
                        pn.activate(true);
                        if ($("#" + pn.config.id).length == 0) {
                            sessionStorage["tour-page-open-next"] = pn.config.id;
                            window.location = pn.config.href
                        } else {
                            pn._open();
                        }
                    });
                }
            } else {
                nb.toggleClass("invisible", true);
            }
            var pb = $(this.popover.tip).find("button[data-role=prev]")
            if (this.get_previous()) {
                pb.on("click", (e) => {
                    const pp = this.get_previous()
                    pp.activate(true);
                    if ($("#" + pp.config.id).length == 0) {
                        sessionStorage["tour-page-open-next"] = pp.config.id;
                        window.location = pp.config.href
                    } else {
                        pp._open();
                    }
                });
            } else {
                pb.toggleClass("invisible", true);
            }
        }

        _init_on_trigger(button, config) {
            const id = this.config.id;
            // Place in the dom tree at a temporary position.
            // This is necessary because otherwise any referencing tour
            // page would not find this button during the initialization of
            // the listeners.
            $(document.body).append(button);
            $(button).hide();

            if (config.init) {

                // now set up the triggering of the initialization
                const ev = config.init["event"];
                const always = config.init["always"]

                var final_target = this._get_body_or_target(config.init["target"])
                var call_init = () => {
                    if (!always) {
                        logger.debug("remove init event handler", ev, final_target);
                        final_target.removeEventListener(ev, call_init, true);
                    }
                    this._init(button, config);
                    if (this.config.init["open"]) {
                        // open immediately
                        sessionStorage["tour-page-open-next"] = this.config.id;
                    }
                    if (this.active && sessionStorage["tour-page-open-next"] == this.config.id) {
                        if (this.config.show_button){
                            $(this.button).show();
                        }
                        this._open();
                    };
                };

                logger.debug("add init event handler", ev, call_init, final_target);
                final_target.addEventListener(ev, call_init, true);

            } else { // trigger immediately
                this._init(button, config);
            }
        }

        _deinit_on_trigger(button, config){
            if (config.deinit) {
                // now set up the triggering of the deinitialization
                const ev = config.deinit["event"];

                const final_target = this._get_body_or_target(config.deinit["target"])
                const call_init = () => {
                    this._deinit(button, config);
                    $(this.button).hide();
                    if (this.popover){
                        this.popover.hide();
                    }
                };

                logger.debug("add init event handler", ev, call_init, final_target);
                final_target.addEventListener(ev, call_init, true);
            }
        }

        /*
         * returns the document body if target is undefined, HTMLElement of
         * target otherwise
         */
        _get_body_or_target(target){
            if (typeof target == "undefined") {
                return document.body;
            }

            const final_target = $(target);
            if (final_target.length < 1) {
                throw new Error("could not find the target");
            }
            return final_target[0];
        }

        /**
         * Close popover and set page to initialized=false
         */
        _deinit(button, config) {
            this.initialized = false;
        }

        /**
         * Intialize the button and the popover of this page.
         *
         * This means positioning it on the page, adding all necessary event
         * listeners, and more.
         **/
        _init(button, config) {
            logger.trace("enter Page._init", this.id, button, config);
            this._position_button(button, config.target, config.button_position);
            if (config.button_css) {
                for (let key in config.button_css) {
                    button.style.setProperty(key,
                        config.button_css[key]);
                }
            }
            this._apply_highlighters(button, config.highlighters);
            this._apply_hiding(button);
            this.initialized = true;
        }

        _activate_by_id(id) {
            this.parent_set._activate_by_id(id);
        }

        /** create_menu_enrty for a Page */
        create_menu_entry() {
            const id_anchor = `#${this.config.id}`;

            if (this.config.do_not_show_in_toc) {
                return;
            }
            var classes = ""
            if (this.config.detour){
                classes = " caosdb-v-tour-toc-detour"
            }
            if (sessionStorage["tour-page-open-cur"] == this.config.id) {
                classes = classes + " caosdb-v-tour-toc-cur";
            }
            var entry = $("<li class='caosdb-f-tour-overview-entry caosdb-v-tour-overview-entry-page" + classes + "' />");
            var link = $("<a href='" + this.config.href + id_anchor + "'>" + this.name + "</a>")[0];

            $(link).click((e) => {
                this.activate(true);

                if ($(id_anchor).length > 0) {
                    this._open();
                    return false;
                } else {
                    sessionStorage["tour-page-open-next"] = this.config.id;
                    // follow link
                }

            });
            entry.append(link);

            this.menu_entry = entry[0];
            return entry[0];
        }

        set old_state_active(value) {
            if (this.config.old_state_active != value) {
                this.config.old_state_active = value;
                this.update();
            }
        }

        get old_state_active() {
            return this.config.old_state_active;
        }

        update() {
            this.parent_set.update();
        }

        get name() {
            return this.config.page;
        }

        get full_name() {
            return this.parent_set.full_name + ":" + this.name;
        }

        /*
         * Page init_activation
         */
        init_activation(restore_old_state = false) {
            logger.debug("Page.init_activation button '" + this.full_name + "'.");
            if (restore_old_state) {
                if (this.old_state_active) {
                    this.activate(false);
                }
            } else if (this.config.active) {
                this.activate(false);
            }
        }

        _highlight_menu() {
            $(".caosdb-v-tour-toc-active-item").toggleClass("caosdb-v-tour-toc-active-item", false);
            $(".caosdb-v-tour-toc-cur").toggleClass("caosdb-v-tour-toc-cur", false);
            $(this.menu_entry).toggleClass("caosdb-v-tour-toc-active-item", true);
        }

        _unhighlight_menu() {
            $(this.menu_entry).toggleClass("caosdb-v-tour-toc-active-item", false);
        }

        _close_other() {
            // hide all other popover, close all other tour pages.
            const tour_inst = this.parent_set.parent_set
            for (var index in tour_inst._elements_by_id ){
                const page = tour_inst._elements_by_id[index]
                if (page.popover){
                    page.popover.hide();
                }
                if (page.button){
                    // TODO close only when f-tour-open-page class is there?
                    $(page.button).toggleClass("caosdb-f-tour-open-page", false)
                    page.button.dispatchEvent(tour.close_page_event);
                }
            }
        }

        get_page_popover() {
            const target = $(this.config["target"])[0]
            if (!this.popover) {
                // not initialized yet.
                this.popover = new bootstrap.Popover(target, this.popover_options);

                const events = [
                  "ext_bottom_line.preview.ready",
                  "caosdb.preview.ready",
                  "caosdb.preview.show",
                  "caosdb.preview.hide",
                  "shown.bs.collapse",
                  "hidden.bs.collapse",
                ];
                for (let ev of events) {
                    document.body.addEventListener(ev, () => {
                        this.popover.update();
                    }, true);
                }
                target.addEventListener("shown.bs.popover", (e) => {
                    if (this.active){
                        this._on_popover_open();
                        this.button.dispatchEvent(tour.open_page_event);
                    }

                });
                target.addEventListener("hidden.bs.popover", (e) => {
                    $(this.button).toggleClass("caosdb-f-tour-open-page", false)
                });

            }
            return this.popover;
        }

        _before_open() {
            if (this.config["before_open"]) {
                const f = new Function(this.config["before_open"]);
                f();
            }
        }

        _open() {
            this._before_open();
            const button = $(this.button);
            const target = $(this.config["target"])
            sessionStorage["tour-page-open-cur"] = this.config.id;
            if (button.is(".caosdb-f-tour-open-page")) {
                return; // already open
            }
            if (! target[0]) {
                return; // element to attach to not available; can't open
            }
            if (this.get_next()) {
                if ($(`#${this.get_next().config.id}`).length == 0) {
                    logger.debug(`Set next page in session store to `, this.get_next().config.id);
                    sessionStorage["tour-page-open-next"] = this.get_next().config.id
                }
            }
            target.on('hidden.bs.popover', (e) => {
                button[0].dispatchEvent(tour.close_page_event);
            });
            this._close_other();

            // open this one
            const popover = this.get_page_popover();
            button.toggleClass("caosdb-f-tour-open-page", true);
            popover.show();

            this._highlight_menu();
        }

        _scroll_into_view(popover) {
            if (typeof popover == "undefined") {
                return;
            }
            const box = popover.getBoundingClientRect();
            const viewport_height = window.innerHeight || document.documentElement.clientHeight;
            var yscroll = 0;

            if (box.bottom > viewport_height) {
                // element's bottom is hidden down there
                // align top of popover with top of viewport or bottom of
                // popover with bottom of viewport, whichever involves the
                // least scrolling.
                yscroll = box.bottom - viewport_height;
            }

            if (box.top < yscroll) {
                // element's top is hidden up there
                // align top of popover with top of viewport
                yscroll += box.top
            }

            window.scrollBy(0, yscroll);
        }

        _close() {
            logger.debug(`Page.close ${this.config.id}`);
            $(this.button)
                .toggleClass("caosdb-f-tour-open-page", false)
            if (this.popover) {
                this.popover.hide();
            }
            this._unhighlight_menu();
        }

        /*
         * Page activate
         */
        activate(deactivate_others) {
            if (deactivate_others) {
                this.parent_set.deactivate_other(this)
            }
            if (!this.active) {
                logger.info("Page.activate button '" + this.full_name + "'.");
                this.old_state_active = true;
                this.active = true;

                // TODO Why is the following line necessary? "References"
                // Chapter of files does not open without it (when the
                // "References" is used to get to the correct page)
                if (this.config.show_button){
                    $(this.button).show();
                }

                // activation propagates to the parents
                this.parent_set._activate()

                if (this.initialized && sessionStorage["tour-page-open-next"] == this.config.id) {
                    if ($(this.config["target"])[0]) {
                        this._open();
                    } else {
                        tour._post_init_cb.push(() => {
                            this._open();
                        });
                    }
                }
                // Set the natural "next" as next tour page in session store as
                // default. This value will be overwritten by navigation.
            }
            if (this.config.show_button){
                $(this.button).show();
            }
        }

        deactivate() {
            if (this.active) {
                logger.info("Page.deactivate button '" + this.full_name + "'.");
                this.old_state_active = false;

                this._deactivate();
            }
        }


        _deactivate() {
            if (this.active) {
                logger.debug("Page._deactivate button '" + this.full_name + "'.");
                this.active = false;
                this._close();
                $(this.button).hide();
                $(this.button).toggleClass("caosdb-f-tour-open-page", false);
            }
        }

        _tour_active () {
            return this.parent_set._tour_active();
        }

        /**
         * Hook the button to (de)activation events.
         */
        _setup_activation_listeners(button, activation, deactivation) {
            //$(button).hide();
            if (typeof activation !== "undefined") {
                activation = tour.assert_array(activation);
                activation.forEach(
                    (act) => {
                        if (act === null) {
                            return;
                        }
                        tour._post_init_cb.push(() => {
                            var target = $(act.target)[0];
                            if (target) {
                                logger.debug("Added listener:", target, act.event)
                                target.addEventListener(
                                    act.event,
                                    (e) => {
                                        if (this._tour_active()) {
                                            logger.debug("Activate", this.config.id, "due to ", act.event)
                                            this.activate();
                                        }
                                    }
                                );
                            }
                        });
                    }
                )
            }
            if (typeof deactivation !== "undefined") {
                deactivation = tour.assert_array(deactivation);
                deactivation.forEach(
                    (deact) => {
                        if (deact === null) {
                            return;
                        }
                        tour._post_init_cb.push(() => {
                            var target = $(deact.target)[0];
                            if (target) {
                                target.addEventListener(
                                    deact.event,
                                    (e) => {
                                        this.deactivate();
                                    }
                                );
                            }
                        });
                    }
                )
            }
        }


        _position_button(button, target, position) {
            var sel = $(target).first()
            logger.debug("positioning button", button, target, sel, position);
            if (typeof sel.css("position") === "undefined" ||
                sel.css("position") === "static") {
                sel.css("position", "relative");
            }

            var wrapper = $('<div class="caosdb-f-tour-button-wrapper" style="display: block; position: relative; text-align: center; height: 0px;"></div>');
            wrapper.append(button);

            $(button).css("position", "absolute");

            switch (position) {
            case "top":
                sel.prepend(wrapper);
                $(button).css("top", -Math.abs($(button).outerHeight()) / 2);
                //$(button).css("left", "50%");
                break;
            case "top-right":
                sel.prepend(wrapper);
                $(button).css("top", -Math.abs($(button).outerHeight()) / 2);
                $(button).css("right", -Math.abs($(button).outerWidth()) / 2);
                break;
            case "right":
                wrapper.css("top", "50%");
                wrapper.css("right", sel.css("padding-right"));
                wrapper.css("position", "absolute");
                sel.prepend(wrapper);
                $(button).css("right", -Math.abs($(button).outerWidth()) / 2);
                $(button).css("top", -Math.abs($(button).outerHeight()) / 2);
                break;
            case "bottom-right":
                sel.append(wrapper);
                $(button).css("bottom", -Math.abs($(button).outerHeight()) / 2);
                $(button).css("right", -Math.abs($(button).outerWidth()) / 2);
                break;
            case "bottom":
                wrapper.css("top", "100%");
                wrapper.css("left", "50%");
                wrapper.css("position", "absolute");
                sel.append(wrapper);
                $(button).css("margin-top", "5px");
                $(button).css("top", 0);
                $(button).css("left", 0);
                $(button).css("transform", "translate(-50%, 0)");
                break;
            case "bottom-left":
                sel.append(wrapper);
                $(button).css("bottom", -Math.abs($(button).outerHeight()) / 2);
                $(button).css("left", -Math.abs($(button).outerWidth()) / 2);
                break;
            case "left":
                wrapper.css("top", "50%");
                wrapper.css("position", "absolute");
                sel.prepend(wrapper);
                $(button).css("margin-right", "5px");
                $(button).css("top", 0);
                $(button).css("right", 0);
                $(button).css("transform", "translate(0, -50%)");
                break;
            default:
                // top-left
                sel.prepend(wrapper);
                $(button).css("top", -Math.abs($(button).outerHeight()) / 2);
                $(button).css("left", -Math.abs($(button).outerWidth()) / 2);
                break;
            }

            // initially hide the button
            $(button).hide();
        }


        _apply_highlighter(highlighter, highlightable) {
            $(highlighter).hover(
                () => {
                    highlightable.toggleClass("caosdb-v-tour-highlight", true);
                },
                () => {
                    highlightable.toggleClass("caosdb-v-tour-highlight", false);
                }
            );
        }

        _apply_hiding(button) {
            $(button).on('shown.bs.popover', (e) => {
                let hiding = this.config.on_activation_hide;
                if (hiding) {
                    logger.debug("Tour page hiding:");
                    hiding = tour.assert_array(hiding);
                    hiding.forEach((selector) => {
                        $(selector).addClass("caosdb-f-tour-hidden");
                    });
                }
                let unhiding = this.config.on_activation_unhide;
                if (unhiding) {
                    logger.debug("Tour page unhiding:");
                    unhiding = tour.assert_array(unhiding);
                    unhiding.forEach((selector) => {
                        $(selector).removeClass("caosdb-f-tour-hidden");
                    });
                }
            })
        }

        get_previous() {
            if (this.config.previous){
                return this.parent_set.parent_set._elements_by_id[this.config.previous];
            } else {
                return this.parent_set.get_previous_tour_page(this.id);
            }
        }

        get_next() {
            if (this.config.next){
                return this.parent_set.parent_set._elements_by_id[this.config.next];
            } else {
                return this.parent_set.get_next_tour_page(this.id);
            }
        }

        get_tour_page_by_id(id) {
            return this.parent_set.get_tour_page_by_id(id)
        }

        _apply_highlighters(button, highlighters) {
            if (typeof button === "undefined") {
                throw new Error("button was undefined");
            }
            if (!(button instanceof HTMLElement)) {
                throw new Error("button must instanciate HTMLElement");
            }
            if (!highlighters) {
                return;
            }
            const ids = Object.keys(highlighters)
            var highlightable = {}
            for (const id of ids) {
                highlightable[id] = $(highlighters[id]);
                if (id == "button") {
                    // special case, don't look for the highlighter in the content
                    this._apply_highlighter(button, highlightable[id]);
                } else {
                    $(button).on('shown.bs.popover', (e) => {
                        // TODO check if this popover is already initialized
                        logger.debug("Highlighting:",
                            highlighters[id], highlightable[id]);
                        this._apply_highlighter("#" + id, highlightable[id]);
                    });
                }
            }
        }

        // currently deactivated
        _create_tour_button(name, content, title, id, size, page, css = {},
            placement = "auto") {
            if (typeof name === "undefined") {
                throw new Error("name was undefined");
            }
            if (typeof content === "undefined") {
                throw new Error("content was undefined");
            }

            var button = $('<button class="caosdb-v-tour-button"></button>');

            var markdown_content = tour.markdown_to_html(content);
            //logger.debug("Page's markdown content: ", markdown_content);

            // Apply custom style immediately at popover creation.
            // Alternatively, we could create a custom style element which sets
            // the style for "#" + id + " + .popover"

            if (!("max-width" in css)) {
                css["max-width"] = "120em";
            }
            if (!("width" in css)) {
                css["width"] = "30em";
            }
            let popover_style = "";
            for (let key in css) {
                popover_style += key + ": " + css[key] + "; ";
            }
            let popover_template = '<div class="popover" role="tooltip" style="z-index:20000; ' +
                popover_style +
                '"><div class="popover-arrow"></div><button class="btn btn-close caosdb-f-tour-popover-close-button caosdb-v-tour-popover-close-button"></button><h3 class="popover-header"></h3><div class="popover-body popover-content"></div><div class="p-3 pt-0 d-flex justify-content-between" ><span><button class="btn btn-sm btn-secondary caosdb-v-tour-pn-btn me-auto" data-role="prev">Previous</button></span><span><button class="btn btn-sm btn-secondary caosdb-v-tour-pn-btn" data-role="next">Next</button></span></div></div>';

            button.attr("title", title);

            if (typeof title === "undefined") {
                title = ""
            };
            this.popover_options = {
                title: title,
                content: markdown_content,
                container: "body",
                placement: placement,
                html: true,
                sanitize: false,
                trigger: 'manual',
                template: popover_template,
            };

            button.on("click", (e) => {
                if (button.hasClass("caosdb-f-tour-open-page")) {
                    this._close();
                } else {
                    this._open();
                }

                // clicks on the tour button should not trigger any other action
                e.preventDefault();
                e.stopPropagation();
            });


            if (id) {
                button.attr("id", id);
            }
            /* currently deactivated
            if(size)
                $(button).toggleClass(size, true);

            */
            return button[0];
        }

    }

    this.config = undefined;
    this.popover_options = {};
    this.close_page_event = new Event("close.tour.page");
    this.open_page_event = new Event("open.tour.page");

    this._post_init_cb = []

    /**
     * Post init is called after the initialization.
     *
     * It is mainly used to initialize event listeners which needed to wait
     * until all the tour buttons are actually present in the DOC tree.
     *
     * All functions which need to be called after the intialization of the
     * tour may be appended to the {@link tour#_post_init_cb} array.
     */
    this.post_init = function () {
        for (const fn of this._post_init_cb) {
            fn();
        }
    }

    /*
     * 0 - SILENT
     * 1 - ERROR
     * 2 - WARNING
     * 3 - INFO
     * 4 - DEBUG
     * 5 - TRACE
     */
    this.verbosity_level = INFO;

    /**
     * Initialize the tour.
     *
     * The `refresh` argument is currently only used interactively on the
     * debugging console.
     */
    this.init = async function _in(refresh) {
        try {
            logger.debug("initializing tour module, refresh: " + refresh);
            if (refresh === true) {
                logger.info("Refreshing tour state.");
                localStorage.removeItem("tour_state");
            }
            await tour.load_tour();
            tour.post_init();
        } catch (error) {
            globalError(error);
        }
    }

    this.load_tour = async function _lt() {
        var config = undefined;
        try {
            var old_state = JSON.parse(localStorage.getItem("tour_state"));
            if (old_state) {
                config = {
                    tour: old_state
                }
            };
        } catch (error) {
            if (error instanceof SyntaxError) {
                logger.warning("Parsing old tour state failed with SyntaxError. Old tour state: '" + localStorage.getItem("tour_state") + "'.");
            } else {
                globalError(error);
            }
        }

        // reset if build number changed
        if (config && config.tour && config.tour._build_number != "${BUILD_NUMBER}") {
            config = undefined;
        }
        if (!config || config.length == 0 || config.tour.length == 0) {
            logger.info("No old tour state in the localStorage.");
            // try to fetch 
            config = await load_config("tour.json");
            logger.debug("Loaded tour.json", config);
        }
        if (!config || config.length == 0 || config.tour.length == 0) {
            localStorage.setItem("tour_state", "[]");
            logger.info("Tour config is empty.");
            return;
        }

        // store build number in tour config
        config.tour._build_number = "${BUILD_NUMBER}";
        tour.configure(config.tour);
    }

    this.add_tour_element = function (element, parent_set, idx) {
        if (element.page_set) {
            // it's a page_set
            return tour.add_tour_page_set(element, parent_set, idx);
        } else if (typeof element.separator != "undefined") {
            // it's a separator
            return tour.add_tour_menu_separator();
        } else {
            // it's a page
            return tour.add_tour_page(element, parent_set, idx);
        }
    }

    this.add_tour_page_set = function (config, parent_set, idx) {
        return new tour.PageSet(parent_set, config, idx);
    }

    this.add_tour_menu_separator = function (element) {
        return {
            create_menu_entry: () => $("<hr>")[0],
            init_activation: function() {},
            _deactivate: function() {},
        }
    }

    this.Tour = class {
        constructor(config) {
            this.full_name = "Tour";
            this.id = config.id || "tour0";
            this.config = config;
            this.elements = new Array();
            this.active = false;

            var menuitem = $('<li class="nav-item" id="caosdb-navbar-tour"><a href="#" class="d-none nav-link caosdb-f-start-tour-btn" title="Start a Tour">Tour</a><a href="#" title="Leave the Tour" class="d-none caosdb-f-leave-tour-btn nav-link">Tour</a></li>')
            $(".caosdb-navbar").append(menuitem);

            var min_width_warning = $('<div class="alert alert-warning caosdb-tour-min-width-warning d-lg-none" role="alert"><strong>Warning</strong> This tour is optimized for screens wider than 992px. If you have trouble displaying elements of this tour, please try accessing it on a larger screen.</div>');
            $(".navbar").append(min_width_warning);

            for (const element of this.config.elements) {
                const next = tour.add_tour_element(element, this, this.elements.length);
                this.elements.push(next);
            }

            // set some defaults
            this.config.active = !!this.config.active;
            if (typeof this.config.old_state_active === "undefined") {
                this.config.old_state_active = this.config.active;
            }
            if (typeof this.config.persistent_state === "undefined") {
                this.config.persistent_state = true;
            }
            if (typeof this.config.deactivate_other === "undefined") {
                this.config.deactivate_other = true;
            }

            $(".caosdb-f-leave-tour-btn").click((e) => {
                this.deactivate();
            });
            $(".caosdb-f-start-tour-btn").click((e) => {
                this.activate();
            });

            this.update();
            this.create_tour_overview_panel();

            this._elements_by_id = {};
            this._tour_pages = [];
            this._index_elements(this._elements_by_id, this._tour_pages, this);
            this._toggle_tour_buttons();
            this._toggle_width_warning();
        }

        _toggle_tour_buttons() {
            $(".caosdb-f-leave-tour-btn").toggleClass("d-none", !this.active);
            $(".caosdb-f-start-tour-btn").toggleClass("d-none", this.active);
        }

        _toggle_width_warning() {
            $(".caosdb-tour-min-width-warning").toggleClass("d-block", this.active);
            $(".caosdb-tour-min-width-warning").toggleClass("d-none", !this.active);
        }

        _tour_active () {
            return this.active;
        }

        /**
         * @param {string} id
         * @return {tour.Page}
         */
        get_next_tour_page(id) {
            const index_old = this._tour_pages.indexOf(id);
            if (index_old < 0) {
                throw new Error("Tour page not in _tour_pages list");
            }
            const index_new = index_old + 1;
            if (index_new >= this._tour_pages.length) {
                return null;
            }
            return this._elements_by_id[this._tour_pages[index_new]];
        }

        get_tour_page_by_id(id) {
            return this._elements_by_id[id];
        }

        /**
         * @param {string} id
         * @return {tour.Page}
         */
        get_previous_tour_page(id) {
            const index_old = this._tour_pages.indexOf(id);
            if (index_old < 0) {
                throw new Error("Tour page not in _tour_pages list");
            }
            const index_new = index_old - 1;
            if (index_new < 0) {
                return null;
            }
            return this._elements_by_id[this._tour_pages[index_new]];
        }

        _index_elements(index, pages, element) {
            if (element.elements) {
                for (const sub of element.elements) {
                    this._index_elements(index, pages, sub);
                    if (sub.id) {
                        index[sub.id] = sub;
                    }
                    if (sub.isPage) {
                        pages.push(sub.id);
                    }
                }
            }
        }

        _activate_by_id(id) {
            var element = this._elements_by_id[id];
            if (element) {
                element.activate(true);
            }
        }

        set old_state_active(value) {
            if (this.config.old_state_active != value) {
                this.config.old_state_active = value;
                this.update();
            }
        }

        get old_state_active() {
            return this.config.old_state_active;
        }

        /**
         * Start tour activation.
         */
        init_activation(restore_old_state = false) {
            logger.debug("Tour.init_activation '" + this.full_name + "'.");
            if (restore_old_state) {
                if (this.old_state_active) {
                    this.activate();
                }
            } else if (this.config.active) {
                this.activate();
            }
        }

        deactivate() {
            logger.info("Tour.deactivate tour");
            this._deactivate();

            // deactivation propagates to the children
            for (const element of this.elements) {
                element._deactivate();
            }

            sessionStorage.removeItem("tour-page-open-next")
        }

        /**
         * Reset the tour state.  Mainly useful for development and debugging.
         */
        async reset_tour() {
            logger.info("Resetting the tour");
            sessionStorage.removeItem("tour-page-open-next")
            this.deactivate();
            localStorage.removeItem("tour_state");
            var config = await load_config("tour.json")
            if (!config || config.length == 0 || config.tour.length == 0) {
                localStorage.setItem("tour_state", "[]");
                logger.info("Tour config is empty.");
            } else {
                tour.configure(config.tour);
            }
            location.reload();
        }

        _deactivate() {
            if (this.active) {
                logger.debug("Tour._deactivate tour");
                this.old_state_active = false;
                this.active = false;
                this._hide_tour_sidebar();
                this._toggle_tour_buttons();
                this._toggle_width_warning();
                this.update();
            }
        }

        _hide_tour_sidebar() {
            $("body").toggleClass("tour-sidebar-visible", false);
        }

        _show_tour_sidebar() {
            $("body").toggleClass("tour-sidebar-visible", true);
        }

        _activate() {
            if (!this.active) {
                logger.debug("Tour._activate tour");
                this.old_state_active = true;
                this.active = true;
                this._show_tour_sidebar();
                this._toggle_tour_buttons();
                this._toggle_width_warning();
                this.update();
            }
        }

        /**
         * Activate tour: initialize activation of elements.
         */
        activate() {
            logger.info("Tour.activate tour");
            this._activate();

            for (const element of this.elements) {
                element.init_activation(true);
            }

        }

        deactivate_other(trigger) {
            if (this.config.deactivate_other) {
                logger.debug("Close pagesets other than '" + trigger.id + "'.");
                for (const element of this.elements) {
                    if (element.isPageSet && element !== trigger) {
                        element.deactivate();
                    }
                }
            }
        }

        create_tour_overview_panel() {
            var tour_overview = $('<ul class="list-unstyled caosdb-v-tour-overview"/>');
            for (const element of this.elements) {
                const next = element.create_menu_entry();
                if (next) {
                    tour_overview.append(next);
                }
            }
            $("#tour-toc .caosdb-f-tour-toc-body").empty().append(tour_overview);
        }

        update() {
            localStorage.setItem("tour_state", JSON.stringify(this.config));
        }


    }

    this.configure = function (config) {
        logger.info({
            "configure tour": config
        });

        // clean up old tour elements (after reload)
        $("#caosdb-f-tour-overview-panel").remove();
        $("#caosdb-navbar-tour").remove();
        $(".caosdb-f-tour-button-wrapper").remove();

        // TODO how can this be done better?
        $("#caosdb-navbar-tour>a").each(function (index) {
            this.addEventListener("click", () => {
                $(".caosdb-v-left-panel").toggleClass("invisible");
            });
        })
        const instance = new tour.Tour(config);

        if (config.reload) {
            $(config.reload.target).each(function (index) {
                this.addEventListener(config.reload.event, (e) => {
                    tour.configure(instance.config);
                }, true);
            });
        }

        if (typeof sessionStorage["tour-page-open-next"] === "undefined") {
            const next = sessionStorage["tour-page-open-cur"] || instance._tour_pages[0];
            sessionStorage["tour-page-open-next"] = next
        }
        instance.init_activation(config.persistent_state);

    }

    this.add_tour_page = function (config, parent_set, idx) {
        return new tour.Page(parent_set, config, idx);
    }

    /**
     * TODO replace with function of the markdown module.
     */
    this.markdown_to_html = function (content) {
        let converter = new showdown.Converter();
        return converter.makeHtml(content.trim());
    }

    this.assert_array = function (content) {
        if (!Array.isArray(content)) {
            return [content];
        }
        return content;
    }

    /**
     * Calls server-side script `scriptname`.
     */
    this.run_script = async function (scriptname) {
        try {
            const script_result = await connection.runScript(scriptname);
            const retcode = script_result.getElementsByTagName("script")[0].getAttribute("code");
            if (parseInt(retcode) > 0) {
                throw ("An error occurred during execution of the server-side script:\n" +
                    script_result.getElementsByTagName("script")[0].outerHTML);
            }
        } catch (e) {
            globalError(e);
        };
        return true;
    }


};


$(document).ready(tour.init);

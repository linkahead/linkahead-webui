/*
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2018 Alexander Schlemmer
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019 Timm Fitschen (t.fitschen@indiscale.com)
 * Copyright (C) 2019 Daniel Hornung <d.hornung@indiscale.com>
 * Copyright (C) 2019 IndiScale GmbH, Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

"use strict";

/**
 * Query Shortcuts Extension for LinkAhead WebUI.
 *
 * Loads, generates and expands shortcuts.
 *
 * Global shortcuts are loaded from webinterface/${BUILD_NUMBER}/conf/json/global_query_shortcuts.json, the
 * format is as follows:
 *
 * [
 *   {
 *     "description": "Find all geological data sets with temperature above {temp} K.",
 *     "query": "FIND Record Geodata with temperature > \"{temp}\""
 *   }, {
 *     ...
 *   }
 * ]
 */
var query_shortcuts = new function () {


    this.dependencies = ["form_elements", "log", "transaction"];
    this.logger = log.getLogger("query_shortcuts");

    this._shortcuts_property_description_name = "templateDescription";
    this._shortcuts_property_query_name = "Query";
    this._shortcuts_record_type_name = "UserTemplate";

    this._shortcuts_property_query_id = undefined;
    this._shortcuts_property_description_id = undefined;
    this._shortcuts_record_type_id = undefined;

    this._global_shortcuts_file = "json/global_query_shortcuts.json";

    /**
     * Return a button element which opens a drop-down menu. The menu contains
     * the toolbox with Create, Edit and Delete functionality.
     *
     * Fun fact: The button shows a little wrench.
     *
     * @return {HTMLElement} The toolbox drop-down.
     */
    this.make_toolbox_button = function () {
        var ret = $(
            `<div class="dropdown text-end caosdb-f-shortcuts-toolbox-button">
              <button title="Shortcuts Toolbox" class="btn dropdown-bs-toggle" type="button"
                  data-bs-toggle="dropdown"><i class="bi-wrench"></i></button>
              <ul class="dropdown-menu">
                <li class="dropdown-header dropdown-item">Shortcuts Tools</li>
                <li><button class="btn dropdown-item" type="button" data-tool="create">Create</button></li>
                <li><button class="btn dropdown-item" type="button" data-tool="edit">Edit</button></li>
                <li><button class="btn dropdown-item" type="button" data-tool="delete">Delete</button></li>
              </ul>
            </div>`);

        // TODO move to separate functions and refactor to `add_tool(toolbox,
        // name, callback)`...

        // bind callback functions
        ret.find("button[data-tool='delete']")
            .click(function () {
                var shortcuts_panel = $(".caosdb-shortcuts-container");
                query_shortcuts.init_delete_shortcut_form(shortcuts_panel[0]);
            });
        ret.find("button[data-tool='create']")
            .click(function () {
                var shortcuts_panel = $(".caosdb-shortcuts-container");
                query_shortcuts.init_create_shortcut_form(shortcuts_panel[0]);
            });
        ret.find("button[data-tool='edit']")
            .click(function () {
                var shortcuts_panel = $(".caosdb-shortcuts-container");
                query_shortcuts.init_update_shortcut_form(shortcuts_panel[0]);
            });

        return ret[0];
    }

    this.init_datamodel = function () {
        // TODO handle the datamodel dependencies in a save way (lazy-loading is
        // bad, if the data-model is not there).
        if (typeof this._shortcuts_property_query_id == "undefined") {
            query("FIND Property " + query_shortcuts._shortcuts_property_query_name)
                .then(result => {
                    if (result.length > 0) {
                        query_shortcuts._shortcuts_property_query_id = getEntityID(result[0]);
                    } else {
                        query_shortcuts._shortcuts_property_query_id = null;
                    }
                })
                .catch(query_shortcuts.logger.error);
        }
        if (typeof this._shortcuts_property_description_id == "undefined") {
            query("FIND Property " + this._shortcuts_property_description_name)
                .then(result => {
                    if (result.length > 0) {
                        query_shortcuts._shortcuts_property_description_id = getEntityID(result[0]);
                    } else {
                        query_shortcuts._shortcuts_property_description_id = null;
                    }
                })
                .catch(query_shortcuts.logger.error);
        }
        if (typeof this._shortcuts_record_type_id == "undefined") {
            query("FIND RecordType " + query_shortcuts._shortcuts_record_type_name)
                .then(result => {
                    if (result.length > 0) {
                        query_shortcuts._shortcuts_record_type_id = getEntityID(result[0]);
                    } else {
                        query_shortcuts._shortcuts_record_type_id = null;
                    }
                })
                .catch(query_shortcuts.logger.error);
        }
    }

    this.init = async function () {
        this.init_datamodel();
        var header = $('<details class="caosdb-f-shortcuts-panel-header"><summary class="caosdb-f-shortcuts-panel-header-title">Shortcuts</summary></details>')
        header.find("summary").append(this.make_toolbox_button());
        var body = $('<div class="caosdb-f-shortcuts-panel-body"/>');
        header.append(body)
        var shortcuts_panel = $('<div class="container caosdb-shortcuts-container"></div>')
            .append(header)

        body.append(await this.retrieve_global_shortcuts());

        if (isAuthenticated()) {
            body.append(await this.retrieve_user_shortcuts());
        }

        if (body.children(".caosdb-f-query-shortcut").length > 0) {
            // append if not empty
            if ("${BUILD_MODULE_LEGACY_QUERY_FORM}" != "ENABLED") {
                // The selector has a different name with the new panel
                const queryPanelSelectorName = ".caosdb-f-query-form";
                $(queryPanelSelectorName).after(shortcuts_panel);
            } else {
                $("#caosdb-query-panel").append(shortcuts_panel);
            }
        }

        return shortcuts_panel[0];

    }




    /**
     * Find all placeholders in a string.
     *
     * E.g. find_placeholders("asdf {test1} safd {test2} asdf") returns
     * ["test1", "test2"].
     *
     * @param {string} str
     * @returns {string[]} array with all ids of placeholders
     */
    this.find_placeholders = function (str) {
        const re = /\{(\w*?)\}/g;
        var match = [...str.matchAll(re)];

        var ret = []
        for (const ph of match) {
            ret.push(ph[1]);
        }

        return ret;
    }


    /**
     * Replace the placeholders with input form elements.
     *
     * E.g. `asdf {test1} safd {test2} asdf` is converted to `asdf <input
     * name="test1" type="text" title="test1"/> safd <input name="test2"
     * type="text" title="test2"/> asdf`
     *
     * @param {string} description - the description of a shortcut.
     * @return {string} the description with all placeholders replaced with
     * input form elements.
     */
    this.replace_placeholders_with_inputs = function (description) {
        var placeholders = this.find_placeholders(description);

        var ret = description;
        for (const ph of placeholders) {
            const re = RegExp("\{" + ph + "\}", "g");
            const type = "text";
            const name = ph;
            const title = ph;
            ret = ret.replace(re, ' <input type="' + type + '" name="' + name + '" title="' + title + '"/> ');
        }
        return ret;
    }



    /**
     * Replace all placeholder in a query string with values from a dictionary.
     *
     * E.g. "FIND RECORD WITH date IN {year}" is converted to "FIND RECORD WITH
     * date IN 2018" when `values` is {"year": "2018"}.
     *
     * @param {string} query - the query with placeholders
     * @param {object} values - a dictionary for the values
     * @return {string} the query string with all placeholders replaced by the
     * values in the dictionary.
     */
    this.replace_placeholders_with_values = function (query, values) {
        var placeholders = this.find_placeholders(query);

        var ret = query;
        for (const ph of placeholders) {
            const re = RegExp("\{" + ph + "\}", "g");
            ret = ret.replace(re, values[ph]);
        }
        return ret;
    }


    /**
     * Extract a dictionary of the values that the user entered into the
     * shortcut forms input elements.
     *
     * This function assumes that the input elements have unique `name`
     * attributes. The resulting dictionary uses these names as keys.
     *
     * @param {HTMLElement} shortcut_form - a single shortcut form.
     * @returns {object} a dictionary with the inputs' names as keys.
     */
    this.extract_placeholder_values = function (shortcut_form) {
        var ret = {}
        for (const input of $(shortcut_form).find("input")) {
            ret[input.name] = input.value;
        }
        return ret;
    }


    /**
     * Generate a single query shortcut form.
     *
     * The form contains the description with all placeholders replaced by
     * input elements and button which insert the generated query into the
     * query panel. The generated query has all placeholders replaced by the
     * values of the input elements.
     *
     * @param {string} description
     * @param {string} query_string
     * @param {HTMLElement} A `DIV.caosdb-f-query-shortcut.row`.
     */
    this.generate_shortcut_form = function (description, query_string) {
        var preparedstr = query_shortcuts.replace_placeholders_with_inputs(description);
        var shortcut_form = $(
            `<div class="row caosdb-f-query-shortcut">
              <div class="col-md-10">
                <span class="caosdb-f-query-shortcut-form">` + preparedstr + `</span>
              </div>
              <div class="position-relative caosdb-f-query-shortcut-right-col col-md-2 text-end">
              </div>
            </div>`
        );

        // function for inserting the generated query string into the query panel
        var insert_to_query_panel = (inputSelectorName) => {
            var values = query_shortcuts.extract_placeholder_values(shortcut_form[0]);
            var replaced_query_string = query_shortcuts.replace_placeholders_with_values(query_string, values);

            $(inputSelectorName)
                .focus()
                .val(replaced_query_string);

            return replaced_query_string;
        };

        // callback for the submission
        var execute = (_) => {

            if ("${BUILD_MODULE_LEGACY_QUERY_FORM}" != "ENABLED") {
                const queryString = insert_to_query_panel(".caosdb-f-query-form input");
                window.localStorage.setItem("query.queryString", queryString);
                $(".caosdb-f-query-panel form").submit()
            } else {
                insert_to_query_panel("#caosdb-query-textarea");
                $("#caosdb-query-form").submit();
            }
        };

        shortcut_form.find(".caosdb-f-query-shortcut-right-col")
            .append(this.make_shortcut_buttons(execute, insert_to_query_panel));
        return shortcut_form[0];
    }

    this.generate_user_shortcut = function (description, query_string, id) {
        const ret = query_shortcuts.generate_shortcut_form(description, query_string);
        $(ret).toggleClass("caosdb-f-user-query-shortcut", true)
            .attr("data-entity-id", id)
            .attr("data-query-string", query_string)
            .attr("data-shortcut-description", description);
        return ret;
    }

    /**
     * Retrieve global query shortcuts and generate forms for each of them.
     *
     * The return value is never null. The array is empty if no global
     * shortcuts are retrieved.
     *
     * @returns {HTMLElement[]} array of query shortcut forms.
     */
    this.retrieve_global_shortcuts = async function () {

        try {
            var temparray = await query_shortcuts._query_global_shortcuts();

            var ret = [];
            for (var i = 0; i < temparray.length; i++) {
                var tempel = temparray[i];
                ret.push(this.generate_shortcut_form(tempel.description, tempel.query));
            }
            return ret;
        } catch (err) {
            query_shortcuts.logger.error(err);
        }

        // always return something when a function is async
        return [];

    }

    /**
     * Retrieve the user-defined query shortcuts and generate forms for each of
     * them.
     *
     * The return value is never null. The array is empty if no user-defined
     * shortcuts are retrieved.
     *
     * @returns {HTMLElement[]} array of query shortcut forms.
     */
    this.retrieve_user_shortcuts = async function () {
        try {
            var temparray = await query_shortcuts._query_user_shortcuts();

            var ret = [];
            for (var i = 0; i < temparray.length; i++) {
                var entity_id = getEntityID(temparray[i]);
                var description = getProperty(temparray[i], query_shortcuts._shortcuts_property_description_name, false);
                var query_string = getProperty(temparray[i], query_shortcuts._shortcuts_property_query_name, false);
                if (query_string && description) {
                    ret.push(this.generate_user_shortcut(description, query_string, entity_id));
                } else {
                    this.logger.warn("User-defined query shortcut without description or query", temparray[i]);
                }
            }
            return ret;
        } catch (err) {
            if (typeof err.message === "string" ||
                err.message instanceof String) {
                if (err.message.indexOf("404") > -1) {
                    query_shortcuts.logger.warn(err);
                    return;
                } else if (err.message.indexOf("401") > -1) {
                    query_shortcuts.logger.warn(err);
                    return;
                }
            }
            query_shortcuts.logger.error(err);
        }

        // always return something when a function is async
        return [];
    }

    /**
     * Retrieve the global query shortcuts from the server.
     *
     * The returned objects should have "description" and "query" members.
     *
     * @return {object[]} array of objects.
     */
    this._query_global_shortcuts = () => load_config(query_shortcuts._global_shortcuts_file);

    /**
     * Retrieve the user-defined query shortcuts from the server.
     *
     * @return {HTMLElement[]} array of entities in HTML representation.
     */
    this._query_user_shortcuts = () => query("FIND Record " + query_shortcuts._shortcuts_record_type_name + " WHICH HAS BEEN INSERTED BY ME");

    /**
     * Initialize the delete shortcuts form.
     *
     * The original shortcuts panel hides, a similar form appears. If the form
     * is submitted successfully the shortcuts panel is re-initialized. If the
     * form is submitted and the server returned an error, the form can be
     * canceled or the deletion can be tried again. If the form is canceled,
     * the original shortcuts panel is restored.
     *
     * @param {HTMLElement} panel - the shortcuts panel with global and
     * user-defined shortcuts.
     */
    this.init_delete_shortcut_form = function (panel) {
        $(panel).find(".alert").remove();
        var form = this.make_delete_form(panel, this.delete_callback, this.handle_delete_success, this.handle_delete_error);
        this.init_cud_shortcut_form(panel, form);
        return panel;
    }

    this.init_create_shortcut_form = function (panel) {
        $(panel).find(".alert").remove();
        var form = this.make_create_form(panel, this.create_callback, this.handle_create_success, this.handle_create_error);
        this.init_cud_shortcut_form(panel, form);
        return panel;
    }

    this.init_update_shortcut_form = function (panel) {
        $(panel).find(".alert").remove();
        var selector = this.make_update_selector(panel, this.init_update_single_shortcut_form);
        this.init_cud_shortcut_form(panel, selector);
    }


    this.init_update_single_shortcut_form = function (panel, entity_id) {
        $(panel).find(".alert").remove();
        var form = query_shortcuts.make_update_form(panel, entity_id, query_shortcuts.update_callback, query_shortcuts.handle_update_success, query_shortcuts.handle_update_error);
        query_shortcuts.init_cud_shortcut_form(panel, form);
        return panel;
    }


    this.handle_update_error = function (panel, results) {
        query_shortcuts.logger.trace("enter handle_update_error", panel, results);

        var errors = $(results).find(".alert-danger");
        $(panel).append(errors);
        query_shortcuts.logger.trace("leave handle_update_error");
        return query_shortcuts.make_dismissible_alert("danger", "<strong>Failure</strong> Shortcuts have not been updated.");
    }


    this.handle_create_error = function (panel, results) {
        query_shortcuts.logger.trace("enter handle_create_error", panel, results);

        var errors = $(results).find(".alert-danger");
        $(panel).append(errors);
        query_shortcuts.logger.trace("leave handle_create_error");
        return query_shortcuts.make_dismissible_alert("danger", "<strong>Failure</strong> Shortcuts have not been created.");
    }


    /**
     * Add an "Edit" button to each query shortcut which opens the actual
     * update form.
     */
    this.make_update_selector = function (panel, init_update_form) {
        caosdb_utils.assert_html_element(panel, "param `panel`");

        // clone panel
        var cloned = $(panel.outerHTML);

        // remove old header and toolbox button
        cloned.find(".caosdb-f-shortcuts-panel-header .h3").text("Edit Shortcut.");
        cloned.find(".caosdb-f-shortcuts-toolbox-button").remove();
        // show
        cloned.children().show();

        cloned.find(".caosdb-f-query-shortcut").each((idx, item) => {
            var wrapper = $(item).find(".col-md-2.text-end");

            // disable the inputs of the query shortcut
            $(item).find(":input").prop("disabled", true);

            // remove old buttons
            wrapper.children().remove();
            if ($(item).hasClass("caosdb-f-user-query-shortcut")) {
                // user shortcut
                // insert "UPDATE" button
                var entity_id = $(item).attr("data-entity-id");
                var input = $('<button type="button" class="btn btn-secondary" name="update-' + entity_id + '">Edit</button>')
                    .attr("title", "Edit this shortcut.")
                    .click(() => init_update_form(panel, entity_id));
                wrapper.append(input);
            } else {
                // global shortcut
                // gray out and add hint that this is a global shortcut which cannot be deleted.
                $(item)
                    .css({
                        "color": "#CCCCCC"
                    })
                    .attr("title", "This is a global shortcut which cannot be updated via the browser.");
            }
        });


        var form = form_elements.make_form({
            fields: cloned.children().toArray(),
            submit: false
        });
        return form;

    }

    this.init_cud_shortcut_form = function (panel, form) {
        // hide content
        $(panel).children().hide();
        var query_panel = $(".caosdb-query-form").hide();

        // show original again on cancel
        form.addEventListener("caosdb.form.cancel", function (e) {
            query_panel.show();
            $(panel).children().show();
        }, true);

        // show results in query panel
        form.addEventListener("caosdb.form.success", function (e) {
            query_panel.show();
        }, true);

        form.addEventListener("caosdb.form.submit", function (e) {
            $(panel).find(".alert").remove();
        }, true);

        $(panel).append(form);
    }

    this.highlight_resulting_entities = function (panel, results) {
        for (const elem of $(results).find("[data-entity-id]")) {
            var result_id = $(elem).attr("data-entity-id");
            var highlight = $(panel)
                .find(".caosdb-f-user-query-shortcut[data-entity-id='" +
                    result_id + "']")
                .toggleClass("caosdb-v-success-query-shortcut", true);
            highlight
                .css({
                    "background": "#90EE90"
                });
        }
        setTimeout(() => {
            $(".caosdb-v-success-query-shortcut")
                .css({
                    "background": "unset"
                });
            setTimeout(() => {
                $(".caosdb-v-success-query-shortcut")
                    .toggleClass("caosdb-v-success-query-shortcut", false);
            }, 2000);
        }, 5000);
    }

    this.append_messages_handler = function (panel, results) {
        for (const entity of results) {
            var result_id = undefined;
            if ($(entity).is("[data-entity-id]")) {
                result_id = $(entity).attr("data-entity-id");
            } else {
                result_id = $(elem).attr("data-entity-id");
            }
            if (typeof result_id !== "undefined") {
                var alerts = $(entity)
                    .find("div.alert")
                    .toggleClass("col-md-12");
                $(panel)
                    .find(".caosdb-f-user-query-shortcut[data-entity-id='" +
                        result_id + "']")
                    .append(alerts);
            }
        }
    }

    /**
     * Reset the query shortcuts, i.e. remove the old container and call init
     * again.
     */
    this.reset = async function () {
        $(".caosdb-shortcuts-container").remove();
        return await query_shortcuts.init();
    }

    /**
     * Reset the shortcut container, highlight the new shortcuts.
     */
    this.handle_create_success = async function (panel, results) {
        query_shortcuts.logger.trace("enter handle_create_success", panel, results);
        var new_panel = await query_shortcuts.reset();
        query_shortcuts.highlight_resulting_entities(new_panel, results);

        $(new_panel).append(query_shortcuts.make_dismissible_alert("info", "<strong>Success</strong> New shortcuts have been created."));
        query_shortcuts.logger.trace("leave handle_create_success");
    }


    /**
     * Reset the shortcut container, highlight the updated shortcuts.
     */
    this.handle_update_success = async function (panel, results) {
        query_shortcuts.logger.trace("enter handle_update_success", panel, results);
        var new_panel = await query_shortcuts.reset();
        query_shortcuts.highlight_resulting_entities(new_panel, results);

        $(new_panel).append(query_shortcuts.make_dismissible_alert("info", "<strong>Success</strong> Shortcuts have been updated."));
        query_shortcuts.logger.trace("leave handle_update_success");
    }

    this.make_dismissible_alert = function (type, content) {
        var ret = $(
            `<div class="alert alert-` + type + ` alert-dismissible" role="alert">
              <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
              </button>
            </div>`);
        ret.append(content);
        return ret[0];
    }

    /**
     * Add the successfully-deleted message to the deleted shortcuts.
     *
     * This method does not reset the shortcuts container, so the form's [OK]
     * button is still visible.
     */
    this.handle_delete_success = function (panel, results) {
        query_shortcuts.logger.trace("enter handle_delete_success", panel, results);

        query_shortcuts.append_messages_handler(panel, results);
        query_shortcuts.logger.trace("leave handle_delete_success");
        return query_shortcuts.make_dismissible_alert("info", "<strong>Success</strong> Shortcuts have been deleted.");
    }

    /**
     * Add the error messages to the shortcuts panel and inform the user that
     * no shortcut has been deleted.
     *
     * This method does not reset the shortcuts container, so the form's
     * [Submit] and [Cancel] buttons are still visible.
     */
    this.handle_delete_error = function (panel, results) {
        query_shortcuts.logger.trace("enter handle_delete_error", panel, results);

        query_shortcuts.append_messages_handler(panel, results);
        query_shortcuts.logger.trace("leave handle_delete_error");
        return query_shortcuts.make_dismissible_alert("danger", "<strong>Failure</strong> Shortcuts have not been deleted.");
    }


    this.make_shortcut_buttons = function (execute, customize) {
        const execute_button = $('<button class="btn btn-primary" type="button" title="Execute query."><i class="bi-search"></i></button>')
            .css({
                "font-size": "12px"
            })
            .click(execute);
        const customize_button = $('<button class="btn btn-primary" type="button" title="Write to the Query Panel for customization."><i class="bi-pencil"></i></button>')
            .css({
                "font-size": "12px"
            })
            .click(customize)
            .mouseenter(function (e) {
                $("#caosdb-query-textarea")
                    .css({
                        "border-color": "#66afe9",
                        "outline": 0,
                        "-webkit-box-shadow": "inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgba(102,175,233,.6)",
                        "box-shadow": "inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgba(102,175,233,.6)"
                    });
            })
            .mouseleave(function (e) {
                $('#caosdb-query-textarea').attr("style", "");
            });
        const button_group = $('<div class="btn-group"/>')
            .css({
                "font-size": "12px",
                position: "absolute",
                top: 0,
                right: "12px"
            })
            .append(customize_button, execute_button)
            .hide()
            .mouseleave(function (e) {
                $(this).hide();
            });

        const hover = $('<button class="btn btn-secondary caosdb-button-search"><i class="bi-search" aria-hidden="true"></i></button>')
            .css({
                "font-size": "12px"
            })
            .click(execute)
            .mouseenter(() => {
                button_group.fadeIn(150);
            });

        return [hover[0], button_group[0]];
    }

    /**
     * Return a FORM element which lets the user delete one or more
     * user-defined query shortcuts.
     *
     * The form is generated from the content of the shortcuts-panel.
     *
     * @param {HTMLElement} panel - the original shortcuts-panel.
     * @param {function} delete_callback - the submit function for the form.
     * @returns {HTMLElement} a form.
     */
    this.make_delete_form = function (panel, delete_callback, success_handler, error_handler) {
        caosdb_utils.assert_html_element(panel, "param `panel`");
        caosdb_utils.assert_type(delete_callback, "function", "param `delete_callback`");
        // clone panel
        var cloned = $(panel.outerHTML);

        // remove old header and toolbox button
        cloned.find(".caosdb-f-shortcuts-panel-header .h3").text("Delete Shortcuts");
        cloned.find(".caosdb-f-shortcuts-toolbox-button").remove();
        // show
        cloned.children().show();

        // make form fields manually, only use the form_elements to wrap it.
        cloned.find(".caosdb-f-query-shortcut").each((idx, item) => {
            var wrapper = $(item).find(".col-md-2.text-end");

            // disable the inputs of the query shortcut
            $(item).find(":input").prop("disabled", true);

            // remove old buttons
            wrapper.children().remove();
            if ($(item).hasClass("caosdb-f-user-query-shortcut")) {
                // user shortcut
                // insert checkbox
                var entity_id = $(item).attr("data-entity-id");

                // TODO use form_elements.make_checkbox_input
                var input = $('<input type="checkbox" name="' + entity_id + '"/>')
                    .attr("title", "Delete this shortcut.");
                wrapper.append(input);
            } else {
                // global shortcut
                // gray out and add hint that this is a global shortcut which cannot be deleted.
                $(item)
                    .css({
                        "color": "#CCCCCC"
                    })
                    .attr("title", "This is a global shortcut which cannot be deleted.");
            }
        });



        var config = {
            submit: delete_callback,
            success: success_handler,
            error: error_handler,
            fields: cloned.children().toArray(),
        };

        var form = form_elements.make_form(config);
        return form;
    }

    this.make_form_entity = function (entity_id) {
        var entity = $(`
            <div class="invisible" data-entity-role="Record">
            </div>`);
        if (typeof entity_id === "string" || entity instanceof String) {
            entity.attr("data-entity-id", entity_id);
            entity.append(`<div class="caosdb-id">` +
                entity_id + `</div>`);
        }
        return entity[0];
    }


    this.make_query_shortcut_parent = function () {
        return $(`<div class="caosdb-parent-name">` +
            query_shortcuts._shortcuts_record_type_name + `</div>`)[0];
    }


    /**
     * Return a FORM element which lets the user create a new user-defined
     * query shortcuts.
     *
     * @param {HTMLElement} panel - the original shortcuts-panel.
     * @param {function} create_callback - the submit function for the form.
     * @returns {HTMLElement} a form.
     */
    this.make_create_form = function (panel, create_callback, success_handler, error_handler) {
        caosdb_utils.assert_html_element(panel, "param `panel`");
        caosdb_utils.assert_type(create_callback, "function", "param `create_callback`");
        // clone panel
        var cloned = $(panel.outerHTML);

        // remove old header and toolbox button
        cloned.find(".caosdb-f-shortcuts-panel-header .h3").text("Create Shortcut");
        cloned.find(".caosdb-f-shortcuts-toolbox-button").remove();
        // show
        cloned.children().show();

        var entity = $(this.make_form_entity());
        entity.append(this.make_query_shortcut_parent());

        var form_config = {
            name: "create_shortcut",
            header: "Create Shortcut",
            submit: create_callback,
            success: success_handler,
            error: error_handler,
            cache_event: "caosdb.field.changed",
            fields: this.make_create_fields(entity[0]),
        };
        var form = form_elements.make_form(form_config);
        this._toggle_entity_property_class(form);


        this.logger.trace("leave make_create_form", form);
        return form;
    }

    /**
     * Add the "caosdb-f-entity-property" class to the form fields. Thus the
     * fields are findable by the `getEntityXML` method which is used in
     * `get_shortcut_entities` to generate the entity xml from the shortcut
     * form.
     *
     * @param {HTMLElement} form - form which contains the fields where the
     * class is to be added.
     */
    this._toggle_entity_property_class = function (form) {
        form.addEventListener("caosdb.form.ready", () => {
            $(form).find(".caosdb-f-field").toggleClass("caosdb-f-entity-property", true);
        });
        $(form).find(".caosdb-f-field").toggleClass("caosdb-f-entity-property", true);
    }

    this.make_create_fields = function (include) {
        return [
            include,
            {
                type: "text",
                name: query_shortcuts._shortcuts_property_description_name,
                label: "Description",
                required: true,
                cached: true,
                //help: query_shortcuts._description_help, TODO
            }, {
                type: "text",
                name: query_shortcuts._shortcuts_property_query_name,
                label: "Query",
                required: true,
                cached: true,
                //help: query_shortcuts._query_help, TODO
            }
        ];
    }


    this.make_update_fields = function (include, old_description, old_query) {
        var fields = this.make_create_fields(include);
        fields[1]["cached"] = false;
        fields[2]["cached"] = false;
        fields[1]["value"] = old_description;
        fields[2]["value"] = old_query;
        return fields;
    }


    this.get_description_id = function () {
        while (typeof query_shortcuts._shortcuts_property_description_id == "undefined") {
            // wait
        }
        return query_shortcuts._shortcuts_property_description_id;
    }


    this.get_query_id = function () {
        while (typeof query_shortcuts._shortcuts_property_query_id == "undefined") {
            // wait
        }
        return query_shortcuts._shortcuts_property_query_id;
    }

    this.get_record_type_id = function () {
        while (typeof query_shortcuts._shortcuts_record_type_id == "undefined") {
            // wait
        }
        return query_shortcuts._shortcuts_record_type_id;
    }


    this._description_help = {
        "title": "Description of a Query Shortcut",
        "content": " <i>Example:</i> <code>Search for experiments in {year}</code>.  <p> The description of a query shortcut is the text that is displayed to the users in the shortcuts section below the main query input field.  </p> <p> When you insert placeholders into the description, they are replaced by input fields. The placeholders are surrounded by curly brackets and must occur in the query too.  </p> <p> You can use any number of placeholders. They just have to have unique names.  </p>",
        "html": true
    };


    this._query_help = {
        "title": "Query of a Query Shortcut",
        "content": " <i>Example:</i> <code>FIND experiment WITH date in {year}</code>.  <p> The query of a query shortcut must conform to the Query Language syntax with the exception that it may contain placeholder which are surrounded by curly brackets.  </p> <p> In the example, 'year' is a placeholder and it will be set to value of the corresponding input field, that is shown in the description in the shortcuts section below the main query input field.  </p>",
        "html": true
    };


    /**
     * Return a FORM element which lets the user update a existing user-defined
     * query shortcuts.
     *
     * @param {HTMLElement} panel - the original shortcuts-panel.
     * @param {function} update_callback - the submit function for the form.
     * @returns {HTMLElement} a form.
     */
    this.make_update_form = function (panel, entity_id, update_callback, success_handler, error_handler) {
        caosdb_utils.assert_html_element(panel, "param `panel`");
        caosdb_utils.assert_type(update_callback, "function", "param `update_callback`");
        caosdb_utils.assert_type(success_handler, "function", "param `success_handler`");
        caosdb_utils.assert_type(error_handler, "function", "param `error_handler`");


        // remove old form
        form_elements.dismiss_form(panel);

        // clone panel
        var cloned = $(panel.outerHTML);

        // remove old header and toolbox button
        cloned.find(".caosdb-f-shortcuts-panel-header .h3").text("Edit Shortcut");
        cloned.find(".caosdb-f-shortcuts-toolbox-button").remove();
        // show
        cloned.children().show();

        var entity = $(this.make_form_entity(entity_id));
        entity.append(this.make_query_shortcut_parent());

        var olddef = $(panel).find(".caosdb-f-query-shortcut[data-entity-id='" + entity_id + "']");

        var form_config = {
            name: "edit_shortcut",
            header: "Edit Shortcut",
            submit: update_callback,
            success: success_handler,
            error: error_handler,
            fields: this.make_update_fields(entity[0], olddef.attr("data-shortcut-description"), olddef.attr("data-query-string")),
        };
        var form = form_elements.make_form(form_config);
        this._toggle_entity_property_class(form);

        this.logger.trace("leave make_update_form", form);
        return form;
    }

    this._cache_visibility_key = "caosdb.query-shortcuts-panel.visible";

    // deps
    this._updateEntities = update;
    this._insertEntities = insert;
    this._deleteEntities = transaction.deleteEntities;
    this._transformEntities = transformation.transformEntities;

    /**
     * Return the check entities of the form as an array of ids.
     */
    this.get_checked_ids = function (form) {
        var checked = [];
        $(form).find(":checked").each((idx, item) => {
            checked.push(item.name);
        });
        return checked;
    }

    /**
     * Return the entities of the form in XML representation.
     */
    this.get_shortcut_entities = function (form) {
        var entities = [];
        entities.push(getEntityXML(form));
        return entities;
    }

    /**
     * @throws {HTMLElement[]} if the results contains `DIV.alert-errors`.
     * @returns {HTMLElement[]} if the transaction was successful.
     */
    this.delete_callback = async function (form) {
        query_shortcuts.logger.trace("enter delete_callback", form);

        var dels_array = query_shortcuts.get_checked_ids(form);
        query_shortcuts.logger.debug("delete", dels_array);

        var response = await query_shortcuts._deleteEntities(dels_array);

        var ret = await query_shortcuts.transform_entities(response);

        query_shortcuts.logger.trace("leave delete_callback", ret);
        return ret;
    }

    /**
     * @throws {HTMLElement[]} if the results contains `DIV.alert-errors`.
     * @returns {HTMLElement[]} if the transaction was successful.
     */
    this.create_callback = async function (form) {
        query_shortcuts.logger.trace("enter create_callback", form);

        var insert_xml = query_shortcuts.get_shortcut_entities(form);
        query_shortcuts.logger.debug("create new shortcuts", insert_xml);

        var response = await query_shortcuts._insertEntities(insert_xml);

        var ret = await query_shortcuts.transform_entities(response);

        query_shortcuts.logger.trace("leave create_callback", ret);
        return ret;
    }

    /**
     * @throws {HTMLElement[]} if the results contains `DIV.alert-errors`.
     * @returns {HTMLElement[]} if the transaction was successful.
     */
    this.update_callback = async function (form) {
        query_shortcuts.logger.trace("enter update_callback", form);

        var update_xml = query_shortcuts.get_shortcut_entities(form);

        // the two properties
        var desc_id = query_shortcuts.get_description_id();
        var query_id = query_shortcuts.get_query_id();

        // the recordtype for user-defined query shortcuts
        var qs_id = query_shortcuts.get_record_type_id();

        $(update_xml).find("Property[name='" + query_shortcuts._shortcuts_property_description_name + "']").attr("id", desc_id);
        $(update_xml).find("Property[name='" + query_shortcuts._shortcuts_property_query_name + "']").attr("id", query_id);
        $(update_xml).find("Parent[name='" + query_shortcuts._shortcuts_record_type_name + "']").attr("id", qs_id);
        query_shortcuts.logger.debug("update shortcuts", update_xml);

        var response = await query_shortcuts._updateEntities(update_xml);

        var ret = await query_shortcuts.transform_entities(response);

        query_shortcuts.logger.trace("leave update_callback", ret);
        return ret;
    }

    /**
     * Transform the reponse XML into an array of entities in HTML
     * representation.
     */
    this.transform_entities = async function (response) {
        var ret = await query_shortcuts._transformEntities(response);

        // throw errors:
        var errors = $(ret).find(".alert.alert-danger").toArray();
        if (errors.length > 0) {
            query_shortcuts.logger.debug("errors in shortcuts creation", errors);
            throw ret;
        }
        return ret;
    }


    // TODO move to css file
    var styles = `
    .caosdb-f-shortcuts-toolbox-button {
            font-size: 14px;
        }
        .caosdb-f-shortcuts-toolbox-button .dropdown-toggle {
            background-color: transparent;
            color: #777;
        }
        .caosdb-f-shortcuts-toolbox-button .dropdown-toggle:hover {
            color: #333;
        }
        .caosdb-f-shortcuts-toolbox-button li button {
            display: block;
            padding: 3px 20px;
            border: none;
            width: 100%;
            text-align: left;
    }

    .caosdb-f-query-shortcut button,
    .caosdb-f-query-shortcut div {
            min-height: 32px;
    }

    div.text-end ul.dropdown-menu {
        left: unset;
        right: 0;
    }

    .caosdb-f-query-shortcut:hover {
        background-color: #eaeaea;
    }

    .caosdb-f-user-query-shortcut.caosdb-v-success-query-shortcut {
          -moz-transition-property: background-color;  /* FF4+ */
          -moz-transition-duration: 2s;
          -webkit-transition-property: background-color;  /* Saf3.2+, Chrome */
          -webkit-transition-duration: 2s;
          -o-transition-property: background-color;  /* Opera 10.5+ */
          -o-transition-duration: 2s;
          -ms-transition-property: background-color;  /* IE10? */
          -ms-transition-duration: 2s;
          transition-property: background-color;  /* Standard */
          transition-duration: 2s;
    }

    .caosdb-f-shortcuts-panel-body {
        border-top: 1px solid #C0C0C0;
    }

    .caosdb-f-shortcuts-panel-toggle-button {
        margin-right: 4px;
        cursor: pointer;
        font-size: 18px;
        color: #777;
        }
        .caosdb-f-shortcuts-panel-toggle-button:hover {
            color: #333;
    }

    .caosdb-f-query-panel.condensed .caosdb-shortcuts-container {
        display: none;
    }
    `;

    var styleSheet = document.createElement("style");
    styleSheet.type = "text/css";
    styleSheet.innerText = styles;
    document.head.appendChild(styleSheet);

}



$(document).ready(function () {
    caosdb_modules.register(query_shortcuts);
});

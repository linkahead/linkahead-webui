/**
 * preview module contains functionality for the preview of referenced entities.
 */
var preview = new function() {

    this.previewReadyEvent = new Event("caosdb.preview.ready");
    this.showPreviewEvent = new Event("caosdb.preview.show");
    this.hidePreviewEvent = new Event("caosdb.preview.hide");

    this.carouselId = 0;
    this.classNameEntityPreview = "caosdb-entity-preview";
    this.classNameShowPreviewButton = "caosdb-show-preview-button";
    this.classNameHidePreviewButton = "caosdb-hide-preview-button";
    this.classNamePreview = "caosdb-preview-container";
    this.classNamePreviewCarouselNav = "caosdb-preview-carousel-nav";
    this.classNameNotificationArea = globalClassNames.NotificationArea;
    this.classNameWaitingNotification = globalClassNames.WaitingNotification;
    this.classNameErrorNotification = globalClassNames.ErrorNotification;

    /**
     * Initialize the preview feature for all reference properties in the current window.
     * 
     * @return {HTMLElement[]} The initialized properties.
     */
    this.init = function() {
        let props = [];
        $('.caosdb-entity-panel').each((index, entity) => {
            props.concat(preview.initEntity(entity))
        });
        return props;
    }

    // import from global name space.
    this.getEntityID = getEntityID;
    this.getEntityVersion = getEntityVersion;
    this.getEntityIdVersion = getEntityIdVersion;

    /**
     * Get the id and, if present, the version of an entity from a link or a
     * displayed reference.
     *
     * @param {HTMLElement} a link to an entity.
     * @return {string} <id>[@<version>]
     */
    this.getEntityRef = function (link) {
        return link.getElementsByClassName("caosdb-id")[0].textContent;
    }

    /**
     * Initialize the preview feature for all reference properties which belong to certain entity.
     *
     * @param {HTMLElement} entity
     * @return {HTMLElement[]} The initialized properties. 
     */
    this.initEntity = function(entity) {
        let props = [];
        $(entity).find('.caosdb-properties > .list-group-item').each((index, property) => {
            let refLinksContainer = preview.getRefLinksContainer(property);
            if (refLinksContainer != null) {
                props.push(preview.initProperty(property));
            }
        });
        return props;
    }
    /**
     * Initialize the preview feature for a certain reference property.
     * @param {HTMLElement} ref_property_elem
     * @return {Object} A state machine.
     */
    this.initProperty = function(ref_property_elem) {
        let showPreviewButton = preview.createShowPreviewButton();
        let hidePreviewButton = preview.createHidePreviewButton();
        let notificationArea = preview.createNotificationArea();
        let refLinksContainer = preview.getRefLinksContainer(ref_property_elem);

        let app = new StateMachine({
            transitions: [{
                name: "init",
                from: 'none',
                to: "showLinks"
            }, {
                name: "requestPreview",
                from: 'showLinks',
                to: () => (preview.hasPreview(ref_property_elem) ? 'showPreview' : 'waiting')
            }, {
                name: "receivePreview",
                from: 'waiting',
                to: 'showPreview'
            }, {
                name: "hidePreview",
                from: 'showPreview',
                to: 'showLinks'
            }, {
                name: "resetApp",
                from: '*',
                to: 'showLinks'
            }, ],
        });

        let executeFailSave = function(fn) {
            try {
                fn();
            } catch (e) {
                globalError(e);
                setTimeout(() => {
                    app.resetApp(e);
                }, 1000);
            }
        };
        // for debugging: 
        //app.onTransition = function(e){
        //    console.log(e);
        //}
        app.onEnterShowLinks = function(e) {
            executeFailSave(function() {
                $(showPreviewButton).show();
                $(hidePreviewButton).hide();
                $(refLinksContainer).show();
                $(preview.getPreviewCarousel(ref_property_elem)).hide();
                ref_property_elem.dispatchEvent(preview.hidePreviewEvent);
            });
        };
        app.onLeaveShowLinks = function(e) {
            executeFailSave(function() {
                $(showPreviewButton).hide();
                preview.removeAllErrorNotifications(ref_property_elem);
            });
        };
        app.onEnterWaiting = function(e) {
            executeFailSave(function() {
                preview.addWaitingNotification(ref_property_elem, preview.createWaitingNotification());
                let entityIds = preview.getAllEntityRefs(refLinksContainer);
                preview.retrievePreviewEntities(entityIds).then(entities => {
                    app.receivePreview(entities);
                }, err => {
                    app.resetApp(err);
                });
            });
        };
        app.onReceivePreview = function(e, entities) {
            executeFailSave(function() {
                preview.addPreview(ref_property_elem, preview.createPreview(entities, refLinksContainer));
                // TODO: Check whether this is needed.
                resolve_references.init();
            });
        }
        app.onLeaveWaiting = function() {
            executeFailSave(function() {
                removeAllWaitingNotifications(ref_property_elem);
            });
        }
        app.onEnterShowPreview = function(e) {
            executeFailSave(function() {
                $(preview.getPreviewCarousel(ref_property_elem)).show();
                $(hidePreviewButton).show();
                $(refLinksContainer).hide();
                ref_property_elem.dispatchEvent(preview.showPreviewEvent);
            });
        }
        app.onResetApp = function(e, error) {
            removeAllWaitingNotifications(ref_property_elem);
            preview.removeAllErrorNotifications(ref_property_elem);
            // remove carousel
            if (preview.hasPreview(ref_property_elem)) {
                $(preview.getPreviewCarousel(ref_property_elem)).remove();
            }
            if (error != null) {
                preview.addErrorNotification(ref_property_elem, createErrorNotification(error));
            }
        }


        // start with showLinks state
        app.init();
        showPreviewButton.onclick = () => app.requestPreview();
        hidePreviewButton.onclick = () => app.hidePreview();
        preview.addShowPreviewButton(ref_property_elem, showPreviewButton);
        preview.addNotificationArea(ref_property_elem, notificationArea);
        preview.addHidePreviewButton(ref_property_elem, hidePreviewButton);

        return app;
    }

    this.createWaitingNotification = function() {
        return createWaitingNotification("Loading preview. Please wait.");
    }

    /**
     * Determine if a previewContainer is already present in a property's div.
     * 
     * @param {HTMLElement} property.
     * @return {Boolean}
     */
    this.hasPreview = function(property) {
        return property.getElementsByClassName(this.classNamePreview).length > 0;
    }

    /**
     * Add a preview container to a property div.
     * 
     * @param {HTMLElement} property - Where to add the preview.
     * @param {HTMLElement} previewContainer - The container which is to be added.
     * @return {HTMLElement} The parameter `property`.
     */
    this.addPreview = function(property, previewContainer) {
        property.getElementsByClassName("caosdb-f-property-value")[0].appendChild(previewContainer);
        property.dispatchEvent(preview.previewReadyEvent);
        return property;
    }

    /**
     * Transform the raw xml response of the server into an array of entities for preview.
     *
     * @param {Promise | XMLDocument} xml - A Promise for the servers xml response.
     * @return {Promise | HTMLElement[]} A Promise for an array of entities.
     */
    this.processPreviewResponse = function(xml) {
        let xsl = preview.getEntityXsl();
        return preview.transformXmlToPreviews(xml, xsl);
    }

    /**
     * Retrieve the XSL script for entities from the server.
     *
     * @return {Promise | XMLDocument} A Promise for the XSL script.
     */
    this.getEntityXsl = async function _getEntityXsl() {
        return transformation.retrieveEntityXsl();
    };

    /**
     * Add a notification area to a reference property.
     *
     * @param {HTMLElement} property
     * @param {HTMLElement} notificationArea
     * @return {HTMLElement} The parameter `property`.
     */
    this.addNotificationArea = function(property, notificationArea) {
        property.getElementsByClassName("caosdb-f-property-value")[0].appendChild(notificationArea);
        return property;
    }

    /**
     * Create a new `show preview` button.
     * @return {HTMLElement} A button for showing the preview carousel.
     */
    this.createShowPreviewButton = function() {
        return $('<button class="' + preview.classNameShowPreviewButton + ' align-self-start btn btn-link btn-sm" title="Show preview of the referenced entities."><i class="bi-eye-fill"></i></button>')[0];
    }

    /**
     * Create a new `hide preview` button.
     * @return {HTMLElement} A button for hiding the preview carousel.
     */
    this.createHidePreviewButton = function() {
        return $('<button class="' + preview.classNameHidePreviewButton + ' align-self-start btn btn-link btn-sm" title="Hide preview and show links."><i class="bi-eye-slash-fill"></i></button>')[0];
    }

    /**
     * Create a notification area. That is a div with class `caosdb-preview-notification-area`.
     * @return {HTMLElement} A div
     */
    this.createNotificationArea = function() {
        return $('<div class="' + preview.classNameNotificationArea + '"></div>')[0];
    }

    /**
     * Add a showPreviewButton to a reference property's value section.
     * 
     * The button is appended to the first occuring element with class `caosdb-f-property-value`.
     * 
     * @param {HTMLElement} ref_property_elem
     * @param {HTMLElement} buttom_elem
     * @return {HTMLElement} parameter `ref_property_elem`
     */
    this.addShowPreviewButton = function(ref_property_elem, button_elem) {
        caosdb_utils.assert_html_element(button_elem, "param `button_elem`");
        $(ref_property_elem.getElementsByClassName("caosdb-f-property-value")[0]).prepend(button_elem);
        return ref_property_elem;
    }

    /**
     * Add a hidePreviewButton to a reference property's value section.
     * 
     * The button is appended to the first occuring element with class `caosdb-f-property-value`.
     * 
     * @param {HTMLElement} ref_property_elem
     * @param {HTMLElement} buttom_elem
     * @return {HTMLElement} The parameter `ref_property_elem`.
     */
    this.addHidePreviewButton = function(ref_property_elem, button_elem) {
        ref_property_elem.getElementsByClassName("caosdb-f-property-value")[0].appendChild(button_elem);
        return ref_property_elem;
    }

    /**
     * Add an error notification to a properties value section.
     * 
     * The error element is appended to the first occuring element with class 
     * `caosdb-preview-notification_area`.
     * 
     * @param {HTMLElement} ref_property_elem
     * @param {HTMLElement} error_elem
     * @return {HTMLElement} The parameter `ref_property_elem`.
     */
    this.addErrorNotification = function(ref_property_elem, error_elem) {
        ref_property_elem.getElementsByClassName(
            preview.classNameNotificationArea)[0].appendChild(error_elem);
        return ref_property_elem;
    }

    /**
     * Add a waiting notification to a properties value section.
     * 
     * The notification element is appended to the first occuring element with class 
     * `caosdb-preview-notification_area`.
     * 
     * Show a waiting notification while the entity request is pending.
     * @param {HTMLElement} ref_property_elem - Add `waiting_elem` here.
     * @param {HTMLElement} waiting_elem - The waiting notification.
     * @return {HTMLElement} The parameter `ref_property_elem`.
     */
    this.addWaitingNotification = function(ref_property_elem, waiting_elem) {
        ref_property_elem.getElementsByClassName(
            preview.classNameNotificationArea)[0].appendChild(waiting_elem);
        return ref_property_elem;
    }

    /**
     * Get a container of reference links or the single reference link of a reference property.
     * @param {HTMLElement} ref_property_elem
     * @return {HTMLElement} A div with links in it.
     */
    this.getRefLinksContainer = function(ref_property_elem) {
        if (ref_property_elem == null) {
            throw new Error("parameter `ref_property_elem` was null.");
        }
        let refLinksList = $(ref_property_elem).find('.caosdb-value-list').has('.caosdb-id')[0];
        if (refLinksList == null) {
            return $(ref_property_elem).find('.caosdb-f-property-value > .btn').has('.caosdb-id')[0];
        }
        return refLinksList
    }

    /**
     * Get the preview carousel of a reference property.
     * @param {HTMLElement} ref_property_elem
     * @return {HTMLElement} A div with the carousel and the navigation bar.
     */
    this.getPreviewCarousel = function(ref_property_elem) {
        return ref_property_elem.getElementsByClassName(preview.classNamePreview)[0];
    }

    /**
     * Get the showPreviewButton of a reference property.
     * @param {HTMLElement} ref_property_elem
     * @return {HTMLElement} A button for showing the preview carousel. 
     */
    this.getShowPreviewButton = function(ref_property_elem) {
        return ref_property_elem.getElementsByClassName(preview.classNameShowPreviewButton)[0];
    }

    /**
     * Get the hidePreviewButton of a reference property.
     * @param {HTMLElement} ref_property_elem
     * @return {HTMLElement} A button for hiding the preview carousel. 
     */
    this.getHidePreviewButton = function(ref_property_elem) {
        return ref_property_elem.getElementsByClassName(preview.classNameHidePreviewButton)[0];
    }

    /**
     * Remove all error notifications from the notification area of a reference property.
     * @param {HTMLElement} ref_property_elem
     * @return {HTMLElement} The parameter `ref_property_elem`.
     */
    this.removeAllErrorNotifications = function(ref_property_elem) {
        $(ref_property_elem.getElementsByClassName(preview.classNameErrorNotification)).remove();
        return ref_property_elem;
    }

    /**
     * Create a preview carousel from an array of entity elements.
     *
     * A carousel consists of the main div with class `carousel slide` and a
     * unique ID which will be generated here. Inside there are the navigation
     * bar with class `caosdb-preview-carousel-nav`, and the inner div which
     * contains and show the several slides with class `carousel-inner`.
     *
     * The refLinksContainer are cloned and modified such that they trigger the
     * sliding and added to the navigation bar. Then a set of empty slides is
     * added to the inner div. The entities are put into the correct slide
     * using the data-bs-slide-to attributes and the entity id of each selector
     * button.
     *
     * @param {HTMLElement[]} entities - The array of entity elements.
     * @param {HTMLElement} refLinksContainer - The original reference links.
     * @return {HTMLElement} A new preview carousel.
     */
    this.createPreviewCarousel = function(entities, refLinksContainer) {
        if (entities == null) {
            throw new Error("entities must not be null.");
        }
        let carouselId = ("previewCarousel" + preview.carouselId++);
        let nav = preview.createCarouselNav(refLinksContainer, carouselId); //preserves order, first is active
        let N = $(nav).find('[data-bs-slide-to]').length;
        let inner = preview.createEmptyInner(N) //no content, first is active

        let selectorButtons = preview.getSelectorButtons(nav);
        selectorButtons.each((index, button) => {
            let slide_id = button.getAttribute("data-bs-slide-to");
            let entity_id_version = preview.getEntityRef(button);
            let entity = preview.getEntityByIdVersion(entities, entity_id_version);
            if (entity == null) throw new Error("Entity with ID " + entity_id_version + " could not be found!");
            inner.children[slide_id].appendChild(preview.preparePreviewEntity(entity));
        });

        let mainDiv = $('<div data-bs-interval="false" class="carousel slide"></div>')[0];
        mainDiv.appendChild(nav);
        mainDiv.appendChild(inner);
        mainDiv.id = carouselId;

        $(mainDiv).on('slid.bs.carousel', preview.triggerUpdateActiveSlideItemSelector);

        return mainDiv;
    }

    /**
     * Get the selector buttons from a div which contains them or return the
     * single selector button if the `refLinksContainer` parameter is itself
     * the selector button.
     *
     * @param {HTMLElement} refLinksContainer
     * @return {jQuery} A collection of selector buttons.
     */
    this.getSelectorButtons = function(refLinksContainer) {
        return $(refLinksContainer).find('[data-bs-slide-to]');
    }

    /**
     * Create a single div with a preview of a single referenced entity or a fancy carousel if
     * there are more than one previews to be shown.
     * 
     * @param {HTMLElement[]} entities - The array of entity elements.
     * @param {HTMLElement} refLinksContainer - Container with the original reference links.
     * @return {HTMLElement} A div.
     */
    this.createPreview = function(entities, refLinksContainer) {
        var previewElement;
        if (preview.getReferenceLinks(refLinksContainer).length > 1) {
            previewElement = preview.createPreviewCarousel(entities, refLinksContainer);
        } else {
            previewElement = preview.createSinglePreview(entities, refLinksContainer);
        }

        $(previewElement).toggleClass(preview.classNamePreview, true);
        return previewElement;
    }

    /**
     * Create a single preview entity
     *
     */
    this.createSinglePreview = function(entities, refLinksContainer) {
        const entityRef = preview.getEntityRef(preview.getReferenceLinks(refLinksContainer)[0]);
        const entity = preview.preparePreviewEntity(preview.getEntityByIdVersion(entities, entityRef));
        return entity;
    }

    /**
     * Clone and prepare a single preview (which may be one of many previews in a carousel)
     * such that the header is clickable and links to the original entity.
     *
     * @param {HTMLElement} entity
     * @return {HTMLElement} The prepared entity.
     */
    this.preparePreviewEntity = function(entity) {
        const preparedEntity = entity.cloneNode(true);

        const href = connection.getBasePath() + transaction.generateEntitiesUri([preview.getEntityRef(entity)]);

        const link = $('<a title="Load this entity in a new window." href="' + href + '" class="btn" target="_blank"></a>');
        link.append('<i class="bi bi-box-arrow-up-right"></i>');

        const buttonsList = $(preparedEntity).find(".caosdb-v-entity-header-buttons-list");
        buttonsList.children().hide();
        buttonsList.append(link);

        return preparedEntity;
    }

    /**
     * Create the navigation bar for a carousel from original reference links to the entities. 
     * This contains selector buttons for each individual slide and prev/next buttons. The first 
     * selector button is active.
     * 
     * @param {HTMLElement} refLinksContainer
     * @param {String} carouselId (without leading hashtag)
     * @return {HTMLElement} A div with class `caosdb-preview-carousel-nav`.
     */
    this.createCarouselNav = function(refLinksContainer, carouselId) {
        if (carouselId == null) {
            throw new Error("carouselId must not be null.");
        }
        let prevButton = $('<a role="button" style="z-index: 5;position:absolute; top: 0;left:0px" class="btn btn-secondary btn-sm" href="#' + carouselId + '" data-bs-slide="prev"></a>')[0];
        prevButton.innerHTML = preview.carouselPrevButtonInnerHTML;
        let nextButton = $('<a role="button" style="z-index: 5;position:absolute; top: 0;right:0px" class="btn btn-secondary btn-sm" href="#' + carouselId + '" data-bs-slide="next"></a>')[0];
        nextButton.innerHTML = preview.carouselNextButtonInnerHTML;
        let nav = $('<div class="' + preview.classNamePreviewCarouselNav + '"></div>')[0];
        let selectors = refLinksContainer.cloneNode(true);
        // TODO handle case when ext_references already removed the
        // resolvable-reference class but did not resolve it yet.

        $(selectors).show();
        $(selectors).find('a.caosdb-f-reference-value').each((index, button) => {
            $(button).toggleClass("active", index === 0);
            button.removeAttribute("href");
            button.setAttribute("data-bs-slide-to", index);
            button.setAttribute("data-bs-target", "#" + carouselId);
        });
        nav.appendChild(prevButton);
        nav.appendChild(nextButton);
        nav.appendChild(selectors);

        return nav;
    };

    this.carouselPrevButtonInnerHTML = '<i class="bi-chevron-left" aria-hidden="true"></i><span class="visually-hidden">Previous</span>';
    this.carouselNextButtonInnerHTML = '<i class="bi-chevron-right" aria-hidden="true"></i><span class="visually-hidden">Next</span>';

    /**
     * Create a div with class `carousel-inner` which contains N divs with
     * class `carousel-item` while the first also has class `active`. These
     * item divs are empty.
     * 
     * @param {Number} N - An integer > 0.
     * @return {HTMLElement} A Div with class `carousel-inner`.
     */
    this.createEmptyInner = function(N) {
        if (N == null || isNaN(N) || N < 1) {
            throw new Error("N is to be an integer > 0");
        }
        let innerDiv = $('<div class="carousel-inner"><div class="carousel-item active"></div></div>')[0];
        let item = $('<div class="carousel-item"></div>')[0];
        for (let i = 1; i < N; i++) {
            innerDiv.appendChild(item.cloneNode());
        }
        return innerDiv;
    }

    /**
     * Get the entity with a certain ID and Version (if applicable) from an
     * array of entities. Returns null if no such entity is in the array.
     *
     * @param {HTMLElement[]} entities
     * @param {String} entity_id_version
     * @return {HTMLElement} Matching entity or null.
     */
    this.getEntityByIdVersion = function(entities, entity_id_version) {
        if (entities == null) {
            throw new Error("entities must not be null");
        }
        if (entity_id_version == null) {
            throw new Error("entity_id_version must not be null");
        }

        // if the entity_id_version contains an "@" it is actually a reference
        // to a versioned entity. Otherwise, it is just an id an thus only the
        // id has to be matched.
        const is_versioned = entity_id_version.indexOf("@") !== -1;
        var matches;
        if (is_versioned) {
            matches = (e) => preview.getEntityIdVersion(e) === entity_id_version;
        } else {
            matches = (e) => preview.getEntityID(e) === entity_id_version;
        }
        for (let i = 0; i < entities.length; i++) {
            const e = entities[i];
            if (matches(e)) {
                return e;
            }
        }
        return null;
    }

    /**
     * Find the index of the div.item.active element withing the div.carouse-inner element.
     * 
     * @param {HTMLElement} carousel
     * @returns {Number} The (integer) index of the active slide item of a given carousel.
     */
    this.getActiveSlideItemIndex = function(carousel) {
        let active_item = carousel.getElementsByClassName("carousel-inner")[0].getElementsByClassName("active")[0];
        let i = 0;
        while (active_item = active_item.previousElementSibling) {
            ++i
        }
        return i;
    }

    /**
     * Remove the `active` class from all slide items selectors of a carousel and add it to the new 
     * slide item selector denoted by the index.
     *
     * @param {HTMLElement} carousel
     * @param {Number} index - The (integer) index of the new active slide item selector.
     * @returns {HTMLElement} The parameter `carousel`.
     */
    this.setActiveSlideItemSelector = function(carousel, index) {
        if (carousel == null) {
            throw new Error("parameter `carousel` must not be null.");
        }
        if (index == null || isNaN(index) || index < 0) {
            throw new Error("parameter `index` is to be a non-null positive integer.");
        }
        let nav = $(carousel).find('.' + preview.classNamePreviewCarouselNav);
        nav.find('.active').toggleClass("active", false);
        $(preview.getSlideItemSelector(carousel, index)).toggleClass("active", true);

        preview.scrollCarouselNavToActiveSelector(nav);

        return carousel;
    }

    this.scrollCarouselNavToActiveSelector = function(nav) {
        let selector = nav.find('.active');
        let selectorPos = selector.position().left;

        let scrollbar = nav.find('.caosdb-value-list').has('.btn-group');
        let scrollbarWidth = scrollbar.innerWidth();
        let currentScroll = scrollbar.scrollLeft();

        let selectorPosInScrollBar = selectorPos - currentScroll;
        let distanceToMiddleOfScrollBar = scrollbarWidth / 2 - selectorPosInScrollBar;

        scrollbar.animate({
            scrollLeft: currentScroll - distanceToMiddleOfScrollBar
        }, 200);
        return nav;

    }

    /**
     * Get the slide item selector at postition `i`, starting with zero.
     *
     * @param {HTMLElement} carousel
     * @param {Number} i - The (integer) index of the slide item selector.
     * @returns {HTMLElement} The ith slide item selector. 
     */
    this.getSlideItemSelector = function(carousel, i) {
        let items = $(carousel).find('.' + preview.classNamePreviewCarouselNav).find('[data-bs-slide-to]');
        if (items.length <= i) {
            throw new Error("Index out of bounds.");
        }
        return items[i];
    }

    /**
     * Find the slideItemSelector which belongs to the next active slideItem and add the `active` class.
     * Note: The function which is to be bound to bootstrap's `slid.bs.carousel` event which is triggered
     * after the transition to a new slide is done.
     * 
     * @return true. 
     */
    this.triggerUpdateActiveSlideItemSelector = function(e) {
        let carousel = this;
        let index_active = preview.getActiveSlideItemIndex(carousel);
        preview.setActiveSlideItemSelector(carousel, index_active);
        return true;
    }

    /**
     * Retrieve a list of entities from the server.
     * 
     * @param {String[]} entityIds - The ids of the entities which are to be retrieved.
     * @return {Promise | HTMLElement[]} A Promise for an array of entities.
     */
    this.retrievePreviewEntities = async function _rPE(entityIds) {
        try {
            let xml = await connection.get(transaction.generateEntitiesUri(entityIds));
            return await preview.processPreviewResponse(xml);
        } catch (err) {
            if (err.message.startsWith("UriTooLongException")) {
                let chunks = preview.halfArray(entityIds);
                let first = await preview.retrievePreviewEntities(chunks[0]);
                let second = await preview.retrievePreviewEntities(chunks[1]);
                return first.concat(second);
            } else {
                throw err
            }
        }
    }

    /**
     * Split an array into two arrays.
     */
    this.halfArray = function(array) {
        if (array.length < 2) {
            throw new Error("Could not cut this array in half. It has a length of " + array.length);
        }
        let half = Math.floor(array.length / 2)
        return [array.slice(0, half), array.slice(half, array.length)]
    }

    /**
     * Transform the xml to an array of entities.
     * 
     * @param {Promise | XMLDocument} xml - The server response.
     * @param {Promise | XMLDocument} xsl - The xsl script.
     * @return {Promise | HTMLElement[]} A promise for an Array of HTMLElements.
     */
    this.transformXmlToPreviews = async function _tXTP(xml, xsl) {
        let html = await asyncXslt(xml, xsl);
        let entities = [];
        $(html).find('.caosdb-entity-panel').each((index, entity) => {
            entities[index] = entity;
            $(entity).toggleClass("caosdb-entity-panel", false).toggleClass(preview.classNameEntityPreview, true);
            $(entity).find('.caosdb-entity-actions-panel').remove();
        });
        return entities;
    }

    /**
     * Get an array of entity ids from a container of entity links.
     *
     * @param {HTMLElement} refLinksContainer
     * @return {String[]} An array of entity ids.
     */
    this.getAllEntityRefs = function(refLinksContainer) {
        if (refLinksContainer == null) {
            throw new Error("parameter refLinksContainer must not be null.");
        }

        let entityRefs = [];
        for (let link of preview.getReferenceLinks(refLinksContainer)) {
            entityRefs.push(preview.getEntityRef(link));
        };
        return entityRefs;
    }

    /**
     * Get an array of all reference links.
     * 
     * @param {HTMLElement} refLinksContainer - The original reference links.
     * @return {HTMLElement[]} A collection of links.
     */
    this.getReferenceLinks = function(refLinksContainer) {
        var cont = $(refLinksContainer);
        if (cont.is("a.caosdb-f-reference-value")) {
            return cont.toArray();
        }
        return cont.find('a.caosdb-f-reference-value').toArray();
    }
};


$(document).ready(function () {
    if ("${BUILD_MODULE_EXT_PREVIEW}" === "ENABLED") {
        caosdb_modules.register(preview);
    }
});

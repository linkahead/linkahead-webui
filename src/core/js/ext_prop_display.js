/*
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2022-2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2022-2023 Florian Spreckelsen <f.spreckelsen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
'use strict';

/**
 * Hide or show properties for specific roles and users
 * see src/doc/extension/display_of_properties.rst for documentation
 *
 * @requires jQuery (library)
 * @requires log (singleton from loglevel library)
 * @requires load_config (function from webcaosdb.js)
 */
var prop_display = new function ($, edit_mode, getEntityName, getEntityRole, getPropertyElements, getPropertyName, getUserName, getUserRoles, logger, load_config, preview, query) {

    /**
     * Return the property-display config file; `ext_prop_display.json` by
     * default.
     *
     * @param {string} resource - file name of the config file
     */
    this.load_config = async function (resource) {

        var conf = {};
        try {
            resource = resource || "ext_prop_display.json";
            conf = await load_config(resource);
        } catch (err) {
            logger.error(err);
        }

        return conf;
    }

    this.getEntitiesInView = function () {
        // Use all entities, both in entity panel and in preview.
        return $(".caosdb-entity-panel,.caosdb-entity-preview");
    }

    this.displayProperties = function (entities, conf, allTypes, userName, userRoles) {

        for (let ent of entities) {
            let parents = getParents(ent).map(par => par.name);
            let properties = getPropertyElements(ent);
            // either the entity has matching parents OR it is the actual
            // RecordType for which a rule is written.
            if (parents.some(par => allTypes.allTypesOrChildren.includes(par)) ||
                (getEntityRole(ent) == "RecordType" && allTypes.allTypesOrChildren.includes(getEntityName(ent)))) {
                // we know that there is at least one rule for this type (it is
                // in `allTypes.allTypesOrChildren`), but we don't know which tp
                // apply yet.
                for (let typeName of Object.keys(conf)) {
                    let typeConf = conf[typeName];
                    let allNames = allTypes.typesWithChildren[typeName];
                    // only change the display something if there is a match in
                    // at least one parent type
                    if (parents.some(par => allNames.includes(par)) ||
                        (getEntityRole(ent) == "RecordType" && allNames.includes(getEntityName(ent)))) {
                        // first sort the properties
                        this._sortProperties(ent, properties, typeConf);
                        properties.forEach((prop, index) => {
                            if (this._hide_property(getPropertyName(prop), userName, userRoles, typeConf)) {
                                // Should be hidden by default but better safe than sorry
                                $(prop).addClass("caosdb-v-hidden-property").removeClass("caosdb-v-show-property");
                            } else {
                                // show this property
                                $(prop).addClass("caosdb-v-show-property").removeClass("caosdb-v-hidden-property");
                            }
                        });
                    }
                }
            } else {
                // no rules for this RecordType, so show all properties
                properties.forEach((prop, index) => $(prop).addClass("caosdb-v-show-property").removeClass("caosdb-v-hidden-property"));
            }
        }
    }

    this._sortProperties = function (entity, properties, conf) {
        if (conf.order == undefined || conf.order.length == 0) {
            return;
        }
        properties.sort(function (a, b) {
            let confIndexA = conf.order.indexOf(getPropertyName(a));
            let confIndexB = conf.order.indexOf(getPropertyName(b));
            if (confIndexA < 0 && confIndexB < 0) {
                // both are not part of order list
                return 0;
            }
            if (confIndexA < 0) {
                // only b is part of order list , so it is placed before a
                return 1;
            }
            if (confIndexB < 0) {
                // only a is part of order list, so it is placed before b
                return -1;
            }
            // From here, we can assume that both are in the order list:
            return confIndexA - confIndexB;
        });
        $(entity).find(".caosdb-properties").append(properties);
    }

    this._hide_property = function (propname, userName, userRoles, conf) {

        // is this property only shown for certain users/groups?
        if ((conf.show != undefined) && conf.show.length > 0) {
            for (let def of conf.show) {
                if (propname.toLowerCase() == def.name.toLowerCase()) {
                    if (!(def.users.includes(userName)) && !(userRoles.some(role => def.roles.includes(role)))) {
                        return true
                    }
                }
            }
        }

        // is this property hidden for certain users/groups?
        if ((conf.hide != undefined) && conf.hide.length > 0) {
            for (let def of conf.hide) {
                if (propname.toLowerCase() == def.name.toLowerCase()) {
                    if (def.users.includes(userName) || userRoles.some(role => def.roles.includes(role))) {
                        return true
                    }
                }
            }
        }

        return false;
    }

    this._getRecordTypes = async function (conf) {

        const parentTypes = Object.keys(conf);

        var typesWithChildren = {};
        var allTypesOrChildren = [];

        for (let parentName of parentTypes) {
            const children = await query(`FIND RECORDTYPE "${parentName}"`);
            const names = children.map(ent => getEntityName(ent));
            typesWithChildren[parentName] = names;
            allTypesOrChildren = allTypesOrChildren.concat(names);
        }

        return {
            "typesWithChildren": typesWithChildren,
            "allTypesOrChildren": allTypesOrChildren
        };
    }

    this.unhideAllProperties = function () {
        // Just show all initially hidden properties
        $(".caosdb-v-hidden-property").removeClass("caosdb-v-hidden-property");
    }

    this._unhideAllPropertiesWrapper = function (original) {
        // construct a function that wirst does the original work, then unhides
        // all properties, then returns the original return values.
        const result = function (entity) {
            var original_return = undefined;
            if (typeof original === "function") {
                original_return = original(entity);
            }
            prop_display.unhideAllProperties();
            return original_return;
        }

        return result;
    }

    this._displayPropertiesWrapper = function (original, conf, allTypes) {
        // Same as above, but for when there actually may be something to hide
        // (i.e., build variable is set and config is non-empty).
        const result = function (entitiy) {
            var original_return = undefined;
            if (typeof original === "function") {
                original_return = original(entitiy);
            }
            var entities = prop_display.getEntitiesInView();
            const userName = getUserName();
            const userRoles = getUserRoles();
            prop_display.displayProperties(entities, conf, allTypes, userName, userRoles);
            return original_return;
        }

        return result;
    }

    this.init = async function () {
        const conf = await this.load_config();
        if (Object.keys(conf).length > 0) {
            const allTypes = await this._getRecordTypes(conf);
            var entities = this.getEntitiesInView();
            const userName = getUserName();
            const userRoles = getUserRoles();
            this.displayProperties(entities, conf, allTypes, userName, userRoles);
            // If we are in the edit mode, (un)hide properties after ending
            // the editing of an entity
            document.body.addEventListener(edit_mode.start_edit.type, (e) => {
                edit_mode.app.onAfterShowResults = this._displayPropertiesWrapper(edit_mode.app.onAfterShowResults, conf, allTypes);
            }, true);
            // After showing a preview for the first time, its entity card is
            // added to the dom tree, so the properties have to be (un)hidden
            // afterwards.
            document.body.addEventListener(preview.previewReadyEvent.type, (e) => {
                let newEntities = $(".caosdb-entity-preview");
                this.displayProperties(newEntities, conf, allTypes, userName, userRoles);
            }, true);

        } else {
            // There are no properties to be hidden, so make this clear in HTML body
            $("body").attr("data-hidden-properties", "false");
            this.unhideAllProperties();
            document.body.addEventListener(edit_mode.start_edit.type, (e) => {
                // also unhide properties when leaving the edit mode
                // TODO(fspreck): We're lacking a proper state/event here in the
                // edit mode, so do this on "init", since this is the state to which
                // the state machine returns after either successfully saving an
                // entity or canceling the edit.
                edit_mode.app.onAfterShowResults = this._unhideAllPropertiesWrapper(edit_mode.app.onAfterShowResults);
            }, true);
            document.body.addEventListener(preview.previewReadyEvent.type, (e) => this.unhideAllProperties(), true);
        }
    }
}($, edit_mode, getEntityName, getEntityRole, getPropertyElements, getPropertyName, getUserName, getUserRoles, log.getLogger("ext_prop_display"), load_config, preview, query);

$(document).ready(() => {
    if ("${BUILD_MODULE_EXT_PROPERTY_DISPLAY}" == "ENABLED") {
        caosdb_modules.register(prop_display);
    }
});

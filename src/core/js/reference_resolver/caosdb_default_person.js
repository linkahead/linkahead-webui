/*
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2021 IndiScale GmbH (info@indiscale.com)
 * Copyright (C) 2021 Florian Spreckelsen (f.spreckelsen@indiscale.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

/**
 * @module caosdb_default_person_reference
 * @version 0.1
 *
 * @description Replace the reference to a Person Record by the values of that
 * Record's firstname and lastname properties.
 *
 * TODO: Make name(s) of person RecordType(s) and names of firstname
 * and lastname properties configurable.
 */
var caosdb_default_person_reference = new function () {

    var logger = log.getLogger("caosdb_default_person_reference");

    const lastname_prop_name = "lastname"
    const firstname_prop_name = "firstname"
    const person_rt_name = "Person"

    /**
     * Return the name of a person as firstname + lastname
     */
    this.get_person_str = function (el) {
        var valpr = getProperties(el);
        if (valpr == undefined) {
            return;
        }
        return valpr.filter(valprel =>
                valprel.name.toLowerCase().trim() ==
                firstname_prop_name.toLowerCase())[0].value +
            " " +
            valpr.filter(valprel => valprel.name.toLowerCase().trim() ==
                lastname_prop_name.toLowerCase())[0].value;
    }

    this.resolve = async function (id) {

        const entity = (await resolve_references.retrieve(id))[0];

        if (resolve_references.is_child(entity, person_rt_name)) {
            return {
                "text": caosdb_default_person_reference.get_person_str(entity)
            };
        }
    }
}

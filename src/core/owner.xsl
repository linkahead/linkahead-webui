<?xml version="1.0" encoding="UTF-8"?>
<!--
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019 Timm Fitschen (t.fitschen@indiscale.com)
 * Copyright (C) 2019 IndiScale GmbH (info@indiscale.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
-->
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" />

    <xsl:variable name="base_uri">
        <xsl:call-template name="uri_ends_with_slash">
            <xsl:with-param name="uri" select="/Response/@baseuri" />
        </xsl:call-template>
    </xsl:variable>

    <xsl:template match="/">
        <html>
            <head>
                <link rel="stylesheet" type="text/css">
                    <xsl:attribute name="href">
                <xsl:value-of
                    select="concat($base_uri, 'webinterface/${BUILD_NUMBER}/owner.css')" />
                </xsl:attribute>
                </link>

            </head>
            <body>
                <xsl:apply-templates select="Response/*" />
            </body>
        </html>
    </xsl:template>

    <xsl:template match="Entity|Property|Record|RecordType">
        <div class="owners">
            Owners:
            <xsl:for-each select="Owner">
            <div class="owner"><xsl:value-of select="@role"></xsl:value-of></div>
            </xsl:for-each>
        </div>
    </xsl:template>

    <!-- assure that this uri ends with a '/' -->
    <xsl:template name="uri_ends_with_slash">
        <xsl:param name="uri" />
        <xsl:choose>
            <xsl:when test="substring($uri,string-length($uri),1)!='/'">
                <xsl:value-of select="concat($uri,'/')" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$uri" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>


#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# ** header v3.0
# This file is a part of the LinkAhead Project.
#
# Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
# Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header

import linkahead as db

# clean
old = db.execute_query("FIND Test*")
if len(old):
    old.delete()

# data model
datamodel = db.Container()
datamodel.extend([
    db.Property("TestProp", datatype=db.TEXT),
    db.RecordType("TestRecordType"),
])

datamodel.insert()


# test data
testdata = db.Container()

test_cases = [
    "no link",
    "https://example.com",
    "https://example.com and http://example.com",
    "this is text https://example.com",
    "this is text https://example.com and this as well",
    "this is text https://example.com and another linke https://example.com",
    "this is text https://example.com and another linke https://example.com and more text",
    ("this is a lot of text with links in it Lorem ipsum dolor sit amet, "
     "consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore "
     "et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud "
     "exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. "
     "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum "
     "dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non "
     "proident, sunt in culpa qui officia deserunt mollit anim id est "
     "laborum.https://example.com and another linke https://example.com and "
     "more text and here comes a very long link: "
     "https://example.com/this/has/certainly/more/than/40/characters/just/count/if/you/dont/believe/it.html"),
]
for test_case in test_cases:
    testdata.append(db.Record().add_parent("TestRecordType").add_property("TestProp",
                                                                          test_case))
testdata.insert()

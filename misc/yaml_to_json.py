#!/usr/bin/env python3

import sys
import json

import yaml

with open(sys.argv[1], 'r') as infile:
    print(json.dumps(yaml.safe_load(infile)))

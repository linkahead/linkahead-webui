#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
(C) Copyright IndiScale GmbH 2019
"""

import linkahead

linkahead.get_connection()._login()

# data model
if linkahead.execute_query("COUNT RecordType UserTemplate") == 0:

    datamodel = linkahead.Container()
    datamodel.extend([
        linkahead.Property("Query", datatype=linkahead.TEXT),
        linkahead.Property("templateDescription", datatype=linkahead.TEXT),
        linkahead.RecordType(
            "UserTemplate"
        ).add_property("Query", importance=linkahead.OBLIGATORY
                       ).add_property("templateDescription", importance=linkahead.OBLIGATORY),
    ])

    datamodel.insert()


# test data
testdata = linkahead.Container()

if linkahead.execute_query("COUNT Record 'Test Template (Delete with Error)'") == 0:
    testdata.append(
        linkahead.Record(
            "Test Template (Delete with Error)"
        ).add_parent("UserTemplate"
                     ).add_property("Query", "FIND Error"
                                    ).add_property("templateDescription",
                                                   "Test Template (Delete with Error)")
    )

if linkahead.execute_query("COUNT Record 'Test Template (Referencing)'") == 0:
    testdata.append(
        linkahead.Record(
            "Test Template (Referencing)"
        ).add_parent("UserTemplate"
                     ).add_property("UserTemplate", "Test Template (Delete with Error)")
    )

for i in range(2):
    testdata.append(
        linkahead.Record(
            "Test Template (User {})".format(i)
        ).add_parent("UserTemplate"
                     ).add_property("Query", "FIND Thing{}".format(i)
                                    ).add_property("templateDescription",
                                                   "Test Template (User {})".format(i))
    )

testdata.insert()

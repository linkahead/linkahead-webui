#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
(C) Copyright IndiScale GmbH 2019
"""

import linkahead
import random

linkahead.get_connection()._login()

# data model
datamodel = linkahead.Container()
datamodel.extend([
    linkahead.Property("longitude", datatype=linkahead.DOUBLE, unit="°"),
    linkahead.Property("latitude", datatype=linkahead.DOUBLE, unit="°"),
    linkahead.RecordType(
        "MapObject"
    ).add_property("longitude", importance=linkahead.OBLIGATORY
                   ).add_property("latitude", importance=linkahead.OBLIGATORY),
    linkahead.RecordType(
        "PathObject"
    ).add_property("MapObject", datatype=linkahead.LIST("MapObject")),
])

datamodel.insert()


# test data

testdata = linkahead.Container()

path = linkahead.Record()
path.add_parent("PathObject")
path.add_property("MapObject", datatype=linkahead.LIST("MapObject"), value=[])
testdata.append(path)

for i in range(100):
    loc = linkahead.Record(
        "Object-{}".format(i)
    ).add_parent("MapObject"
                 ).add_property("longitude", random.gauss(-42.0, 5)
                                ).add_property("latitude", random.gauss(77.0, 5))
    testdata.append(loc)
    path.get_property("MapObject").value.append(loc)


testdata.insert()

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# ** header v3.0
# This file is a part of the LinkAhead Project.
#
# Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
# Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header

""" Insert test data for manually testing the versioning feature in the
webinterface.
"""
# pylint: disable=no-member

import linkahead

# clean
old = linkahead.execute_query("FIND Test*")
if len(old) > 0:
    old.delete()

# data model

rt = linkahead.RecordType("TestRT")
rt.insert()


# test data
# record with several versions
rec1 = linkahead.Record("TestRecord1-firstVersion").add_parent("TestRT")
rec1.description = "This is the first version."
rec1.insert()

rec1.name = "TestRecord1-secondVersion"
rec1.description = "This is the second version."
rec1.update()
if rec1.version:
    ref = str(rec1.id) + "@" + rec1.version.id
else:
    ref = rec1.id

rec1.name = "TestRecord1-thirdVersion"
rec1.description = "This is the third version."
rec1.update()

rec2 = linkahead.Record("TestRecord2").add_parent("TestRT")
rec2.description = ("This record references the TestRecord1 in the second "
                    "version where the name and the description of the record "
                    "should indicate the referenced version.")
rec2.add_property("TestRT", ref)
rec2.insert()

rec3 = linkahead.Record("TestRecord3").add_parent("TestRT")
rec3.description = ("This record references the TestRecord1 without "
                    "specifying a version. Therefore the latest (at least the "
                    "third version) is being openend when you click on the "
                    "reference link.")
rec3.add_property("TestRT", rec1.id)
rec3.insert()

rec4 = linkahead.Record("TestRecord4").add_parent("TestRT")
rec4.description = ("This record has a list of references to several versions "
                    "of TestRecord1. The first references the record without "
                    "specifying the version, the other each reference a "
                    "different version of that record.")
if rec1.version:
    rec4.add_property("TestRT", datatype=linkahead.LIST("TestRT"),
                      value=[rec1.id,
                             str(rec1.id) + "@HEAD",
                             str(rec1.id) + "@HEAD~1",
                             str(rec1.id) + "@HEAD~2"])
else:
    rec4.add_property("TestRT", datatype=linkahead.LIST("TestRT"),
                      value=[rec1.id,
                             str(rec1.id),
                             str(rec1.id),
                             str(rec1.id)])
rec4.insert()

for i in range(4, 11):
    rec1.name = f"TestRecord1-{i}thVersion"
    rec1.description = f"This is the {i}th version."
    rec1.update()

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# ** header v3.0
# This file is a part of the LinkAhead Project.
#
# Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
# Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header

import os
import linkahead
import random

filename = "small.webm"
if not os.path.isfile(filename):
    import wget  # pylint: disable=E0401
    filename = wget.download("http://techslides.com/demos/sample-videos/small.webm")
linkahead.configure_connection(ssl_insecure=True)

# clean
old = linkahead.execute_query("FIND Test*")
if len(old):
    old.delete()

# data model
datamodel = linkahead.Container()
datamodel.extend([
    linkahead.RecordType("TestPreviewRecordType")
])

datamodel.insert()


# test data
testdata = linkahead.Container()

# record with references
refrec = linkahead.Record("TestPreviewRecord-references").add_parent("TestPreviewRecordType")
testdata.append(refrec)

video = linkahead.File(name="TestFileVideo", file=filename, path="testfile.webm")
image = linkahead.File(name="TestFileImage",
                       file="../src/core/pics/map_tile_linkahead_logo.png",
                       path="testfile.png")
for i in ["load-forever", "fall-back", "error", "success", "success-2"]:
    rec = linkahead.Record("TestPreviewRecord-{}".format(i)).add_parent("TestPreviewRecordType")
    testdata.append(rec)
    refrec.add_property("TestPreviewRecordType", rec)

testdata.append(image)
testdata.append(video)


testdata.insert()

os.remove(filename)
